﻿
   version v0.4.2 2023-03-13`09:14 Normalize versions
   Set all scripts to version v0.4.2
   ⬞

   chg 20230224°1714 Merge/transit
   fro : http://www.trekta.biz/svn/scriptsweb/trunk/webdevadv/index.html
   to  : https://normai.gitlab.io/scripts_webdev/webdevadv/index.html
   ⬞

   log 20201118°1751 v0.0.4
   ⬞

   ———————————————————————
   [file 20201118°1741] ⬞Ω
