﻿'use strict';
function init() {
  if (window.File && window.FileReader && window.FileList && window.Blob) { // feature detect
    function handleFileSelected(event) {
      let files = event.target.files;
      //for (let i = 0; i < files.length; i++) {       // Orginalzeile

      // (1) To work with the ar.forEach method we need an array of file objects
      let arFiles = [];
      for (let x in files) {
         if (! isNaN(x)) {             // process only props '0', '1', etc., not 'length', 'item', etc.
            arFiles.push(files[x]);
         }
      }

      // (2) Now with arFiles we have a proper array and can use the ar.forEach method
      arFiles.forEach(function (file) {
        let reader = new FileReader(); // this line and the rest of the 'loop body' remain unchanged
        if (file.type.match('text.*')) {
            reader.onload = (event) => {
            let span = document.createElement('span');
            span.innerHTML = reader.result;
            document.getElementById('list').insertBefore(span, null);
          };
          reader.readAsText(file);
        } else if(file.type.match('image.*')) {
            reader.onload = (event) => {
            let span = document.createElement('span');
            span.innerHTML = '<img class="thumbnail" src="' + reader.result + '"/>';
            document.getElementById('list').insertBefore(span, null);
          };
          reader.readAsDataURL(file);
        } else {
           alert("Datei wird nicht angezeigt: " + file.name);
        }
      });
    }
    document.getElementById('files').addEventListener('change', handleFileSelected, false);
  } else {
    alert('File API nicht vollständig durch den Browser unterstützt');
  }
}
document.addEventListener('DOMContentLoaded', init); // entspricht <body onlad="init()">
