﻿'use strict';
function init() {
  if (window.File && window.FileReader && window.FileList && window.Blob) { // feature detect
    function handleFileSelected(event) {
      let files = event.target.files;
      //for (let i = 0; i < files.length; i++) {       // Orginalzeile
      for (let x in files) {                           // ForEach-Schleife

        // Added condition to skip non-index properties.
        // Process only props '0', '1', etc., not 'length', 'item', etc.
        // The demo works as well without this precaution, just ends with error.
        if (isNaN(x)) {
          continue;
        }

        let file = files[x];                           //
        let reader = new FileReader();
        if (file.type.match('text.*')) {
            reader.onload = (event) => {
            let span = document.createElement('span');
            span.innerHTML = reader.result;
            document.getElementById('list').insertBefore(span, null);
          };
          reader.readAsText(file);
        } else if(file.type.match('image.*')) {
            reader.onload = (event) => {
            let span = document.createElement('span');
            span.innerHTML = '<img class="thumbnail" src="' + reader.result + '"/>';
            document.getElementById('list').insertBefore(span, null);
          };
          reader.readAsDataURL(file);
        } else {
           alert("Datei wird nicht angezeigt: " + file.name);
        }
      }
    }
    document.getElementById('files').addEventListener('change', handleFileSelected, false);
  } else {
    alert('File API nicht vollständig durch den Browser unterstützt');
  }
}
document.addEventListener('DOMContentLoaded', init); // entspricht <body onlad="init()">
