﻿

   project 'filereader3' 20200901°1711
   summary : This demonstrates …
   •


   project 'filereader2' 20200901°1211
   summary : This demonstrates …
   •


   project 20200901°0131 'filereader1'
   summary : This demonstrates …
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   sources : https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_41/
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_41/index.html [file 20200901°0133]
    • https://github.com/cleancoderocker/javascripthandbuch/tree/master/Kapitel12/Listing_12_41/scripts [file 20200901°0135]
    • https://github.com/cleancoderocker/javascripthandbuch/tree/master/Kapitel12/Listing_12_41/styles [file 20200901°0137]
   ⬞Ω
