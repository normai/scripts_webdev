﻿
   project 20200902°0531 '1'
   summary : This demonstrates …
   credits     : Philip Ackermann 2018
   license     : The code is used here under the 'Fair Use Doctrine'. That allows using
                  small portions of the published code for non-profit educational purposes.
                  Ackermann himself did not provide any license information in the files.
   sources : Philip Ackermann 2018 files
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel11/Listing_11_30/index.html
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel11/Listing_11_30/scripts/main.js
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel11/Listing_11_30/styles/main.css
   ⬞

   project 20200902°0541 '2'
   summary : This demonstrates …
   summary : Formal adjustment …
   credits     : Philip Ackermann 2018
   license     : The code is used here under the 'Fair Use Doctrine'. That allows using
                  small portions of the published code for non-profit educational purposes.
                  Ackermann himself did not provide any license information in the files.
   ⬞

   file ak1130_svgmocircle_3.html [file 20200903°1011]
   summary :
    • With SVG-Animation
    • Single file HTML, no more *.css and *.js
   ⬞Ω
