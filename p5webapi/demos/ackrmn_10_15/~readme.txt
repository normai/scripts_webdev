﻿
   project 20200901°0111 ''
   summary : This demonstrates …
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   sources : https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel10/Listing_10_15/
            • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel10/Listing_10_15/index.html [file 20200901°0113]
            • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel10/Listing_10_15/scripts/main.js [file 20200901°0115]
            • https://github.com/cleancoderocker/javascripthandbuch/tree/master/Kapitel10/Listing_10_15/styles [file 20200901°0117]
   ⬞Ω
