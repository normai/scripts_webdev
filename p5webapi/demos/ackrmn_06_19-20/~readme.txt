﻿
   project 20200901°2101 '1'
   summary     : This demonstrates …
   credits     : Philip Ackermann 2018
   license     : The code is used here under the 'Fair Use Doctrine'. That allows using
                  small portions of the published code for non-profit educational purposes.
                  Ackermann himself did not provide any license information in the files.
   sources : Philip Ackermann 2018
              https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel06/Listing_06_19-20/
              • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel06/Listing_06_19-20/index.html
              • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel06/Listing_06_19-20/scripts/main.js
              • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel06/Listing_06_19-20/styles/main.css
   ⬞

   project 20200901°2111 '2'
   summary : This demonstrates …
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   note        : This are not two projects, but just versions showing minimal differences.
                   The first is original code, the second has little adjustments.
   ⬞

   ⬞Ω
