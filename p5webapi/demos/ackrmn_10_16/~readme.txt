﻿
   project ~20200901°2121
   summary : This demonstrates …
   credits     : Philip Ackermann 2018
   license     : The code is used here under the 'Fair Use Doctrine'. That allows using
                  small portions of the published code for non-profit educational purposes.
                  Ackermann himself did not provide any license information in the files.
   sources : Philip Ackermann 2018
              https://github.com/cleancoderocker/javascripthandbuch/tree/master/Kapitel10/Listing_10_16
   ⬞Ω
