﻿
   project 20180912°0011 'shoppingcart'
   summary : This demonstrates the use of the WebStorage-API
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   source : https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_27/
   ⬞Ω
