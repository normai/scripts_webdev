﻿
   project 20200901°0311 ''
   summary : This demonstrates …
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   sources : https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_51/
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_51/index.html [file 20200901°0313]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_51/scripts/main.js [file 20200901°0315]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_51/styles/main.css [file 20200901°0317]
   ⬞Ω
