﻿'use strict';
function init() {
  let sliderX = document.getElementById('sliderX');
  let sliderY = document.getElementById('sliderY');
  sliderX.addEventListener('change', (evt) => {
    moveCircle(evt.target.value, 'x');
  });
  sliderY.addEventListener('change', (evt) => {
    moveCircle(evt.target.value, 'y');
  });
}
function moveCircle(value, direction) {
  let svgDocument = document.getElementById('svg');
  let circle = svgDocument.getElementById('circle');
  circle.setAttribute('c' + direction, value);
}
window.addEventListener('DOMContentLoaded', init);
