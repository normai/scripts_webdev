﻿
   project 20200901°0211 ''
   summary : This demonstrates …
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   sources : https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_46-47/
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_46-47/index.html [file 20200901°0213]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_46-47/scripts/main.js [file 20200901°0215]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_46-47/scripts/worker.js [file 20200901°0217]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/common/styles/main.css [file 20200901°0221]
   ⬞Ω
