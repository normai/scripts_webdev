﻿function draw() {
   let canvas = document.getElementById('canvas');
   if (canvas.getContext){
      let context = canvas.getContext('2d');
      let path = new Path2D();
      path.moveTo(50, 200);    // Zeichnen des Pfades
      path.lineTo(400, 50);    // Zeichnen des Pfades
      path.lineTo(400, 200);   // Zeichnen des Pfades
      path.closePath();        // Schließen des Pfades
      context.stroke(path);    // Rahmen einfärben
   } else {
      // Browser kennt canvas.getContext nicht, wir können nicht zeichnen,
      // wir sollten auch dem Anwender eine Meldung darüber geben.
   }
}
document.addEventListener('DOMContentLoaded', draw);
