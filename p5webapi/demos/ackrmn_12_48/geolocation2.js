﻿'use strict';
function getPosition() {
  if (navigator.geolocation) { // feature detect
    navigator.geolocation.getCurrentPosition(function(position) {
      document.getElementById('latitude').value = position.coords.latitude;
      document.getElementById('longitude').value = position.coords.longitude;
      document.getElementById('altitude').value = position.coords.altitude;
      document.getElementById('accuracy').value = position.coords.accuracy;
      document.getElementById('altitudeAccuracy').value = position.coords.altitudeAccuracy;
      document.getElementById('heading').value = position.coords.heading;
      document.getElementById('speed').value = position.coords.speed;
      ////document.getElementById('timestamp').value = position.timestamp;
      document.getElementById('timestamp').value = niceTimestamp(position.timestamp); // DOMTimeStamp
    });
  }
  else {
     alert("Ihr Browser unterstützt keine Geolocation.");
  }
}

// [func 20200901°1021]
function niceTimestamp(zeitstempl)
{
   let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

   let oDate = new Date(zeitstempl); // Aus unsinnigem Zeitstempel ein Objekt mit sinnvollen Eigenschaften

   let year = oDate.getFullYear();
   let month = oDate.getMonth();
   let monthName = months[month];
   let day = oDate.getDate();
   let hour = oDate.getHours();
   let minu = oDate.getMinutes();
   let secu = oDate.getSeconds();
   let nice = year + '-' + monthName + '-' + day + " " + hour + ':' + minu + ':' + secu;

   return nice
}

/*
 * ref : https://developer.mozilla.org/en-US/docs/Web/API/Geolocation
 * ref : https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/getCurrentPosition
 * ref : https://javascript.info/date
 * ref : https://stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
 * ref : https://usefulangle.com/post/258/javascript-timestamp-to-date-time
 * ref : https://developer.mozilla.org/en-US/docs/Web/API/DOMTimeStamp
 * ref : https://stackoverflow.com/questions/3089308/converting-domtimestamp-to-localized-hhmmss-mm-dd-yy-via-javascript
 */