﻿
   project 'geolocation2' 20200901°1021
   summary : This demonstrates
      • Zeitstempel formatiert ausdrucken
      • Meldung, falls Browser das Geolocation-Objekt nicht kennt
   ⬞

   project 'geolocation1' 20200901°0231 orginalprojekt
   summary : This demonstrates …
   credits : Philip Ackermann 2018
   license : The code is used here under the 'Fair Use Doctrine'. That allows using
             small portions of the published code for non-profit educational purposes.
             Ackermann himself did not provide any license information in the files.
   sources : https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_48/
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_48/index.html [file 20200901°0233]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_48/scripts/main.js [file 20200901°0235]
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_48/styles/main.css [file 20200901°0237]
   ⬞Ω
