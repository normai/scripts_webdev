﻿
   project 20200902°0401 '1'
   summary : This demonstrates …
   credits     : Philip Ackermann 2018
   license     : The code is used here under the 'Fair Use Doctrine'. That allows using
                  small portions of the published code for non-profit educational purposes.
                  Ackermann himself did not provide any license information in the files.
   sources : Philip Ackermann 2018 files
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel11/Listing_11_25/index.html
    • https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel11/Listing_11_25/example.svg
   ⬞

   project 20200902°0411 '2'
   summary : This demonstrates …
   credits     : Philip Ackermann 2018
   license     : The code is used here under the 'Fair Use Doctrine'. That allows using
                  small portions of the published code for non-profit educational purposes.
                  Ackermann himself did not provide any license information in the files.
   ⬞Ω
