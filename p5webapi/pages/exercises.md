Möglicher Kursverlauf in 5 Tagen

# Tag 1

## Themen:

- Theorie: Web-API LocalStorage

- Übung 1: Ackermann Demo 'Shopping Cart' zum Laufen bringen

- Übung 2: 'Seitenaufrufzähler mittels LocalStorage'

## Übung 1: Ackermann Demo '[Shopping Cart](./../demos/ackrmn_12_27/) zum Laufen bringen'

- Bringen Sie das Beispiel in Ihrem Browser lokal zum Laufen

- Tracen Sie im Debugger die beiden Schleifen (1) die in catalog.js, die eine
Tabelle aufbaut und (2) die in shoppingcart.js, die eine UL-Liste füllt.

- Inspizieren Sie den LocalStorage im Debugger

- Modifizieren Sie das Beispiel

## Übung 2 'Seitenaufrufzähler mittels LocalStorage'

- Schreiben Sie eine HTML-Seite mit zwei eingebauten JavaScript-Funktionen

- Funktion eins: 'Zaehlen' verwendet den LocalStorage, um einen Aufrufzähler
bei jedem Aufruf der Seite je um eins zu **erhöhen**.

- Funktion zwei: 'Anzeigen' liest den LocalStorage, um den jeweils den
aktuellen Zählerstand **anzuzeigen**.

Falls Sie keinen Plan haben wie Sie anfangen sollen, inspizieren Sie die Demo in
[./../demos/ncm/u2\_seitenaufrufzaehler.html](./../demos/ncm/u2_seitenaufrufzaehler.html)

---

# Tag 2

## Themen:

- Theorie: Web-API Geolocation (Codebesprechung)
- Theorie: Web-API File (Codebesprechung)
- Theorie: Web-API Webworkers (oberflächlich)
- Theorie: Web-API Websockets (oberflächlich)
- Theorie: jQuery (Codebesprechung)
- Übung 3: Ergänzen Sie Ackermann Demo 12.48 Geolocation (Zeitstempel leserlich ausgeben)
- Übung 4: Modifizieren Sie Ackermann Demo 12.41 FileReader (ForEach-Schleife verwenden)

## Übung 3: 'Ergänzen Sie Ackermann Demo 12.48 Geolocation'

- Es wird ein kryptischer Zeitstempel ausgegeben. Verwandeln Sie diese Ausgabe
in eine verständliche Datums-/Zeitangabe.

- Wenn Sie den Knopf 'Position ermitteln' drücken, aber der Browser hat kein
navigator.geolocation Objekt, dann soll eine Nachricht erscheinen, damit die
Anwenderin sieht, daß diese Funktion nicht ausgeführt werden kann.

Lösungsbeispiel siehe
[./../demos/ackrmn\_12\_48/geolocation2.js](./../demos/ackrmn_12_48/geolocation2.js)

## Übung 4: 'Modifizieren Sie Ackermann Demo 12.41 FileReader'

- In der JS-Datei kommt eine herkömmliche For-Schleife vor.

- Ändern Sie diese For-Schleife in eine ForEach-Schleife "**for (let o in files) {..}**"

- Für Fortgeschrittene. Andern Sie die For-Schleife in die
Array-ForEach-Methode "**files.forEach(function(){..});**".
Dieser Programmierstil nennt sich 'funktionale Programmierung'.

Lösungsvorschlag für ForEach-Schleife siehe
[./../demos/ackrmn\_12\_41/filereader2.js](./../demos/ackrmn_12_41/filereader2.js)
und für ar.forEach-Funktion siehe
[./../demos/ackrmn\_12\_41/filereader3.js](./../demos/ackrmn_12_41/filereader3.js)

&nbsp;

---

# Tag 3

## Themen:

- Theorie: Grafik mit Canvas

- Theorie: Grafik mit SVG

- Übung 5: Canvas — Serie Quadratischer Kurven in Single-HTML-Datei

- Übung 6: SVG — Zeichen Sie Kreise und Rechtecke.

- Übung 7: SVG — Maus fängt Kreis (Fortgeschrittene)

### Übung 5: Canvas — Serie Quadratischer Kurven in Single-HTML-Datei

- Zeichnen Sie eine Serie Quadratischer Kurven mit jeweils gleichen Anfangs-
und Endpunkten aber verschiedenen Kontrollpunkten. Damit können Sie das
Verhalten der Quadratische Kurve erkennen.

- Um zu sehen wie die quadratische Kurve geht, vergleichen Sie Demo
[./../demos/ackrmn\_11\_11/ak1111\_canvasarc\_1.html](./../demos/ackrmn_11_11/ak1111_canvasarc_1.html)

- Verwenden Sie dabei nicht drei Dateien HTML/CSS/JS, sondern nur eine HTML-Datei
und CSS und JS definieren Sie inline.

Lösungsvorschlag siehe
[./../demos/ncm/u5canvas\_quad-curves.html](./../demos/ncm/u5canvas_quad-curves.html)

### Übung 6: SVG — Zeichen Sie Kreise und Rechtecke.

- Schreiben Sie eine HTML-Seite, auf der eine SVG-Grafik erzeugt wird,
auf ein oder mehrere Kreise, Rechtecke sind.

Als Vorlage kann dienen:
[./../demos/ackrmn\_11\_30/ak1130\_svgmocircle\_1.html](./../demos/ackrmn_11_30/ak1130_svgmocircle_1.html)

SVG-Referenzen: Mozilla [SVG Elementreferenz](https://developer.mozilla.org/de/docs/Web/SVG/Element),
für Kreis: [&lt;circle&gt;](https://developer.mozilla.org/de/docs/Web/SVG/Element/circle),
für Rechteck: [&lt;rect&gt;](https://developer.mozilla.org/de/docs/Web/SVG/Element/rect)

Diese Aufgabe sollte jede und jeder lösen können. Wer diese Aufgabe gelöst hat
kann die nächste schwierigere versuchen.

Lösungsvorschlag:
[./../demos/ncm/u6svg-circles.html](./../demos/ncm/u6svg-circles.html)

### Übung 7: SVG — Maus fängt Kreis (Fortgeschrittene)

- Schreiben Sie eine HTML-Seite mit einer SVG-Grafik mit einem Kreis darin.
Als Vorlage kann dienen:
[./../demos/ackrmn\_11\_31-32/ak1131\_svgsliders\_1.html](./../demos/ackrmn_11_31-32/ak1131_svgsliders_1.html)

- Wenn die Maus in die Grafik fährt, dann folgt der Kreis dem Mauszeiger.
Als Vorlage, wie die Position des Mauszeigers gelesen wird dient
[./../demos/ackrmn\_06\_19-20/ak0619\_mousecanvas\_1.html](./../demos/ackrmn_06_19-20/ak0619_mousecanvas_1.html)

Hinweise: Die Schwierigkeit, daß die Vorlage für die Mausbehandlung aus dem Canvas-Kapitel
stammt, und Sie das auf SVG übertragen müssen.

Lösungsvorschlag:
[./../demos/ncm/u7svg-maus-faengt-kreis.html](./../demos/ncm/u7svg-maus-faengt-kreis.html).
Die Lösung hat sich viel einfacher erwiesen als erwartet!

&nbsp;

---

# Tag 4

## Themen:

- Animation

- Übung 8: Bringen Sie JsFiddle 'Kugel kommt zu Mausklick' zum Laufen

- Übung 9: Schaffen Sie eine animierte Computergraphik

## Übung 8: JsFiddle 'Kugel kommt zu Mausklick'

- Auf [jsfiddle.net/9h261pzo/291/](http://jsfiddle.net/9h261pzo/291/) finden Sie
den Code zu einer CSS-Transition, der bewirkt, daß eine Kugel sich zum
Mausklick hin bewegt.

- Bringen Sie diesen JsFiddle in einer eigenen Seite zum Laufen.

Lösungsvorschlag:
[./../demos/ncm/u8ani-kugel-zu-klick.html](./../demos/ncm/u8ani-kugel-zu-klick.html)

## Übung 9: Animierte Computergraphik

Erzeugen Sie eine animierte Computergraphik, entweder mit Canvas oder mit SVG.
Wählen Sie eines der folgenden Motive oder ein frei erfundenes.

### Übung 9.1: Motiv im Stil von 'Rose'

- Vergleichen sie
[www.trekta.biz/svn/demosjs/trunk/canvaspulse/canvas-1-rose.html](http://www.trekta.biz/svn/demosjs/trunk/canvaspulse/canvas-1-rose.html)

- In die Vorlage einer statischen Rose ergänzen Sie mittels requestAnimationFrame
ein dynamische Wiederholung und ändern bei jeder Wiederholung einen oder mehrere Variablen

Lösungsbeispiel: Rose ohne Animation
[./../demos/ncm/u91\_rose\_1.html](./../demos/ncm/u91_rose_1.html)
und mit Animation
[./../demos/ncm/u91\_rose\_2.html](./../demos/ncm/u91_rose_2.html)

### Übung 9.2: Motiv im Stil von 'Pulse'

- Verwenden Sie als Vorlage
[www.trekta.biz/svn/demosjs/trunk/canvaspulse/canvas-2-pulse-v1.html](http://www.trekta.biz/svn/demosjs/trunk/canvaspulse/canvas-2-pulse-v1.html)

- Bauen Sie einen Schieberegler ein, um die Pulsfrequenz zu ändern.
Als Vorlage für den Schieberegler verwenden Sie
[./../demos/ackrmn\_11\_31-32/ak1131\_svgsliders\_1.html](./../demos/ackrmn_11_31-32/ak1131_svgsliders_1.html).

Lösungsvorschlag: Ohne Schieberegler,
[./../demos/ncm/u92\_pulse\_1.html](./../demos/ncm/u92_pulse_1.html),
mit Schieberegler
[./../demos/ncm/u92\_pulse\_2.html](./../demos/ncm/u92_pulse_2.html)

### Übung 9.3: Motiv im Stil von Georg Nees 'Schotter'

- Vergleichen Sie z.B.
[github.com/tja/schotter](https://github.com/tja/schotter) oder
[www.sunjackson.com/post/6131/](https://www.sunjackson.com/post/6131/)

- Schaffen Sie zuerst ein statisches Bild.

- Als Vorlage kann folgende Demo dienen
[./../demos/ackrmn\_11\_30/ak1130\_svgmocircle\_1.html](./../demos/ackrmn_11_30/ak1130_svgmocircle_1.html)

- In einem eventuellen zweiten Schritt bauen Sie eine Animation ein.

Lösungsbeispiel — (1) Einzelnes Rechteck:
[./../demos/ncm/u93\_schotter1.html](./../demos/ncm/u93_schotter1.html),
(2) Schwarm:
[u93\_schotter2.html](./../demos/ncm/u93_schotter2.html)
(3) Verschiebung:
[u93\_schotter3.html](./../demos/ncm/u93_schotter3.html)
(4) Rotation:
[u93\_schotter4.html](./../demos/ncm/u93_schotter4.html)

<a href="./pages/img/20200902o2303.header.v1.p99q66.jpg">
<img src="./../pages/img/20200902o2303.header.v1.p50q66.jpg" width="600" height="372">
</a>
<br>Bild von [https://github.com/tja/schotter](https://github.com/tja/schotter)

&nbsp;

---

# Tag 5

## Themen:

- Theorie: Wiederholung der Themen dieser Woche

- Übung 10 : SVG-Animation Schnellbeispiel 'Rectangle Run'

## Übung 10 : SVG-Animation Schnellbeispiel 'Rectangle Run'

- Schreiben Sie eine SVG-Animation, in der sich ein Rechteck auf der Webseite
von links nach rechts bewegt.

- Wenn das Rechteck rechts aus dem Bild läuft soll es links wieder hereinkommen.

- Als Vorlage können Sie
[./../demos/ncm/u93\_schotter1.html](./../demos/ncm/u93_schotter1.html)
verwenden, und dann brauchen Sie die Methode
**window.requestAnimationFrame(run)**.

Lösungsbeispiel:
[./../demos/ncm/u10\_rectangl-run.html](./../demos/ncm/u10_rectangl-run.html)

&nbsp;

---
<sub><sup><sub>[file 20200830°1531] ⬞Ω</sub></sup></sub>
