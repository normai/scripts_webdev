﻿## Varible usage styles

```
   // flavour : just use any var without keyword
   // scope   : global
   // note    : bad style, pollutes global namespace, throws error in ES6 [check]
   i = 2;
   i = i + 3;
```

**Oha**, hier eine Formatierungsdemo ..

```
   // flavour : use without var keyword
   // scope   : inside function
   // note    : recommended before ES6
   var i = 2;
   i = i + 3;
```

```
   // use without keyword, global, bad style
   // pollutes the global namespace
   // throws error in ES6 [check]
   'use strict'
   let i = 2;
   i = i + 3;
```
<span style="display:none;">file 20190827°0331 </span><sup><sub>⬞Ω</sub></sup>
