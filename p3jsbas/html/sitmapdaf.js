﻿/**
 * file : /sitmapdaf.js
 * id : 20200630°1531 [after 20200221°0331]
 * encoding : UTF-8-with-BOM
 * note :
 */

var DAF_PAGENODES =
[
   [ 'p3jsbas'        ,  './../webdevjs.html'
     , [ ''            ,  './../webdevjs.html' ] // workaround for Daftari issue 20200602°1021 'breadcrumbs eat page'
   ]
];
