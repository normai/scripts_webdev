﻿<img src="./p1html/pages/icos/20150306o1421.rgesthuizenincandescentlightbulb1.v1.x0128y0128.png" align="right" width="128" height="128" alt="Logo 20150306°1421">

# Web Development Scripts <sup><sub><sup>*v0.4.3*</sup></sub></sup>

Subject: This are some scripts around web development.

Status: Possibly messy

The scripts are:

1. **HTML Basics** — Introduction into HTML, without CSS, without JavaScript

2. **CSS Basics** — Introduction into CSS *(Status: empty)*

3. **JavaScript Basics** — Introduction into JavaScript *(Status: uncomplete)*

4. **JavaScript Advanced** — JavaScript advanced topics

5. **HTML5 Web API** — About the HTML5 Web API

6. **Graphics** — Graphics in HTML/CSS/JavaScript *(Status: empty)*

7. **Networking Basics** — On protocols, topologies, media and products

See

### &nbsp; &nbsp; &nbsp; [**normai.gitlab.io/scripts_webdev/index.html**](https://normai.gitlab.io/scripts_webdev/index.html)

&nbsp;

&nbsp;

---

<sup><sub>*Project 20180910°1123, file 20180910°1124, repo 20230226°0143]* ⬞Ω</sub></sup>
