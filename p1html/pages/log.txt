﻿
   -----------------------------------------------------

   Tag 5 2020-Feb-28

   - Media-Einbettung

   -----------------------------------------------------

   Tag 4 2020-Feb-27

   - Listen

   - Formulare

   -----------------------------------------------------

   Tag 3 2020-Feb-26

   - Tabellen

   -----------------------------------------------------

   Tag 2 2020-Feb-25

   - Navigation

   -----------------------------------------------------

   Tag 1 2020-Feb-24

   - Grundlegende Struktur einer Webseite und erste Tags, siehe hallo.html

   - Verlinkung.

   - CSS als Inline-, Seiten- oder globale Regeln.

   - Übersicht über HTML und CSS mittels Mozilla-Dokumentation

   - Layout für die Autohaus-Webseite

   - Bereitstellung und Verlinkung der Autohaus-Seiten.


   -----------------------
   [file 20200214°0913] ⬞Ω

