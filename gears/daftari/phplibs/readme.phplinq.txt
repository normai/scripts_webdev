﻿
   chg 20201203°1151 Delete library phplinq-0.4.0
   note : The library is unused here and seems no more maintained anyway. Some files
 caused 'parse errors' while being scanned from VSCode/XDebug, thus cluttering the output console.
   ⬞

   note 20190428°0105 'phplinq seems obsolete'
   matter : This project seems no more to exist.
   proposal : Alternatively use
   see : ref 20190428°0113 'github → YaLinqo'
   see : download 20190428°0111 'YaLinqo v2.4.2'
   ⬞

   folder 20161008°0811 '/daftari/phplibs/phplinq'
   summary : The folder stores phplinq-0.4.0
   see : download 20110531°1821
   see : ref 20110531°1822 'codeplex → PHPLinq'
   ⬞

   ———————————————————————
   [file 20161008°0812] ⬞Ω
