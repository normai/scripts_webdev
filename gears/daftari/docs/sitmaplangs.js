﻿/*!
 * This provides token translations for the crumbs
 *
 * file        : 20190417°0421 daftari/docs/sitmaplangs.js
 * license     : GNU AGPL v3
 * copyright   : © 2014 - 2024 Norbert C. Maier
 * authors     : Norbert C. Maier
 * encoding    : UTF-8-with-BOM
 */

/*global window, Daf*/ // setting for e.g. validatejavascript.com [line 20200103°0241]

/**
 * Announce namespace
 *
 * @id 20170923°0421`10
 * @c_o_n_s_t — Namespace
 */
Daf = window.Daf || {};

/**
 * Announce namespace
 *
 * @id 20150317°0911`02
 * @const — Namespace
 */
Daf.Sitmp = Daf.Sitmp || {};

/**
 * Translations for the crumbs, experimental
 *
 * @id 20190417°0411
 * @type {Object}
 */
Daf.Sitmp.oSitlangs =
{
   "fileinfo" :
   {    "file"     : "daftari/docs/sitmaplangs.js"
      , "id"       : "20161109°2051"
      , "encoding" : "UTF-8-with-BOM"
      , "callers"  : "~daftari/jsi/daflingos.js"
   }
   , "Articles#20181217°0233" :
   {
        "eng" : "Articles"
      , "ara" : "مقالات"
      , "chi" : "用品"
      , "cze" : "Články"
      , "fre" : "Des articles"
      , "ger" : "Artikel"
      , "hin" : "सामग्री"
      , "ita" : "Articoli"
      , "jpn" : "記事"
      , "rus" : "статьи"
      , "spa" : "Artículos"
      , "swa" : "Makala"
   }
   , "Autoflick#20201114°0223" :
   {
        "eng" : "Autoflick"
      , "ara" : "تسليم السيارات"
      , "chi" : "自动翻面"
      , "cze" : "Automatické Otočení"
      , "fre" : "Rotation Automatique"
      , "ger" : "Autoblättern"
      , "hin" : "ऑटो पलट गया"
      , "ita" : "Gira Automaticamente la Pagina"
      , "jpn" : "ページを自動めくります"
      , "rus" : "автоповорот страницы"
      , "spa" : "Pasar la Página Automáticamente"
      , "swa" : "Geuza Kiotomatiki"
   }
   , "Blog#20180619°0322" :
   {
        "eng" : "Blog"
      , "ara" : "مدونة"
      , "chi" : "博客"
      , "cze" : "Blog"
      , "fre" : "Blog"
      , "ger" : "Blog"
      , "hin" : "ब्लॉग"
      , "ita" : "Blog"
      , "jpn" : "ブログ"
      , "rus" : "блог"
      , "spa" : "Blog"
      , "swa" : "Blogu"
   }
   , "Blogs#20180619°0323" :
   {
        "eng" : "Blogs"
      , "ara" : "المدونات"
      , "chi" : "博客"
      , "cze" : "Blogy"
      , "fre" : "Blogs"
      , "ger" : "Blogs"
      , "hin" : "ब्लॉग"
      , "ita" : "Blog"
      , "jpn" : "ブログ"
      , "rus" : "блоги"
      , "spa" : "Blogs"
      , "swa" : "Blogu"
   }
   , "Breadcrumbs#20161109°2111" :
   {    "eng" : "Breadcrumbs"
      , "ara" : "فتات الخبز"
      , "chi" : "面包屑"
      , "cze" : "Drobečková"
      , "fre" : "Chapelure"
      , "ger" : "Paniermehl"
      , "hin" : "Breadcrumbs"
      , "ita" : "Pangrattato"
      , "jpn" : "パン粉"
      , "rus" : "панировочные"
      , "spa" : "Pan Rallado"
      , "swa" : "Breadcrumbs"
   }
   , "Cakecrumbs#20181217°0234" :
   {    "eng" : "Cakecrumbs"
      , "ara" : "كعكة فتات"
      , "chi" : "蛋糕屑"
      , "cze" : "Cukrovinky"
      , "fre" : "Miettes de gâteaux"
      , "ger" : "Kuchenkrümel"
      , "hin" : "केक-टुकड़ों"
      , "ita" : "Cake-briciole"
      , "jpn" : "ケーキのパン粉"
      , "rus" : "Торт-крошки"
      , "spa" : "Migas de pastel"
      , "swa" : "Makombo ya keki"
   }
   , "CanvasGear#20161109°2112" :
   {    "eng" : "CanvasGear"
      , "ara" : "معدات قماش"
      , "chi" : "帆布齿轮"
      , "cze" : "Plátnogear"
      , "fre" : "Enginsdetoile"
      , "ger" : "Leinwandgetriebe"
      , "hin" : "कैनवास गियर"
      , "ita" : "Ingranaggiotela"
      , "jpn" : "キャンバスギア"
      , "rus" : "холст передач"
      , "spa" : "Engranajedelalona"
      , "swa" : "Turubai Gia"
   }
   , "Credits#20161109°2113" :
   {    "eng" : "Credits" , "rem" : "Thanks"
      , "ara" : "شكر"
      , "chi" : "谢谢"
      , "cze" : "Díky"
      , "fre" : "Merci"
      , "ger" : "Danksagung"
      , "hin" : "शुक्रिया"
      , "ita" : "Grazie"
      , "jpn" : "クレジット"
      , "rus" : "благодаря"
      , "spa" : "Gracias"
      , "swa" : "Shukrani"
   }
   , "DaftariBoard#20161109°2114" :
   {
        "eng" : "Daftari Cockpit"
      , "ara" : "دفتري مجلس"
      , "chi" : "Daftari 板"
      , "cze" : "Daftari Deska"
      , "fre" : "Daftari Conseil"
      , "ger" : "Daftari Kanzel"
      , "ger_v0" : "Daftari Armaturen"
      , "hin" : "Daftari मंडल"
      , "ita" : "Daftari Pensione"
      , "jpn" : "ダフタリ ボード"
      , "rus" : "Дафтари доска"
      , "spa" : "Daftari Tablero"
      , "swa" : "Daftari Bodi"
   }
   , "Editpoints#20161109°2115" :
   {    "eng" : "Editpoints"
      , "ara" : "نقاط ثابتة"
      , "chi" : "静点"
      , "cze" : "Statické Body"
      , "fre" : "Des Points Statiques"
      , "ger" : "Editpunkte"
      , "hin" : "स्टेटिक अंक"
      , "ita" : "Punti Statici"
      , "jpn" : "静的ポイント"
      , "rus" : "Статические моменты"
      , "spa" : "Puntos Estáticos"
      , "swa" : "Pointi Static"
   }
   , "FadeInFiles#20161109°2116" :
   {    "eng" : "Fade-In-Files"
      , "ara" : "تتلاشى في ملفات"
      , "chi" : "淡入文件"
      , "cze" : "Slábnout v Souborech"
      , "fre" : "Fanent dans les Fichiers"
      , "ger" : "Dateien-Einblenden"
      , "hin" : "फाइलों में फीका"
      , "ita" : "Fade in File"
      , "jpn" : "ファイルをフェードイン"
      , "rus" : "исчезать в файлах"
      , "spa" : "Desvanecerse en los Archivos"
      , "swa" : "Kuisha Katika Mafaili"
   }
   , "Features#20161109°2117" :
   {    "eng" : "Features"
      , "ara" : "الميزات"
      , "chi" : "产品特点"
      , "cze" : "Rysy"
      , "fre" : "Caractéristiques"
      , "ger" : "Eigenschaften"
      , "hin" : "विशेषताएं"
      , "ita" : "Caratteristiche"
      , "jpn" : "特徴"
      , "rus" : "особенности"
      , "spa" : "Características"
      , "swa" : "Features"
   }
   , "Fonts#20230414°1011" :
   {    "eng" : "Fonts"
      , "ara" : "الخطوط"
      , "chi" : "字体"
      , "cze" : "Fonty"
      , "fre" : "Polices"
      , "ger" : "Schriften"
      , "hin" : "फोंट्स"
      , "ita" : "Caratteri"
      , "jpn" : "フォント"
      , "rus" : "шрифты"
      , "spa" : "Fuentes"
      , "swa" : "Fonti"
   }
   , "Gallery#20161109°2121" :
   {    "eng" : "Gallery"
      , "ara" : "رواق"
      , "chi" : "画廊"
      , "cze" : "Galerie"
      , "fre" : "Galerie"
      , "ger" : "Galerie"
      , "hin" : "गेलरी"
      , "ita" : "Galleria"
      , "jpn" : "ギャラリー"
      , "rus" : "галерея"
      , "spa" : "Galería"
      , "swa" : "Nyumba Ya Sanaa"
   }
   , "Help#20161109°2122" :
   {
        "eng" : "Help"
      , "ara" : "مساعدة"
      , "chi" : "帮助"
      , "cze" : "Pomoc"
      , "fre" : "Assistant"
      , "ger" : "Hilfe"
      , "hin" : "सहायता"
      , "ita" : "Assistenza"
      , "jpn" : "助けて"
      , "rus" : "Помогите"
      , "spa" : "Ayudar"
      , "swa" : "Msaada"
   }
   , "HelpFiles#20190205°0552" :
   {
        "eng" : "Help Files"
      , "ara" : "ملفات المساعدة"
      , "chi" : "帮助文件"
      , "cze" : "Soubory Nápovědy"
      , "fre" : "Fichiers d'Aide"
      , "ger" : "Hilfedateien"
      , "hin" : "फाइलों में मदद करें"
      , "ita" : "Aiuta i File"
      , "jpn" : "ヘルプファイル"
      , "rus" : "Файлы справки"
      , "spa" : "Archivos de ayuda"
      , "swa" : "Faili za Usaidizi"
   }
   , "Imprint#20180619°0321" :
   {
        "eng" : "Imprint"
      , "ara" : "بصمة"
      , "chi" : "版本说明"
      , "cze" : "Otisk"
      , "fre" : "Imprimer"
      , "ger" : "Impressum"
      , "hin" : "छाप"
      , "ita" : "Impronta"
      , "jpn" : "インプリント"
      , "rus" : "выходные данные"
      , "spa" : "Imprimir"
      , "swa" : "Imprint"
   }
   , "Installation#20161109°2123" :
   {    "eng" : "Installation"
      , "ara" : "تركيب"
      , "chi" : "安装"
      , "cze" : "Instalace"
      , "fre" : "Installation"
      , "ger" : "Installation"
      , "hin" : "स्थापना"
      , "ita" : "Installazione"
      , "jpn" : "インストール"
      , "rus" : "установка"
      , "spa" : "Instalación"
      , "swa" : "Ufungaji"
   }
   , "Introduction#20161109°2124" :
   {    "eng" : "Introduction"
      , "ara" : "مقدمة"
      , "chi" : "介绍"
      , "cze" : "Úvod"
      , "fre" : "Introduction"
      , "ger" : "Einleitung"
      , "hin" : "परिचय"
      , "ita" : "Introduzione"
      , "jpn" : "導入"
      , "rus" : "введение"
      , "spa" : "Introducción"
      , "swa" : "Utangulizi"
   }
   , "Issues#20161109°2125" :
   {    "eng" : "Issues"
      , "ara" : "قضايا"
      , "chi" : "问题"
      , "cze" : "Problémy"
      , "fre" : "Questions"
      , "ger" : "Tücken"
      , "hin" : "मुद्दे"
      , "ita" : "Problemi"
      , "jpn" : "問題"
      , "rus" : "проблемах"
      , "spa" : "Cuestiones"
      , "swa" : "Masuala"
   }
   , "License#20161109°2126" :
   {    "eng" : "License"
      , "ara" : "رخصة"
      , "chi" : "许可证"
      , "cze" : "Licence"
      , "fre" : "Licence"
      , "ger" : "Lizenz"
      , "hin" : "लाइसेंस"
      , "ita" : "Licenza"
      , "jpn" : "ライセンス"
      , "rus" : "лицензия"
      , "spa" : "Licencia"
      , "swa" : "Leseni"
   }
   , "Lingos#20161109°2127" :
   {    "eng" : "Lingos"
      , "ara" : "رطانة"
      , "chi" : "行话"
      , "cze" : "žargon"
      , "fre" : "Jargon"
      , "ger" : "Sprachen"
      , "hin" : "लिंगौ भाषा"
      , "ita" : "Gergo"
      , "jpn" : "専門用語"
      , "rus" : "жаргон"
      , "spa" : "Jerga"
      , "swa" : "Lingo"
   }
   , "Login#20161109°2131" :
   {    "eng" : "Login"
      , "ara" : "دخول"
      , "chi" : "注册"
      , "cze" : "Přihlášení"
      , "fre" : "Connexion"
      , "ger" : "Anmelden"
      , "hin" : "लॉगिन"
      , "ita" : "Entra"
      , "jpn" : "ログイン"
      , "rus" : "Войти"
      , "spa" : "Login"
      , "swa" : "Login"
   }
   , "Moon#20181217°0113" : // replaced by Moonwalk#20181217°0114
   {    "eng" : "Moon"
      , "ara" : "القمر"
      , "chi" : "月亮"
      , "cze" : "měsíc"
      , "fre" : "lune"
      , "ger" : "Mond"
      , "hin" : "चांद"
      , "ita" : "Luna"
      , "jpn" : "月"
      , "rus" : "Луна"
      , "spa" : "Luna"
      , "swa" : "mwezi"
   }
   , "Moonwalk#20181217°0114" : // replaced by DevelopersArea#20201228°1211
   {    "eng" : "Moonwalk"
      , "ara" : "التمشي على سطح القمر"
      , "chi" : "月亮漫步"
      , "cze" : "Měsíční Chůze"
      , "fre" : "Marche de la Lune"
      , "ger" : "Mondspaziergang"
      , "hin" : "चाँद की सैर"
      , "ita" : "Passeggiata Lunare"
      , "jpn" : "ムーンウォーク"
      , "rus" : "Лунная походка"
      , "spa" : "Paseo Lunar"
      , "swa" : "Kutembea Mwezi"
   }
   , "Overview#20180513°1731" :
   {    "eng" : "Overview"
      , "ara" : "نظرة عامة"
      , "chi" : "概观"
      , "cze" : "Přehled"
      , "fre" : "Aperçu"
      , "ger" : "Übersicht"
      , "hin" : "अवलोकन"
      , "ita" : "Panoramica"
      , "jpn" : "概要"
      , "rus" : "обзор"
      , "spa" : "Visión de conjunto"
      , "swa" : "Maelezo ya jumla"
   }
   , "OutputPane#20161109°2132" :
   {    "eng" : "OutputPane"
      , "ara" : "إخراج جزء"
      , "chi" : "输出窗格"
      , "cze" : "Výstup Tabule"
      , "fre" : "Volet de Sortie"
      , "ger" : "Ausgabebereich"
      , "hin" : "उत्पादन फलक"
      , "ita" : "Riquadro di Uscita"
      , "jpn" : "出力ペイン"
      , "rus" : "панель вывода"
      , "spa" : "Panel de Salida"
      , "swa" : "Pato Pane"
   }
   , "Settings#20161109°2133" :
   {    "eng" : "Settings"
      , "ara" : "إعدادات"
      , "chi" : "设置"
      , "cze" : "Nastavení"
      , "fre" : "Paramètres"
      , "ger" : "Einstellungen"
      , "hin" : "सेटिंग्स"
      , "ita" : "Impostazioni"
      , "jpn" : "設定"
      , "rus" : "настройки"
      , "spa" : "Ajustes"
      , "swa" : "Mazingira"
   }
   , "Sitemap#20181217°0235" :
   {    "eng" : "Sitemap"
      , "ara" : "خريطة الموقع"
      , "chi" : "网站地图"
      , "cze" : "Mapa stránek"
      , "fre" : "Plan du site"
      , "ger" : "Seitenverzeichnis"
      , "hin" : "साइटमैप"
      , "ita" : "Mappa del sito"
      , "jpn" : "サイトマップ"
      , "rus" : "Карта сайта"
      , "spa" : "Mapa del sitio"
      , "swa" : "Sitemap"
   }
   , "Slideshow#20161109°2134" :
   {    "eng" : "Slideshow"
      , "ara" : "عرض الشرائح"
      , "chi" : "幻灯片放映"
      , "cze" : "Prezentace"
      , "fre" : "Slide Show"
      , "ger" : "Diashow"
      , "hin" : "स्लाइड शो"
      , "ita" : "Proiezione di Diapositive"
      , "jpn" : "スライドショー"
      , "rus" : "Презентация"
      , "spa" : "Slideshow"
      , "swa" : "Slideshow"
   }
   , "Staticpoints#20110820°1141" :
   {    "eng" : "Staticpoints"
      , "ara" : "نقاط ثابتة"
      , "chi" : "静点"
      , "cze" : "Statické Body"
      , "fre" : "Des Points Statiques"
      , "ger" : "Statische Punkte"
      , "hin" : "्टेटिक अंक"
      , "ita" : "Punti Statici"
      , "jpn" : "静的ポイント"
      , "rus" : "Статические моменты"
      , "spa" : "Puntos Estáticos"
      , "swa" : "Pointi Static"
   }
   , "Styles#20161109°2135" :
   {    "eng" : "Styles"
      , "ara" : "الأساليب"
      , "chi" : "风格"
      , "cze" : "Styly"
      , "fre" : "Styles"
      , "ger" : "Stile"
      , "hin" : "शैलियाँ"
      , "ita" : "Stili"
      , "jpn" : "スタイル"
      , "rus" : "Стили"
      , "spa" : "Estilos"
      , "swa" : "Mitindo"
   }
   , "Tools#20161109°2136" :
   {    "eng" : "Tools"
      , "ara" : "أدوات"
      , "chi" : "工具"
      , "cze" : "Nářadí"
      , "fre" : "Outils"
      , "ger" : "Werkzeuge"
      , "hin" : "उपकरण"
      , "ita" : "Utensili"
      , "jpn" : "ツール"
      , "rus" : "инструменты"
      , "spa" : "Herramientas"
      , "swa" : "Zana"
   }
   , "Treeview#20190209°0643" :
   {    "eng" : "Treeview"
      , "ara" : "عرض الشجرة"
      , "chi" : "树视图"
      , "cze" : "Strom Pohled"
      , "fre" : "Vue d'Arbre"
      , "ger" : "Baumansicht"
      , "hin" : "ट्री व्यू"
      , "ita" : "Visualizzazione ad Albero"
      , "jpn" : "ツリー表示"
      , "rus" : "В виде дерева"
      , "spa" : "Vista de Árbol"
      , "swa" : "Mtazamo wa Mti"
   }
   , "Treeviews#20161109°2137" :
   {    "eng" : "Treeviews"
      , "ara" : "وجهات النظر شجرة"
      , "chi" : "树视图"
      , "cze" : "Pohledy Strom"
      , "fre" : "Vues d'Arbre"
      , "ger" : "Baumansichten"
      , "hin" : "पेड़ विचारों"
      , "ita" : "Viste ad Albero"
      , "jpn" : "ツリービュー"
      , "rus" : "древовидных"
      , "spa" : "Vistas de Árbol"
      , "swa" : "Maoni Mti"
   }
   , "UserManual#20190205°0551" :
   {    "eng" : "User Manual"
      , "ara" : "دليل الاستخدام"
      , "chi" : "用户手册"
      , "cze" : "Uživatelský Manuál"
      , "fre" : "Manuel Utilisateur"
      , "ger" : "Benutzerhandbuch"
      , "hin" : "उपयोगकर्ता पुस्तिका"
      , "ita" : "Manuale Utente"
      , "jpn" : "ユーザーマニュアル"
      , "rus" : "руководство пользователя"
      , "spa" : "Manual de Usuario"
      , "swa" : "Mwongozo wa Mtumiaji"
   }
   , "Vault#20181217°0236" :
   {    "eng" : "Vault"
      , "ara" : "قبو"
      , "chi" : "拱顶"
      , "cze" : "Klenba"
      , "fre" : "Voûte"
      , "ger" : "Gewölbe"
      , "hin" : "मेहराब"
      , "ita" : "Volta"
      , "jpn" : "ボールト"
      , "rus" : "свод"
      , "spa" : "Bóveda"
      , "swa" : "Vault"
   }
};
