﻿
   img 19910805°0712
   objecttype  : img.computergraphics
   title       : Testbild
   summary     : Processed very early GIF file
   policy      : public and redistributable
   license     : none
   copyright   : none
   author      : unknown
   owner       :
   source      : From 1991 GIF collection
   keywords    : colors
   albums      : oldgifs
   objectsize  :
   processing  : Squared from 19910805°0711
   quality     :
   note        :
   ⬞Ω
