﻿/*!
 *  This module shall operate the terminal
 *
 *  id : file 20190205°0111 daftari/jsi/dafterm.js
 *  license : GNU AGPL v3
 *  copyright : © 2019 - 2024 Norbert C. Maier
 *  authors : ncm
 *  status : Under construction
 *  callers :
 *  note :
 */

// Options for JSHint [seq 20190423°0421`10]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf, Terminal, Trekta */

/**
 *  Announce namespace
 *
 *  @id 20190106°0311`23
 *  @c_o_n_s_t — Namespace
 */
Trekta = window.Trekta || {};

/**
 *  Routine namespace opening
 *
 *  @line 20190205°0113
 *  @callers
 *  @const — Namespace
 */
Daf = window.Daf || {};

/**
 *  This namespace is hosting the Terminal accessories
 *
 *  @id 20190205°0121
 *  @callers
 *  @const — Namespace
 */
Daf.Term = Daf.Term || {};

/**
 *  Help string
 *
 *  @id 20210514°0911`02
 *  @type {string} —
 *  @constant —
 */
Daf.Term.sHelp = "Commands: beep, clear, help, spin, stop.";

/**
 *  This method performs the AJAX request
 *
 *  @id 20190209°0321
 *  @note Compare func 20110816°1623 daftari.js::MakeRequest
 *  @callers : Only Daf.Term.runSpin()
 *  @return {undefined} —
 */
Daf.Term.ajaxRequest = function()
{
    'use strict';

    // () Assemble shipment ingredients [seq 20190209°0322]
    var sTargetUrl = Trekta.Utils.s_DaftariBaseFolderRel
                    + '/' + Daf.Const.s_DAFTARI_OFFSET_PHP_SCRIPTS
                     + '/' + 'Go.php'
                      + '?cmd=' + 'spin'
                       ;
    var sMsgToSend = 'Hello spin ..';

    // Get Ajax performer object [seq 20190209°0323]
    // See todo 20190209°0836 'XMLHttpRequest availability'
    var xmlHttp = new XMLHttpRequest();

    // Provide response callback [seq 20190209°0324]
    xmlHttp.onreadystatechange = function()
    {
        if (xmlHttp.readyState === 4)
        {
            Daf.Term.ajaxResponse(xmlHttp.responseText);
        }
    };

    // Finish sending [seq 20190209°0325]
    xmlHttp.open('POST', sTargetUrl);
    xmlHttp.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
    xmlHttp.send(sMsgToSend);
};

/**
 *  This method receives the AJAX response
 *
 *  @id 20190209°0331
 *  @param {string} sResponse — The Ajax result sent from server
 *  @return {undefined} —
 */
Daf.Term.ajaxResponse = function(sResponse)
{
    'use strict';

    // Show response [line 20190209°0333]
    // Remember formerly used RTL char U+1F310 '🌐️' Globe with Meridians [emoji 20210514°1143]
    Daf.Term.t21.print('☞ '+ sResponse);                               // Use U+261E 'White right pointing index' [chg 20220312°1311]

   // Repeat [line 20190209°0335]
   if (Daf.Term.bSpinAutoRun)
   {
      setTimeout( Daf.Term.runSpin, 1234 );
   }
};

/**
 *  This function performs initializing actions after module is loaded
 *
 *  @id 20190205°0131
 *  @todo 20220220°1103 This function fits not with the functionality of this
 *     module, so possibly outsource it to the (only) beneficiary, page tools.html.
 *     Or rather, if the PageTerminal finally works, eliminate the terminal there.
 *     See seq 20220221°0731 in func 20220220°1033 Daf.Mnu.Meo.exe_PageTerminal_3_DoTheJob
 *  @callers Only the onload event, see line 20190205°0151 at the end of this module
 *  @return {undefined} —
 */
Daf.Term.execute = function()
{
   'use strict';

   // Determine and show PHP availability [seq 20210509°1041`02]
   var el = document.getElementById('Is_PHP_Available');
   var sInner = el.innerHTML;
   if ( sInner.indexOf('PHP is available') >= 0 ) {
      Daf.Term.isPhpAvailable = true;
      el.style.backgroundColor = 'Lime';
   }
   else {
      Daf.Term.isPhpAvailable = false;
      el.innerHTML = '&nbsp;PHP is not available&nbsp;';
      el.style.backgroundColor = 'Tomato';
   }

   // Create terminal [seq 20190205°0133] after terminaljs/test.html
   Daf.Term.t21 = new Terminal('Terminal_One');
   Daf.Term.t21.setHeight("250px");
   Daf.Term.t21.setWidth('650px');
   Daf.Term.t21.setBackgroundColor('blue');
   Daf.Term.t21.blinkingCursor(false);
   var eDiv = document.getElementById("Furniture_20190205o0211_Terminal");
   eDiv.appendChild(Daf.Term.t21.html);

   // Launch [seq 20190205°0135]
   Daf.Term.t21.print('This is the Daftari proof-of-concept terminal ..');
   Daf.Term.t21.input(Daf.Term.sHelp, Daf.Term.inputz);
};

/**
 *  This function reacts on user input
 *
 *  @id 20190205°0221
 *  @callers
 *  @param {string} sCmd — The user input
 *  @return {undefined} —
 */
Daf.Term.inputz = function(sCmd)
{
   'use strict';

   // Do job
   var sRet = Daf.Term.interpret(sCmd);
   if (sRet !== '') {
      Daf.Term.t21.print(sRet);
   }

   // Wait for next input
   Daf.Term.t21.input(null, Daf.Term.inputz);                           // null [chg 20210515°1104]
};

/**
 *  This function interprets the user input
 *
 *  @id 20190205°0231
 *  @callers
 *  @todo: Possibly evaluate return value [todo 20190205°0232]
 *  @param {string} sCmd — The command to act on
 *  @return {string} — Seems not be evaluated at all so far
 */
Daf.Term.interpret = function(sCmd)
{
   'use strict';

   var sRet = '';
   switch (sCmd)
   {
      case 'beep' :
         Daf.Term.t21.beep();
         sRet = "Can you hear the beep?";
         break;
      case 'clear' :
         Daf.Term.t21.clear();
         break;
      case 'help' :
         Daf.Term.t21.print(Daf.Term.sHelp);
         break;
      case 'spin' :
         Daf.Term.bSpinAutoRun = true;
         Daf.Term.runSpin();
         sRet = "Spinning ..";                                          // "Scanning ..";
         break;
      case 'stop' :
         Daf.Term.bSpinAutoRun = false;
         sRet = 'Received stop signal';
         break;
      default :
         Daf.Term.bSpinAutoRun = false;
         sRet = 'Error: "' + sCmd + '"';
   }
   return sRet;
};

/**
 *  This function executes the scan/spin command
 *
 *  @id 20190209°0311
 *  @callers
 *  @return {undefined} —
 */
Daf.Term.runSpin = function()
{
   'use strict';
   Daf.Term.ajaxRequest();
};

/**
 *  Flag to run and stop spinner
 *
 *  @id 20190209°0351
 *  @type {boolean} —
 */
Daf.Term.bSpinAutoRun = false;

/**
 *  This variable stores some dummy string
 *
 *  @id 20190205°0141
 *  @callers
 *  @type {string} —
 */
Daf.Term.sDebugWelcome = "[Debug 20190205°0141] Hello, namespace Daf.Term is loaded.";

/**
 *  This variable stores the terminal object — Seems nowhere used?!
 *
 *  @id 20190205°0143
 *  @callers
 *  @type {Object|null} —
 */
Daf.Term.t21 = null;

// Perform initialization [line 20190205°0151]
// note : The given function is only executed, if the script is loaded from the
//    HTML header. It is not executed if this module is loaded via pull-behind.
Trekta.Utils.windowOnloadDaisychain(Daf.Term.execute);

// eof
