/*!
 *  This module serves to test the pull-behind feature
 *
 *  id          : 20160502°0311 daftari/jsi/dafcanary.js
 *  license     : GNU AGPL v3
 *  copyright   : © 2016 - 2024 Norbert C. Maier
 *  authors     : ncm
 *  status      : Under construction
 *  dependencies : dafmenu.js
 *  note        :
 *  callers     :
 */

// Options for JSHint [seq 20190423°0421`02]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf */

/**
 *  This namespace is for testing the pull-behind feature
 *
 *  @id 20160502°0331
 *  @const — Namespace
 */
var DafCanary =
{
    /**
     *  This function serves testing the pull-behind feature
     *
     *  @id 20160502°0341
     *  @status Working
     *  @callers • Button id="id20160503o0134" by direct call
     *      • Button id20160503o0132 via func 20110821°0121 pullScriptBehind()
     *  @return {undefined}
     */
    squeak : function ()
    {
        'use strict';

        // in case of direct squeak call
        if ( Daf.Vars.iTimeOfStartLoading === null ) {
            Daf.Vars.iTimeOfStartLoading = new Date();
        }
        var datNow = new Date();

        // algo with error on the second turnaround
        var iMsStart = Daf.Vars.iTimeOfStartLoading.getMilliseconds();
        var iMsStop = datNow.getMilliseconds();
        var sMillisec = '' + (iMsStop - iMsStart);

        // algo with error on the minute turnaround (what means 'minute'?)
        var iTicksStart = Daf.Vars.iTimeOfStartLoading.getSeconds() * 1000
                         + Daf.Vars.iTimeOfStartLoading.getMilliseconds()
                          ;
        var iTicksStop = datNow.getSeconds() * 1000
                        + datNow.getMilliseconds()
                         ;
        var iMsTicks = iTicksStop - iTicksStart;

        // reset .. good place here?
        Daf.Vars.iTimeOfStartLoading = null;

        // the wanted task
        var sMsg = 'Hello, this is squeaking dafcanary.js.'
                  + '\n\nMilliseconds elapsed = ' + iMsTicks
                   + '\n\nAlgo 1 : ' + iTicksStop + " - " + iTicksStart + " = " + iMsTicks
                    + '\nAlgo 2 : ' + iMsStop + " - " + iMsStart + " = " + sMillisec
                     + '\n\n[msg 20160502°0342]'
                      ;
        alert(sMsg);

        return;
    }
};

/*
    chg 20190422°0121 'Eliminate MB bulk payload'
    done : Eliminating the huge payload comment reduced file dafcanary.js
       from 9.7 KB to 3.5 KB. Just multiply below payload line 1000 times,
       and the old size will be restored.
    note : Having the bulk in a comment would not work with the minified
       module anyway. To make a bulk payload really useful, it had to be
       in some constant or variable, which survives the optimization.
 */

/*
    (No more big since chg 20190422°0121 'Eliminate MB bulk payload')
    Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
 Big payload to delay the loading. Big payload to delay the loading.
*/
