﻿/*!
 *  Daftari — Simplistic static multilingual webpage edit helper
 *
 *  File       : 20120103°1901 daftari/jsi/daftari.js
 *  Version    : v0.7.8.i — 20240509°0911
 *  License    : AGPL v3 (GNU Affero General Public License v3)
 *  Copyright  : © 2011 - 2024 Norbert C. Maier
 */
/**
 *  authors    : ncm
 *  encoding   : UTF-8-with-BOM
 *  callers    : • window.onload event, assigned from the bottom of this module
 *  note       : See ✂ for the faded-in shared bottom part.
 */

// Options for JSHint [seq 20190423°0421`08]
/* jshint laxbreak : true, laxcomma : true */
/* globals flickerStop, XBTooltip */

/**
 *  This object serves as top level namespace
 *
 *  @id 20170923°0421
 *  @see issue 20210417°1411 'GoCloCom warnings with @const annotation'
 *  @c_o_n_s_t — Namespace
 */
var Daf = Daf || {};                                                    // [marker 20210416°1633`11] solved by option '--jscomp_off=checkVars'

/**
 *  This namespace shall collect Daftari constants
 *
 *  @id 20120902°0456
 *  @note Remember ref 20120902°1101 'stackoverflow — js var scope over files'
 *  @c_o_n_s_t — Namespace
 */
Daf.Const = Daf.Const || {};

Daf.Const.s_VERSION_ATTRIB = 'Ongoing';                                 // 'beta';
Daf.Const.s_VERSION_NUMBER = 'v0.7.8.i';                               // KEEP IN SYNC with on-top and with dafutils.js
Daf.Const.s_VERSION_TIMEST = '20240509°0911'

Daf.Const.b_Toggle_FALSE = false;                                       // use this instead plain 'false' [const 20141118°0234]
Daf.Const.b_Toggle_TRUE = true;                                         // use this instead plain 'true' [const 20141118°0235]

// Provide the list of the supported languages [const 20140605°1517]
// note : Keep in sync with the languages array in daflingos.js seq 20120831°1311
// todo : Somehow merge those two languages arrays (see todo 20140605°1521)
Daf.Const.aLang_Codes = [ 'eng', 'ara', 'chi', 'cze', 'dan', 'fin', 'fre'
                         , 'ger', 'gre', 'hin', 'ita', 'jpn', 'nep', 'pol'
                          , 'rus', 'spa', 'swa', 'tur', 'all'
                           ];

// Language flags // [const 20140605°1515]
Daf.Const.aLang_DefsiFlagon = '20120831o1321.flag-of-all.v0.x0021y0014.png';

// Language flags // [const 20190422°0612]
Daf.Const.b_IncludeExperimentalDropdownMenu = true;

// Cookie name for self-closing feature [prop 20150319°0432]
Daf.Const.sCookieName_pinLoginWindow = "pinLoginWindow";

// Cookie name for autoflick feature
Daf.Const.sCookieName_autoflick = "autoflick";                          // [prop 20150319°0438]

// Internal path
Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_MAIN = 'jsi/imgs23/20100814o021302.logo-edit-small-v015.png';  // [const 20150209°0943]

// Internal path [const 20150211°0321]
Daf.Const.sFILE_DAFTARI_ICON_NOTEBOOK_NARROW = 'jsi/imgs23/20100814o0214.logo-edit-small-narrow.png';

// Define the Help location [seq 20150211°0230]
// note : This tells the Help pages located *relative* to daftari base folder.
Daf.Const.sFILE_PAGE_DIALOG         = '/panels/dialog.html';            // [const 20150211°0232]
Daf.Const.sFILE_PAGE_EDIT           = '/panels/edit.html';              // [const 20150211°0231]
Daf.Const.sFILE_PAGE_HELP           = 'docs/help.html';                 // [const 20150211°0421]
Daf.Const.sFILE_PAGE_LOGIN          = 'panels/login.html';              // [const 20150211°0322]
Daf.Const.sFILE_PAGE_SETTINGS       = 'panels/settings.html';           // [const 20150211°0323]
Daf.Const.sFILE_PAGE_TOOLS          = 'panels/tools.html';              // [const 20150211°0324]
Daf.Const.sFOLDER_FLAGS             = 'jsi/imgs24/';                    // [const 20150211°0251]

// Adjust this if Daftari folder structure changes [const 20160502°0212]
Daf.Const.s_DAFTARI_OFFSET_PHP_SCRIPTS = 'php';

// Feature IDs
Daf.Const.sKey_DAFTARI_NOTEBOOK_ICON_4 = 'DaftariMainMenuIcon_20120827o0314';  // Use this as ID for div [const 20210518°1631]
Daf.Const.sKey_LINGOS_SELECTION_AREA_3 = 'LingoSelectionArea_20120901o2023';  // Use this as ID for a div [const 20150209°0942]

// Text bricks
Daf.Const.sTb_1Cr = '\r\n';                                             // [const 20141118°0243] remember issue 20180322°0331 'use cr or crlf?'
Daf.Const.sTb_2Cr = '\r\n\r\n';                                         // [const 20141118°0244]
Daf.Const.sTb_3Cr = '\r\n\r\n\r\n';                                     // [const 20201218°1911]
Daf.Const.sTb_InSpn  = '        ';                                      // [const 20141118°0245] beautify html strings
Daf.Const.sTb_InTxt  = '               ';                               // [const 20141118°0246] beautify html strings

/**
 *  This namespace shall collect the Daftari core functions
 *
 *  @id 20190404°0141
 *  @c_o_n_s_t — Namespace
 */
Daf.Core = {};                                                          // [marker 20210416°1633`11] solved by option '--jscomp_off=checkVars'

/**
 *  This namespace shall collect AJAX related functions
 *
 *  @id 20170902°1041
 *  @callers • func 20120830°0451 Daf.Mnu.Edt.EditFinishTransmit
 *  @c_o_n_s_t — Namespace
 */
Daf.Core.Ajax = {};

/**
 *  This function processes the response from a preceeding AJAX request
 *
 *  @id 20110816°1624
 *  @status Not so nice
 *  @todo Solve issue 20170902°1441 'Edit silently fails'
 *  @ref 20110816°1421 'First simple ajax example'
 *  @note Remember issue 20110821°1121 'IE textarea.innerHTML susceptibility'
 *  @note Remember issue 20110821°1135 'Wrap style values'
 *  @callers only 20110816°1623 MakeRequest
 *  @param {string} sResponse — The text coming from the server
 *  @return {undefined} —
 */
Daf.Core.Ajax.HandleResponse = function(sResponse)
{
    'use strict';

    // (1) Determine output medium
    var el = document.getElementById('textarea_intermediate');

    // (2) Mangle response
    sResponse = sResponse.replace(/</g, '&lt;');                        // g = replace all hits, not only the first
    sResponse = sResponse.replace(/>/g, '&gt;');

    // (3) Display response
    el.innerHTML = sResponse;
    el.style.visibility = 'visible';                                    // Remember issue 20110821°1135 'wrap style values'
};

/**
 *  This function performs an AJAX request
 *
 *  @id 20110816°1623
 *  @status Nice
 *  @see ref 20110816°1423 'W3C → XMLHttpRequest Level 1'
 *  @see ref 20110816°1421 'First simple ajax example'
 *  @note With Chrome, below xmlHttp.send() will not work with local files,
 *      only via HTTP. See issue 20150516°0531 'Chrome cannot load local files'.
 *  @note Compare func 20190209°0321 dafterm.js::ajaxRequest
 *  @callers • EditFinishTransmit • execPage07DemoPSHDP • execPage08DemoDomDoc
 *            • execPage09DemoDomdocGenerate
 *  @param {string} sTargetUrl — The URL to request
 *  @param {string} sTransmit — The payload to be sent
 *  @return {undefined} —
 */
Daf.Core.Ajax.MakeRequest = function(sTargetUrl, sTransmit)
{
    'use strict';

    // Provide the XmlHttp object [seq 20160611°0441]
    // See todo 20190209°0836 'XMLHttpRequest availability'
    var xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = function() {
       if (xmlHttp.readyState === 4) {
           Daf.Core.Ajax.HandleResponse(xmlHttp.responseText);
       }
    };

    // Send request [seq 20110816°1721]
    xmlHttp.open('POST', sTargetUrl);
    xmlHttp.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
    xmlHttp.send(sTransmit);
};

/**
 *  (B) This function is Daftari entry point after page onLoad event, working off the feature list
 *
 *  @id 20110813°2221
 *  @callers Only • script level via windowOnloadDaisychain
 *  @return {undefined} —
 */
Daf.Core.daftari = function()
{
    'use strict';

    // Debug
    var sDbg = " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~"
              +"\n[Dbg 20150321°0521 ⇘⇘] Function daftari() is starting."
               + '\n &nbsp; • Trekta.Utils.s_DaftariBaseFolderRel = ' + Trekta.Utils.s_DaftariBaseFolderRel
                + "\n ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~"
                 ;
    Trekta.Utils.outDbgMsg(sDbg);

    // (F) Work off the feature list
    // The execution order might be still improved [note 20190418°0121]

    // (F.4) Provide some prerequisites [line 20150321°0531]
    Daf.Dspat.provideCornerstonePathes();

    // (F.5) Work off cakecrumbs [line 20190413°0733]
    // See issue 20200103°0252 'Chrome → AJAX fails with file protocol'
    if (true) { // false true // experiment 20200103°0221
        Daf.Dspat.process_cake1ignit();
    }

    // (F.1) Build and inject the notebook icon [line 20190417°0443]
    Daf.Dspat.process_1_MainMenu();

    // (F.3) Process possibly wanted dashboard features [line 20190403°0511]
    Daf.Dspat.process_dashboard();

    // (F.2) Process page languages [line 20150210°1413]
    // Remember ref 20150210°1412 'Stackoverflow → Check if function exists'
    Daf.Lngo.onloadLangSplit(null);

    // (F.6) Work off blog feature line 20190417°0511]
    Daf.Dspat.process_blog();

    // (F.7) Process any slideshow [line 20190417°0533]
    Daf.Dspat.process_slideshow();

    // (F.8) Process any FadeInFiles elements [line 20190404°0811]
    Daf.Dspat.process_fadeinfiles();

    // (F.10) Process any Fancytree elements [line 20190403°0211]
    Daf.Dspat.process_fancytree();

    // (F.11) Process any CanvasGear elements [line 20190404°0907]
    Daf.Dspat.process_canvasgear();

    // (F.12) Process the cookie display feature [line 20170901°1221]
    Daf.Dspat.process_displayCookies();

    // (F.13) Save and print the currently-not-used RtFiture class [line 20190417°0521]
    Daf.Dspat.process_rtFitursDemo();

    // (F.14) Provide autoflick facility [line 20190420°0241]
    Daf.Utlis.Flick.autoflickAvail();

    // Debug — Message indicating that all has worked
    sDbg = " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~";
    sDbg += "\n[Dbg 20150326°0441 ⇖⇖] Function daftari() finished.";
    sDbg += "\n &nbsp; • Daf.Sitmp.Config.sRelPath_FromPage_ToSiteBase = " + Daf.Sitmp.Config.sRelPath_FromPage_ToSiteBase;
    sDbg += "\n &nbsp; • Daf.Sitmp.Config.sRelPath_FromPage_ToSitemap = " + Daf.Sitmp.Config.sRelPath_FromPage_ToSitemap;
    sDbg += "\n Bye.";
    sDbg += "\n ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~";
    Trekta.Utils.outDbgMsg(sDbg);

};                                                                      // End Daf.Core.daftari()

/**
 *  This function summarizes the code to be executed on script level
 *
 *  @id 20190411°0131
 *  @callers : Only • script level
 *  @return {undefined} —
 */
Daf.Core.executeScriptLevel = function ()
{
    'use strict';
    var s23 = '';

    // Is the present code already loaded? [condi 20190408°0113]
    // Explanation. Detection is done by using the namespace 'Daf.Utlis' as a
    // canary. If this present namespace is already loaded, then it must have
    // been done from the concatenated daftari.min.js. And then we know, it must
    // be the minified JavaScript file '*.min.js'. This information is used for
    // later pull-behind, where we will also pull minfied files, not plain ones.
    if (Daf.Utlis) {
        Trekta.Utils.bUseMinified = true;
    }

    // Output some startup debug information [line 20111227°1211]
    // note : The function is called before DOM is readily available,
    //     soe some operations miay fail, e.g. document.getElementById().
    s23 = '[Dbg 20111227°1211] *** Green box before onload ***'
         + ' bUseMinified = ' + Trekta.Utils.bUseMinified
          ;
    Trekta.Utils.outDbgMsg(s23);

    // (X) Explore location [seq 20160501°1641]
    //if (Trekta.Utils.s_DaftariBaseFolderAbs === undefined) {                                 // [condi 20240428°1845] Try workaround issue 20240428°1831 'Base folder broken'
    //if (Trekta.Utils.s_DaftariBaseFolderAbs === '') {                                        // [condi 20240428°1845] Try workaround issue 20240428°1831 'Base folder broken'
    if (Trekta.Utils.s_DaftariBaseFolderAbs.length < 1) {                                      // [condi 20240428°1845] Try workaround issue 20240428°1831 'Base folder broken'
       var sJs = Trekta.Utils.bUseMinified ? '/jsi/daftari.min.js' : '/jsi/daftari.js';
       var sBaseAbs = Trekta.Utils.retrieveDafBaseFolderAbs(sJs);
       Trekta.Utils.s_DaftariBaseFolderAbs = sBaseAbs;
    }

    // Load daftari.css [line 20190411°0157]
    // See issue 20190413°0111 'flash of unstyled content'
    Daf.Core.executeScriptLevel_1_pullCss();

     // Load scripts rat-tail [line 20190411°0213]
    Daf.Core.executeScriptLevel_2_pullRatTail();

    // Registers daftari to the onload event [line 20160503°0411]
    Trekta.Utils.windowOnloadDaisychain(Daf.Core.daftari);
};

/**
 *  This function shall load CSS files
 *
 *  @id 20190411°0151
 *  @todo If func 20190423°0111 pullCss is ready, use it
 *  @note Sorrily, styleslocal.css can not be pulled yet, but only after
 *      the sitemap function has determined the location of sitmapdaf.js
 *  @callers : Only • func 20190411°0131 executeScriptLevel
 *  @return {undefined} —
 */
Daf.Core.executeScriptLevel_1_pullCss = function ()
{
    'use strict';

    // Get path [line 20190411°0153]
    var sCssFile = Trekta.Utils.s_DaftariBaseFolderAbs + '/css/daftari.css';

    // Avoid multiple loadings [seq 20190411°0154 (after algo 20110820°2042)]
    // note : From the stylesheets given given in the HTML code, only
    //    those will be detected, which are given before the daftari.js
    //    script line. Those lines behind are not yet read by the browser.
    var re = new RegExp('\/daftari.css$', "g");
    var eles = document.getElementsByTagName("link");
    if (eles && eles.length > 0) {
        for (var i = 0; i < eles.length; i++) {
            if (eles[i].href.match(re)) {
                return;                                                 // Do not load again
            }
        }
    }

    // Insert the stylesheet [seq 20190412°0223]
    var head = document.getElementsByTagName('head')[0];
    var lnk23 = document.createElement('link');
    lnk23.type = 'text/css';
    lnk23.href = sCssFile;
    lnk23.rel = 'stylesheet';
    head.appendChild(lnk23);

    // [line 20190420°0111]
    Daf.Vars.bToggle_FoucFighter = Trekta.Utils.getCookieBool('reducePageLoadFlash', false);

    // Experiment [seq 20190413°0121] to be complemented by seq 20190413°0123
    // Try fight issue 20190413°0111 'flash of unstyled content'
    if ( Daf.Vars.bToggle_FoucFighter ) {
        var styl23 = document.createElement('style');
        styl23.type = 'text/css';
        styl23.innerHTML = 'body { display:none; }';
        head.appendChild(styl23);
    }
};

/**
 *  This function loads the rat tail of JavaScript modules in parallel,
 *   if all are loaded, executes callback ..
 *
 *  @id 20190404°0511
 *  @see summary 20190404°0645 'the pull-behind proceedings'
 *  @callers : Only • func 20190411°0131 executeScriptLevel
 *  @return {undefined} —
 */
Daf.Core.executeScriptLevel_2_pullRatTail = function ()
{
    'use strict';

    // [condition 20190412°0111]
    if ( Trekta.Utils.bUseMinified ) {

        // (X) Explore location [seq 20160501°1642]
        //  If not minified, this must be done after the rat-tail was loaded
        var sBaseRel = Daf.Utlis.retrieveDafBaseFolderRel ('/jsi/daftari.min.js');
        Trekta.Utils.s_DaftariBaseFolderRel = sBaseRel;

        return;
    }

    // () Prepare mandatory parallel-pull [seq 20190404°0123]
    // This are those, which are combined into daftari.min.js by build.py
    var sBaseAbs = Trekta.Utils.s_DaftariBaseFolderAbs;
    var aSc = [ sBaseAbs + '/jsi/dafutils.js'
               , sBaseAbs + '/jsi/dafmenu.js'
                , sBaseAbs + '/jsi/daflingos.js'
                 , sBaseAbs + '/jsi/dafdispatch.js'
                  , sBaseAbs + '/jsi/dafsitmap.js'
                   , sBaseAbs + '/jsi/gallery-js-autoflick.js'          // chg 20210416°0913`01 add line
                    , sBaseAbs + '/jsi/gallery-js-xbtooltip.js'         // chg 20210416°0913`02 add line
                     , sBaseAbs + '/jslibs/tigra_tree_menu/tree.js'     // Remember solved issue 20210516°0932 'new tree() in page fails'
                      ];

    // [seq 20190411°0223]
    // If we inline the function below, NetBeans Navigator will no more work
    // Remember issue 20190411°0221 'NetBeans Navigator pane stays empty'
    var cbk_retrieveDafBaseFolderRel = function()                       // 'paral_callback'
    {
        // (X) Explore location [seq 20160501°1643]
        var sJs = Trekta.Utils.bUseMinified ? '/jsi/daftari.min.js' : '/jsi/daftari.js';
        var sBaseRel = Daf.Utlis.retrieveDafBaseFolderRel(sJs);
        Trekta.Utils.s_DaftariBaseFolderRel = sBaseRel;
    };

    // Build the jobs parameter [seq 20190404°0312]
    // note : Provide loading state flag fore each individual. The continuation
    //    is done, when all flags are set. Like synchronisation with threading.

    // Alternative which also IE11 understands [seq 20190411°0723]
    // See issue 20190411°0721 'IE9..11 rejects bracketed dynamic properties'
    //    but it understands defineProperty
    var oJobs = {};
    Object.defineProperty ( oJobs, Daf.Core.sPullJob_ParalInfo
                           , { value : aSc                              // The module set
                            ,  writable : true                          // Not sure we need this true
                             , enumerable : true                        // Not sure we need this true
                              , configurable : true                     // Not sure we need this true
                               });
    // This prepared flags are set true if respective module was loaded. If they
    //  all are true, the module set is finished, and the callback can be fired
    Object.defineProperty ( oJobs, Daf.Core.sPullJob_ParalReady
                           , { value : [false, false, false, false, false]
                            ,  writable : true
                             , enumerable : true
                              , configurable : true
                               });
    // Set the one callback fired after the complete modules set is loaded
    Object.defineProperty ( oJobs, Daf.Core.sPullJob_ParalCallback
                           , { value : cbk_retrieveDafBaseFolderRel
                            ,  writable : true
                             , enumerable : true
                              , configurable : true
                               });

    // () Perform chain-pull-behind [seq 20190404°0125]
    // Compare note 20220220°1051 'About pull-behind-chaining alternative technique'
    for (var i = 0; i < aSc.length; i++) {
        Trekta.Utils.pullScriptBehind ( aSc[i]
                                       , Daf.Core.pullBehind_onLoad
                                        , Daf.Core.pullBehind_onError
                                         , oJobs
                                          );
    }
};

/**
 *  This event handler reacts if script failed loading
 *
 *  @id 20190404°0211
 *  @note Process todo 20190404°0131 'Finetune chain-pull-behind'
 *  @callers Only • via pull-behind
 *  @param {string} sScript — ..
 *  @param {Object} oJobs — ..
 *  @return {undefined} —
 */
Daf.Core.pullBehind_onError =  function(sScript, oJobs)
{
    'use strict';
    console.error('[Error 20190404°0213] Failed to load "' + sScript + '" ' + oJobs);
};

/**
 *  This event handler fires if script is loaded
 *
 *  @id 20190404°0221 (similar 20190403°0151)
 *  @note Process todo 20190404°0131 'Finetune chain-pull-behind'
 *  @callers : Only • browser via pull-behind
 *  @param {string} sScript — The script to load, as pull-behind has got it
 *  @param {Object} oJobs — Something forwarded from the original pull-behind caller
 *  @param {type} bWasAlreadyLoaded — Flag telling whether script was already loaded
 *            or is newly loaded. Not used yet, but I want provide this information
 *  @return {undefined} —
 */
Daf.Core.pullBehind_onLoad = function ( sScript
                                       , oJobs
                                        , bWasAlreadyLoaded
                                         )
{
    'use strict';

    // Prologue [seq 20190404°0223]
    var bDebug_OnLoadHandler = false;                                   // false true
    if (bDebug_OnLoadHandler) {
        var sMsgProps = '';
        for (var sProp in oJobs) {
            if (oJobs.hasOwnProperty(sProp)) {
                var sVal = '*function*';
                if (typeof oJobs[sProp] !== 'function') {
                    sVal = oJobs[sProp];
                }
                sMsgProps += '\n' + sProp + ' = ' + sVal;
            }
        }
        alert ( 'This is callbackOnload' + '\n\nscript = ' + sScript
               + '\nbWasAlreadyLoaded = ' + bWasAlreadyLoaded + '\n' + sMsgProps
                );
    }

    // Hm ..  [seq 20190404°0225]
    if ( typeof oJobs !== 'object' ) {
        return;                                                         // Hm ..
    }

    // Process the chain [seq 20190404°0227]
    // note : Here an array were more comfortable to process than an object,
    //    because then we had a guaranteed numerical order to work with.
    if (( 'job_2' in oJobs ) || ( 'job_3' in oJobs ) || ( 'job_4' in oJobs ) || ( 'job_5' in oJobs )) {

        // This were easier with an array than with an object [seq 20190404°0229]
        var sJob = '';
        if ( 'job_2' in oJobs ) { sJob = 'job_2'; }
        else if ( 'job_3' in oJobs ) { sJob = 'job_3'; }
        else if ( 'job_4' in oJobs ) { sJob = 'job_4'; }
        else { sJob = 'job_5'; }

        // Process the job object [seq 20190404°0231]
        var sScriptToLoad = oJobs[sJob];
        delete oJobs[sJob];

        // () Perform batch-pull-behind [seq 20190404°0233]
        Trekta.Utils.pullScriptBehind ( sScriptToLoad
                                       , Daf.Core.pullBehind_onLoad
                                        , Daf.Core.pullBehind_onError
                                         , oJobs
                                          );
        return;
    }
    else if ( Daf.Core.sPullJob_No6 in oJobs ) { // 'job_6'
        // Paraonia [seq 20190404°0235]
        if (typeof oJobs[Daf.Core.sPullJob_No6] === 'function') {       // 'job_6'
            // The final continuation call [line 20190404°0237]
            oJobs[Daf.Core.sPullJob_No6]();                             // 'job_6'
        } else {
            // Fatal error [line 20190404°0239]
        }
    }
    else if ( Daf.Core.sPullJob_ParalReady in oJobs ) { // 'paral_ready'

        // Process strategy two, parallel-pull [seq 20190404°0321]
        // See issue 20190404°0341 'parallel-pull remaining timing issue'

        // Determine index [seq 20190404°0322]
        var iNdx = -1;
        for (var i1 = 0; i1 < oJobs[Daf.Core.sPullJob_ParalInfo].length; i1++) {  // 'paral_info'
            if ( sScript === oJobs[Daf.Core.sPullJob_ParalInfo][i1] ) {  // 'paral_info'
                iNdx = i1;
                break;
            }
        }
        // Paranoia [seq 20190404°0323]
        if (iNdx < 0) {
            // fatal error
            return;
        }

        // [line 20190404°0324]
        oJobs[Daf.Core.sPullJob_ParalReady][iNdx] = true;               // 'paral_ready' // This script is loaded

        // All jobs done? [seq 20190404°0325]
        var bAllJobsDone = true;
        for (var i2 = 0; i2 < oJobs[Daf.Core.sPullJob_ParalReady].length; i2++) {  // 'paral_ready'
            if ( ! oJobs[Daf.Core.sPullJob_ParalReady][i2] ) {          // 'paral_ready'
                bAllJobsDone = false;
                break;
            }
        }

        // Finish? [seq 20190404°0326]
        if (bAllJobsDone) {

            // The final continuation call [seq 20190404°0327]
            // note : Consider to perform this call via a setTimeout(0),
            //  so this function were ready immediately. Hm ..
            if (typeof oJobs[Daf.Core.sPullJob_ParalCallback] === 'function') {  // 'paral_callback' // Introduced for script-level-pull [line 20190404°0523]
                oJobs[Daf.Core.sPullJob_ParalCallback]();               // 'paral_callback'
            }
        }
        return;
    }
    else {
        // Fatal error [line 20190404°0241]
    }
};

/**
 *  This function shall load any CSS files
 *
 *  @id 20190423°0111
 *  @see summary 20190411°0111 'load css file and maintain stlyes'
 *  @callers : So far only • func 20190422°0631 Daf.Mnu.Tfd.dropMenu1Load
 *      not yet • func 20190411°0151 Daf.Core.executeScriptLevel_1_pullCss
 *  @param {string} sPath — The path to the CSS file to load
 *  @param {string} sFile — The CSS file, this part is used as the canary string
 *  @return {undefined} —
 */
Daf.Core.pullCss = function (sPath, sFile)
{
    'use strict';

    // Avoid multiple loading [seq 20190411°0154 (after algo 20110820°2042)]
    // note : From the stylesheets given given in the HTML code, only
    //    those will be detected, which are given before the daftari.js
    //    script line. Those lines behind are not yet read by the browser.
    var sCanary = sFile.replace(/\//g, '\/') + '$';
    var re = new RegExp(sCanary, "g");
    var eles = document.getElementsByTagName("link");
    if (eles && eles.length > 0) {
        for (var i = 0; i < eles.length; i++) {
            if (eles[i].href.match(re)) {
                return;                                                 // Do not load twice
            }
        }
    }

    // Insert the stylesheet [seq 20190412°0223]
    var head = document.getElementsByTagName('head')[0];
    var lnk23 = document.createElement('link');
    lnk23.type = 'text/css';
    lnk23.href = sPath + '/' + sFile;
    lnk23.rel = 'stylesheet';
    head.appendChild(lnk23);
};

/**
 *  Helper constant for pull-behind
 *
 *  @id 20190408°0151
 *  @type {string} —
 */
Daf.Core.sPullJob_No6 = 'job_6';

Daf.Core.sPullJob_ParalCallback = 'paral_callback';                     // [var 20190408°0152]

Daf.Core.sPullJob_ParalInfo = 'paral_info';                             // [var 20190408°0153]

Daf.Core.sPullJob_ParalReady = 'paral_ready';                           // [var 20190408°0154]

/**
 *  This namespace stores some (more or less globally used) variables
 *
 *  @id 20160504°0131
 *  @callers
 *  @c_o_n_s_t — Namespace
 */
Daf.Vars = Daf.Vars || {};

// [var 20160504°0133]
Daf.Vars.bGlobalPageStatusEditModeIsOn = false;

// Relevant for inline editing only [var 20160504°0135]
Daf.Vars.bGlobalPageStatusEditingIsOngoing = false;

/**
 *  Switch the feature on or off. Shall be true while normal operation
 *
 *  @id 20190413°0125
 *  @type {boolean} —
 */
Daf.Vars.bToggle_BakeStaticCakecrumbs = true;

/**
 *  Experimental flag. Hides the body while loading until dynamic CakeCrumbs
 *  are inserted. Page load still flashes, but not so nasty with a down-slide.
 *  Dangerous: User will see nothing if JavaScript fails to restore visibility.
 *  Feature become obsolete by the bake-static-cakecrumbs feature.
 *
 *  This feature is dismantled(?). The code is left in memory of this
 *  technique, and for possible reuse for eventual other purposes.
 *
 *  @id 20190413°0125 'fouc fighter flag'
 *  @see issue 20190413°0111 'flash of unstyled content'
 *  @type {boolean} —
 */
Daf.Vars.bToggle_FoucFighter = false;                                   // false true

// [var 20160504°0137]
Daf.Vars.sGlobalParamCloseMode = '';

// Used only with dafcanary.js [var 20181229°1951]
Daf.Vars.iTimeOfStartLoading = new Date();

/**
 *  This namespace shall represent Daftari for functions directly called in pages
 *
 *  @id 20190404°0731
 *  @note 20190411°0621 : With "var Daftari = window.Daftari || {};",
 *           the members will not show up in NetBeans Navigator
 *  @c_o_n_s_t — Namespace
 */
var Daftari = Daftari || {};

// Why is this method not shown in NetBeans Navigator pane? [note 20190411°0545]
/**
 *  This event handler processes tooltip requests. This is a kind
 *   of wrapper function for gallery-js-xbtooltips.js::XBT()
 *
 *  @id 20190404°0741
 *  @todo 20190404°0742 : Daftari.XBT was placed quickly into daftari.js.
 *     It is not clear whether here is the best place. Perhaps dafmenu.js
 *     or any other smaller default-loaded script were also suited and would
 *     result in better lines-per-script balancing.
 *  @callers • manually written onmouseover events, e.g. in page 20111224°2311 moonwalk.html
 *  @param {} paraXbt1 — The calling element ('this')
 *  @param {} paraXbt2 — The ID of the text element to show
 *  @return {undefined} —
 */
Daftari.XBT = function(paraXbt1, paraXbt2)
{
    'use strict';
    //console.log('[Log 20190404°0747] Tooltip wanted ..');

    // Feature detect [condition 20190404°0743]
    if ( typeof XBTooltip !== 'undefined' ) {
        // Directly forward the wanted call [seq 20190404°0745]
        XBTooltip( paraXbt1, paraXbt2 );
    }
    else {
        // Indirect forward, pull script [seq 20190404°0744]
        var sLoc = Trekta.Utils.s_DaftariBaseFolderAbs;
        var sScriptToLoad = sLoc + '/jsi/gallery-js-xbtooltip.js';
        Trekta.Utils.pullScriptBehind ( sScriptToLoad
                                       , ( function() { XBTooltip( paraXbt1, paraXbt2 ); } )
                                        , Daf.Core.pullBehind_onError
                                         , null                         // oJobs
                                          );
    }
};

// Why is this method not shown in NetBeans Navigator pane? [note 20190411°0545]
/**
 *  This function is the XBT wrapper allowing a third argument for separate purpose.
 *
 *  @id 20110511°1321
 *  chg 20190404°0755 Function shifted here from gallery-js-xbtooltips.js
 *  @note The autoflick stop link is sometimes difficult to click,
 *          so we fire it not only by clicking but also by mouseover.
 *  @callers • PHP generated onmouseover events, e.g. in page 20110428°0222 PageAlbum.php
 *  @param {element} paraXbt1 — The calling element ('this')
 *  @param {element} paraXbt2 — The ID of the text element to show
 *  @param {string} sAdditionalAction — The additional autoflick command (FAST, SLOW, STOP, cook)
 *  @return {undefined} —
 */
Daftari.XBT2 = function (paraXbt1, paraXbt2, sAdditionalAction)
{
    'use strict';

    Daftari.XBT(paraXbt1, paraXbt2);
    switch (sAdditionalAction)
    {
        case 'FAST' : break;
        case 'SLOW' : break;
        case 'STOP' : flickerStop(); break;
        case 'cook' : break;
        default : break;
    }
};

// (Closure Compiler complains 'Misplaced function annotation ..'
// /**
//  *  This is a polyfill for Array.prototype.indexOf, which is not known by IE8.
//  *  @id seq 20190407°0313 'polyfill for array.indexOf'
//  *  @see ref 20190407°0311 'MDN → Array​.prototype​.indexOf()'
//  *  @callers E.g. • 3214 ..
//  *  @param Object {Object} ...
//  *  @param max {number} — ...
//  *  @param min {number} — ...
//  *  @return {number} —
//  */
if (! Array.prototype.indexOf) Array.prototype.indexOf = ( function(Object, max, min) {
    "use strict";
    return function indexOf(member, fromIndex) {
        if (this === null || this === 'undefined') {                    // [note 20210517°1111`32] Think about possible reordering?
             throw TypeError("Array.prototype.indexOf called on null or undefined");
        }

        var that = Object(this), Len = that.length >>> 0, i = min(fromIndex | 0, Len);

        if (i < 0) { i = max(0, Len+i); } else if (i >= Len) { return -1; }

        if (member === void 0) { for(; i !== Len; ++i) if (that[i] === void 0 && i in that) return i;  // undefined
        } else if (member !== member) { for (; i !== Len; ++i) if (that[i] !== that[i]) return i;  // NaN
        } else for (; i !== Len; ++i) if (that[i] === member) return i;  // All else

        return -1;                                                      // If the value was not found, then return -1
    };
})(Object, Math.max, Math.min);

/**
 *  Add a getElementsByClassName function if the browser doesn't have one
 *  Limitation : Only works with one class name
 *  Copyright : Eike Send http://eike.se/nd
 *  License : MIT License
 *
 *  @id seq 20190407°0341 'polyfill getElementsByClassName'
 *  @see ref 20190407°0331 gist.github.com/eikes/2299607
 *  @note The IE6/IE7 part is cut out as opposed to the original
 */
if (! document.getElementsByClassName) {

  /**
   *  Polyfill for getElementsByClassName()
   *
   *  @ note 20210416°1735 This is for IE8. Somewhen we will switch it off, but not today.
   *  @param {Object} search — Search
   *  @return {Array} — // {Array|Node.getElementsByClassName.results|document.getElementsByClassName.results|Document.getElementsByClassName.results|HTMLDocument.getElementsByClassName.results}
   */
    document.getElementsByClassName = function(search) {
        'use strict';
        var d = document, elements, pattern, i, results = [];
        if (d.querySelectorAll) {                                       // IE8
          return d.querySelectorAll("." + search);
        }
        elements = d.getElementsByTagName("*");
        pattern = new RegExp("(^|\\s)" + search + "(\\s|$)");
        for (i = 0; i < elements.length; i++) {
            if ( pattern.test(elements[i].className) ) {
                results.push(elements[i]);
            }
        }
        return results;
    };
}


// ~ ✂ ~ ~ ~ ~ ~ ~ ~ Area 20190106°0307 Start ~ ~ ~ ~ ~ ~ ~ ~ ~
/**!
 *  This area Trekta.Utils holds low level functions to be pasted into standalone scripts
 *
 *  File        : 20190105°1717 daftari/jsi/trektautils.js
 *  Version     : v0.2.9 — 20240428°1911
 *  Copyright   : © 2019 - 2024 Norbert C. Maier
 *  License     : GNU AGPL v3
 *  Content     : Overview on Trekta.Utils. objects
 *    • func 20110510°2127 — getCookie(sCookieName)
 *    • func 20120828°2021 — getCookieBool(sCookieName, bDefault)
 *    • func 20190420°0311 — getCookieInt(sCookieName, iDefault)
 *    • func 20110820°1741 — getFileNameFull()
 *    • func 20120827°1511 — getFilenamePlain()
 *    • func 20140926°1431 — htmlEscape(sHtml)
 *    • func 20160503°0231 — isScriptAlreadyLoaded(sWantedScript)
 *    • func 20150321°0311 — outDbgMsg(sOut)
 *    • func 20150323°0321 — outDbgMsg_GuaranteeParentElement()
 *    • func 20110821°0121 — pullScriptBehind(sScLoad, callbackOnLoad, callbackOnError, oJobs)
 *    • func 20190405°0341 — pullScript_onload(sScript, cbkCustom)
 *    • func 20190417°0311 — readTextFile2(sUrl, cbkLoad, cbkFail)
 *    • func 20190405°0231 — ajax3Send(sMethod, sUrl, sPayload, cbkLoad, cbkFail)
 *    • func 20110820°2041 — retrieveDafBaseFolderAbs(sScCanary)
 *    • func 20201203°1445 — sConfine(sOrig, iMaxLen)
 *    • func 20110510°2126 — setCookie(sCookieName, sCookieValue, iExpirationDays)
 *    • func 20160614°0331 — windowOnloadDaisychain(funczion)
 *    • var 20190405°0345 — aPulled = [];
 *    • var 20160622°0221 — bIs_Browser_Chrome = ( navigator.userAgent.match(/Chrome/) ? true : false );
 *    • var 20190417°0217 — bIs_Browser_Edge = ( navigator.userAgent.match(/Edge/) ? true : false );
 *    • var 20150209°0941 — bIs_Browser_Explorer
 *    • var 20160624°0121 — bIs_Browser_Firefox = ( navigator.userAgent.match(/Firefox/) ? true : false );
 *    • var 20190107°0821 — bIs_Browser_Opera = ( navigator.userAgent.match(/(Opera)|(OPR)/) ? true : false );
 *    • var 20190311°1521 — bShow_Debug_Dialogs = false;
 *    • var 20190407°0121 — bToggle_FALSE = false;
 *    • var 20190407°0122 — bToggle_TRUE;
 *    • var 20190408°0115 — bUseMinified = false;
 *    • var 20150516°0451 — InitialMessageCache = Array();
 *    • var 20160618°0421 — sFurniture_OutputArea_Id = "id20150321o0231_StandardOutputDiv";
 *    • var 20160501°1622 — s_DaftariBaseFolderAbs = '';
 *    • var 20160501°1623 — s_DaftariBaseFolderRel = '';
 *    • func 20140926°0641 — CmdlinParser = (function(){})();
 */
/**
 *  Usage. Propagate this area via cut-n-paste to the following scripts:
 *   • canvasgear.js
 *   • daftari.js
 *   • fadeinfiles.js
 *   • slidegear.js
 *  Issue : Think about some better propagation or sharing mechanism but
 *   without sacrificing the single-file principle [issue 20210516°1321].
 *     
 *
 *  Versions :
 *    • v0.2.9 — 20240428°1911 'Saveguard' [chg 20240428°1905]
 *    • v0.2.8 — 20240427°1511 — Adjust -- This was bad
 *    • v0.2.7.d — 20220312°1211 — Indent 4 spaces
 *    • v0.2.7.c — 20220307°0911 — Reformatting
 *    • 20210518°1441 — v0.2.7.b — Clear comments
 *    • 20210517°1121 — v0.2.7 — Streamline 'undefined' usage [Chg 20210517°1111]
 *    • 20210516°1331 — v0.2.6 — Cleanup
 *    • 20210516°0951 — Callers of pullScriptBehind must decide on minification [chg 20210516°0941]
 *    • v0.2.5 (20210428°1031) — Streamlining
 *    • 20210418°1111 — Little streamlining
 *    • 20201228°1511
 *    • 20190418°0343
 */

/**
 *  This shall be the root namespace
 *
 *  @id 20190106°0311
 *  @c_o_n_s_t — Namespace
 */
var Trekta = Trekta || {};

/**
 *  This namespace shall provide some general basic functionalities
 *
 *  The section between ~~~ Schnippel ~~~ and ~~~ Schnappel ~~~ can be cut
 *  and pasted to other scripts to provide them independent standalone basics.
 *
 *  @id 20190106°0313
 *  @c_o_n_s_t — Namespace
 *  @const — Namespace
 */
Trekta.Utils = Trekta.Utils || {};

/**
 *  This function reads a cookie value
 *
 *  @id 20110510°2127
 *  @note Remember finished issue 20201228°1311 'Cookie setting with file protocol'
 *  @param {string} sCookieName — The name of the cookie to be read
 *  @return {string} —
 */
Trekta.Utils.getCookie = function(sCookieName)
{
    'use strict';

    // Prologue [seq 20110510°2128]
    var i = 0;
    var sNam = '';
    var sRet = '';
    var sVal = '';
    var sCookies = document.cookie;                                     // Semicolon-delimited string

    // [condi 20201228°1331]
    // Todo 20201228°1332 : Detection is quick'n'dirty. This should be done in a more 'official' way.
    if (sCookies.length > 0) {

        // Find value [seq 20110510°2129]
        var aCookies = document.cookie.split(';');
        for (i = 0; i < aCookies.length; i++) {
            sNam = aCookies[i].substr(0, aCookies[i].indexOf('='));
            sVal = aCookies[i].substr(aCookies[i].indexOf('=')+1);
            sNam = sNam.replace(/^\s+|\s+$/g, '');
            if (sNam === sCookieName) {
                // Function unescape is deprecated, I boldly replaced it by decodeURI [chg 20190423°0641]
                sRet = decodeURI(sVal);
                break;
            }
        }
    } else {
        // Provide fallback for possibly failing cookies [seq 20201228°1333]
        sRet = localStorage.getItem(sCookieName);
    }

    return sRet;
};

/**
 *  This function reads a cookie as boolean
 *
 *  @id 20120828°2021
 *  @note This function is a wrapper for Trekta.Utils.getCookie()
 *  @callers • many
 *  @param {string} sCookieName — The name of the cookie to be read
 *  @param {boolean|null} bDefault — The default value
 *  @return {boolean} — The wanted cookie value
 */
Trekta.Utils.getCookieBool = function(sCookieName, bDefault)
{
    'use strict';

    // Establish optional parameter [line 20120828°2022]
    bDefault = bDefault || false;

    // Forward to cookie reading function [line 20120828°2023]
    var sVal = Trekta.Utils.getCookie(sCookieName);

    // Postprocess value [line 20120828°2024]
    var bReturn = bDefault;
    if (sVal === 'true') {
        bReturn = true;
    } else if (sVal === 'false') {
        bReturn = false;
    } else {
        bReturn = bDefault;
    }

    return bReturn;
};

/**
 *  This function reads a cookie as integer
 *
 *  @id 20190420°0311
 *  @callers •
 *  @param sCookieName {string} The name of the cookie to be read
 *  @param iDefault {boolean} The default value
 *  @return {number} (integer) — The wanted cookie value
 */
Trekta.Utils.getCookieInt = function(sCookieName, iDefault)
{
    'use strict';

    // Establish optional parameter [line 20190420°0312]
    iDefault = iDefault || 0;

    // Forward to cookie reading function [line 20190420°0313]
    var sVal = Trekta.Utils.getCookie(sCookieName);

    // Postprocess value [line 20190420°0314]
    var iReturn = iDefault;
    if (sVal !== '') {
        iReturn = parseInt(sVal, 10);
    }
     return iReturn;
};

/**
 *  This function retrieves the filename of the page to be edited
 *
 *  @id 20110820°1741
 *  @callers • 20120827°1511 getFilenamePlain • 20150411°0651 provideCornerstonePathes
 *            • 20120830°0451 EditFinishTransmit
 *  @return {string} — E.g. 'daftari/panels/login.html' (with Firefox)
 */
Trekta.Utils.getFileNameFull = function()
{
    'use strict';

    // Read URL of this page, values are e.g. [line 20110820°1742]
    //    • 'http://localhost/'
    //    • 'http://localhost/eps/index.html?XDEBUG_SESSION_START=netbeans-xdebug#'
    //    • 'file:///G:/work/daftaridev/trunk/daftari/docs/moonwalk.html' (not yet working)
    var sUrl = document.location.href;

    // Remove possible query after the file name [line 20110820°1743]
    sUrl = sUrl.substring(0, (sUrl.indexOf('?') === -1) ? sUrl.length : sUrl.indexOf('?'));

    // Remove possible anchor at the end [line 20110820°1744]
    sUrl = sUrl.substring(0, (sUrl.indexOf('#') === -1) ? sUrl.length : sUrl.indexOf('#'));

    // Possibly supplement page name 'index.html' [seq 20190419°0133] Note the analogy to seq 20181228°0935
    if ( sUrl.indexOf('/', sUrl.length - 1) !== -1 ) {
        sUrl += 'index.html';
    }

    return sUrl;
};

/**
 *  This function gets the plain filename of the page, e.g. 'help.html'
 *
 *  @id 20120827°1511
 *  @callers E.g. • dafdispatch.js::workoff_Cake_0_go
 *  @return {string} — The plainfilename, e.g. 'help.html'
 */
Trekta.Utils.getFilenamePlain = function()
{
    'use strict';

    var sUrl = Trekta.Utils.getFileNameFull();
    var a = sUrl.split('/');
    sUrl = a[a.length - 1];

    return sUrl;
};

/**
 *  This function escapes a string to be used as HTML output
 *
 *  @id 20140926°1431
 *  @callers • Cvgr.Func.executeFrame
 *  @todo  In FadeInFiles seq 20151106°1822 and seq 20151106°1821
 *          shall use this function here. [todo 20190328°0943]
 *  @param {string} sHtml — The HTML fragment to be escaped
 *  @return {string} — The wanted escaped HTML fragment
 */
Trekta.Utils.htmlEscape = function(sHtml)
{
    'use strict';

    sHtml = sHtml.replace(/</g, '&lt;');                                // g = replace all hits, not only the first
    sHtml = sHtml.replace(/>/g, '&gt;');

    return sHtml;
};

/**
 *  This function tests, whether the given script is already loaded
 *  or not. This function is unfaithful during the loading phase
 *
 *  @id 20160503°0231
 *  @callers ..
 *  @param {string} sWantedScript — The plain name of the wanted script (not a complete path)
 *  @return {boolean} — Flag telling whether the script is loaded or not.
 */
Trekta.Utils.isScriptAlreadyLoaded = function (sWantedScript)
{
    'use strict';

    var regexp = null;

    // Build the appropriate regex variable [seq 20160623°0311]
    // note : See howto 20160621°0141 'Programmatically build regex'
    // note : "/" seems automatically replaced by "\/"!
    var s = sWantedScript.replace(/\./g, "\\.");                        // E.g. '/slidegear.js' to '/slidegear\.js$'
    s = s + '$';
    regexp = new RegExp(s, '');                                         // E.g. /dafutils\.js$/

    // Do the job [algo 20160503°0241 (like algo 20110820°2042 'Find specific script')]
    var scripts = document.getElementsByTagName('SCRIPT');
    if (scripts && scripts.length > 0) {
        for (var i1 in scripts) {
            var i2 = Number.parseInt(i1,10);
            if (scripts[i2]) {                                          // [marker 20210416°1633`58 GoCloCom] Restricted index type
                if (scripts[i2].src.match(regexp)) {
                    return true;
                }
            }
        }
    }
    return false;
};

/**
 *  This function outputs a string to some default 'shell' div
 *
 *  @id 20150321°0311
 *  @note chg 20190412°0253 Shift func outDbgMsg() from dafmenu.js to Trekta.Utils
 *  @param {string} sOut — The text to be output
 *  @return {undefined} —
 */
Trekta.Utils.outDbgMsg = function (sOut)
{
    'use strict';

    // Obey flag [seq 20150814°0241]
    if ( ! Trekta.Utils.getCookieBool('checkbox_yellowdebugpane', null)) {
        return;
    }

    // Test page for availability, otherwise cache the message [seq 20150516°0431]
    if ( document.readyState !== "complete" ) {
        Trekta.Utils.InitialMessageCache.push(sOut);
        return;
    }

    // Provide target element [line 20150321°0312]
    // See issue 20160618°0431 'HTML class and id set the same'
    // '<div class="id20150321o0231_StandardOutputDiv" id="id20150321o0231_StandardOutputDiv"></div>'
    var ele = Trekta.Utils.outDbgMsg_GuaranteeParentElement();

    // Shim for IE8 [seq 20150411°0151]
    // Summary : IE8 does not know Date.now like other browsers
    // Chain : todo 20210516°1241 'Dismantle IE8 shim' {Daftari}
    if (! Date.now) {
        Date.now = function () {
            return new Date().getTime();
        };
    }
    var sTimestamp = Math.floor(Date.now() / 1000);
    sTimestamp = sTimestamp.toString();

    // [seq 20150321°0313]
    var sMsgDbg = "[Dbg 20150324°0321] Run function Trekta.Utils.outDbgMsg().";
    sMsgDbg += "\n ele.id = " + ele.id + "\n outerHTML = " + ele.outerHTML;

    // Possibly prepend cached messages [seq 20150516°0441]
    if (Trekta.Utils.InitialMessageCache.length > 0) {
        var sOut22 = '';
        for ( var i = Trekta.Utils.InitialMessageCache.length - 1; i >= 0; i-- ) {
            if (sOut22 !== '') { sOut22 = '\n\n' + sOut22; }
            sOut22 = Trekta.Utils.InitialMessageCache[i] + sOut22;
        }
        Trekta.Utils.InitialMessageCache.length = 0;
        // Expeimental cosmetics [seq 20190412°0241]
        sOut22 = '<div style="'
                + 'background-color:LightGreen; margin:1.1em; padding:0.7em;'
                 + '">' + sOut22 + '</div>'
                  ;
        sOut = sOut22 + "\n\n" + sOut;
    }

    // [algo 20150322°0221] 'replace textfile linebreaks by html tags'
    //  Just replace the linebreak characters by HTML linebreak tags,
    //  this is easier than converting them to paragraphs.
    var sPayload = sOut.replace(/\n/g, '<br />');

    // Wrap output in paragraph [seq 20150321°0314]
    var s = '<span style="font-size:72%;">' + sTimestamp + '</span>';
    var sAppend = '<p>' + s + ' ' + sPayload + '</p>';

    // [seq 20150321°0316]
    var eTarget = document.getElementById(Daf.Dspat.Config.sFurniture_OutputArea_Id);  // "id20150321o0231_StandardOutputDiv"

    // Assemble new element content [seq 20150321°0317]
    sAppend = '\n' + sAppend;
    eTarget.insertAdjacentHTML('beforeend', sAppend);
};

/**
 *  This function guarantees an element
 *  If the wanted element does not exist, it is created newly.
 *
 *  @id 20150323°0321
 *  @status Working
 *  @callers • func Trekta.Utils.outDbgMsg
 *  @return {Object|Node} — The wanted target element
 */
Trekta.Utils.outDbgMsg_GuaranteeParentElement = function ()
{
    'use strict';

    // (1) Seek target output element
    var sTargetId = Daf.Dspat.Config.sFurniture_OutputArea_Id;          // "id20150321o0231_StandardOutputDiv"
    var ele = document.getElementById(sTargetId);                       // 'id20150321o0231_...'

    // (2) Create element if not exists
    if (! ele) {

        // (Q.2) Create HTML fragment [seq 20150325°0311]
        // note : We put '<br />', because this string is immediate, and will not
        //    be passed trough the string mangling engine of Trekta.Utils.outDbgMsg(),
        //    which would have replaced automatically a newline by a br tag.
        // note : The '&nbsp;' is used, because in a HTML paragraph, leading
        //    blanks have no effect, as opposed to being in a pre tag block.
        //    Perhaps output were better done in a pre instead a p block?
        var sHtml = '<div'
                   + ' id="' + sTargetId + '"'
                   + ' class="dafBoxDebugOutput"'                       // daftari.css style=".. background-color:LemonChiffon; .."
                   + '>'
                   + '<p>'
                   + '[Msg 20150325°0211] Loading dafmenu.js (1/x).'
                   + '\n<br />&nbsp; Here comes the yellow Standard Output Box. Some page values are :'
                   + Daf.Mnu.Jsi.getJsiIntroDebugMessage(true)          // refactor 20180517°191103
                   + '</p>'
                   + '</div>'
                    ;

        // (Q.3) Integrate created fragment
        var eBody = document.getElementsByTagName('body')[0];
        var eDiv = document.createElement('div');
        eDiv.innerHTML = sHtml;
        eBody.appendChild(eDiv);

        // (Q.4) Retry, now mostly successful
        ele = document.getElementById(sTargetId);                       // 'id20150321o0231_StandardOutputDiv'
    }

    return ele;
};

/**
 *  This function loads the given script then calls the given function
 *
 *  @id 20110821°0121
 *  @version 20210516°0941 No more minification service, caller must decide (chg 20210516°0941)
 *  @version 20190405°0347 Refine onload callback (finished issue 20190405°0333)
 *  @version 20190331°0241 Added parameter for onError callback
 *  @version 20181229°1941 Now with parameter for onload callback function
 *  @see howto 20181229°1943 'summary on pullbehind'
 *  @callers • Many
 *  @param {string} sScLoad — The path from page to script, e.g. "./../jsi/dafcanary.js", jsi/dafcanary.js'
 *  @param {Function} callbackOnLoad — Optional. Callback function for the script onload event
 *  @param {Function} callbackOnError — Optional. Callback function for the script onerror event
 *  @param {Object} oJobs — Optional. Some identifiyer string or object to be
 *       passed from initiator to the callbacks (introduced 20190403°0215)
 *  @return {boolean|undefined} — Success flag (just a dummy, always true)
 */
Trekta.Utils.pullScriptBehind = function (sScLoad, callbackOnLoad, callbackOnError, oJobs)
{
    'use strict';
    /// debugger;

    // Avoid multiple loading [seq 20110821°0122]
    // Remember issue 20190405°0331 'isScriptAlreadyLoaded unfaithful'
    if ( Trekta.Utils.aPulled.indexOf(sScLoad) >= 0 ) {
        callbackOnLoad ( sScLoad                                        //
                        , oJobs                                         //
                         , true                                         // was already loaded
                          );
        return;
    }

    // Prepare the involved elements [seq 20110821°0123]
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');

    // Set the trivial properties [seq 20110821°0124]
    script.type = 'text/javascript';
    script.src = sScLoad;

    // Possibly work without callback [condi 20190404°0831]
    // This condition was wanted for loading fadeinfiles.js in seq 20190404°0827.
    //  No, it is not wanted from there. But hm.. the paranoia may be nice anyway.
    //  Finally, I am not sure now, whether the condition is useful or not.
    if (typeof callbackOnLoad !== 'undefined') {

        // Set the non-trivial but crucial property [line 20181229°1932]
        // The custom callback goes piggyback with the mandatory one
        var cbkCustom = function () { callbackOnLoad ( sScLoad
                                                      , oJobs
                                                       , false          // Flag 'was already loaded'
                                                        ); };
        script.onload = function () { Trekta.Utils.pullScript_onload(sScLoad, cbkCustom); };
    }

    // Attach onerror handler [condi 20190331°0242]
    callbackOnError = callbackOnError || null;
    if ( callbackOnError !== null) {
        script.onerror =  ( function () { callbackOnError (sScLoad, oJobs); } );
    }

    // Ignit the pulling [seq 20110821°0125]
    head.appendChild(script);

    return true;
};

/**
 *  This function constitutes the unconditional onload handler
 *
 *  @id 20190405°0341
 *  @callers Onyl • onload event from pullScriptBehind
 *  @param {string} sScript — The script to be pulled behind
 *  @param  {Function} cbkCustom — The callback to be executed after script is loaded
 *  @return {undefined} —
 */
Trekta.Utils.pullScript_onload = function (sScript, cbkCustom)
{
    'use strict';
    Trekta.Utils.aPulled.push(sScript);
    cbkCustom();
};

/**
 *  This function reads a file via asynchronous Ajax
 *
 *  @id 20190417°0311
 *  @status  Not really tested(?)
 *  @id      Rmember todo 20210516°1251 'Clear readTextFile2/ajax3Send status' {Daftari}
 *  @note    Remember issue 20140713°1121 'Ajax read file via file protocol' {FadeInFiles}
 *  @callers • E.g. func 20190106°0615 slidegear.js::o2ReadSetup_ImageList
 *  @param   {string} sUrl — File to be read
 *  @param   {Function} cbkLoad — Callback function for the case of success, taking one string parameter with the read content
 *  @param   {Function} cbkFail — Callback function for the case of fail, taking one string parameter
 *  @return  {undefined} —
 */
Trekta.Utils.readTextFile2 = function(sUrl, cbkLoad, cbkFail)
{
    'use strict';
    Trekta.Utils.ajax3Send('GET', sUrl, '', cbkLoad, cbkFail);
};

/**
 *  This function sends an Ajax request
 *
 *  @id : func 20190405°0231 [after 20140704°1011]
 *  @callers : • E.g. Trekta.Utils.readTextFile2
 *  @param sMethod {string} — Either 'GET' or 'POST' ("GET", "POST", "PUT", "DELETE")
 *  @param sUrl {string} — The request URL
 *  @param {string} sPayload — The data to transmit, only used with a POST request — SEEMS UNUSED
 *  @param {Function} cbkLoad — Callback function for the case of success, taking one string parameter
 *  @param {Function} cbkFail — Callback function for the case of fail, taking one string parameter
 *  @return {undefined} —
 */
Trekta.Utils.ajax3Send = function(sMethod, sUrl, sPayload, cbkLoad, cbkFail)
{
    'use strict';

    // () Prologue [line 20140704°1013]
    // todo : This must be refined .. e.g. with default callbacks
    cbkLoad = (typeof cbkLoad === 'undefined') ? null : cbkLoad ;
    cbkFail = (typeof cbkFail === 'undefined') ? null : cbkFail ;

    // Get the XMLHttpRequest object [line 20190417°0111]
    // See todo 20190209°0836 'XMLHttpRequest availability'
    var xmlHttp = new XMLHttpRequest();

    // () Set request parameters [line 20140704°1015]
    xmlHttp.open(sMethod, sUrl, true);                                  // true means asynchronous

    // () Probe the ongoing [line 20140704°1016]
    xmlHttp.onreadystatechange = function ()
    {
        // List after ref 20190412°0132 'MDN → XMLHttpRequest.readyState'
        if ( xmlHttp.readyState === 0 ) {
            // State = UNSENT	— Client has been created. open() not called yet
        }
        else if ( xmlHttp.readyState === 1 ) {
            // State = OPENED — open() has been called
        }
        else if ( xmlHttp.readyState === 2 ) {
            // State = HEADERS_RECEIVED — send() has been called, and headers and status are available
        }
        else if ( xmlHttp.readyState === 3 ) {
            // State = LOADING — Downloading; responseText holds partial data
        }
        else if ( xmlHttp.readyState === 4 ) {
            // State = DONE — The operation is complete
            // Below list after ref 20190412°0133 'MDN → HTTP response status codes'
            var bSuccess = false;                                       // Pessimistic predetermination
            switch (xmlHttp.status) {
                // Case '0' may happen e.g. if • Page is opened via the file
                //  scheme or • Too much time passes before the server responds
                //  Is setting success to true then really a good idea?
                case   0 : break;                                       // Consider this case as fail [chg 20220221°0911]
                case 100 : break;                                       // "Continue"
                case 101 : break;                                       // "Switching Protocol"
                case 102 : break;                                       // "Processing (WebDAV)"
                case 103 : break;                                       // "Early Hints"
                case 200 : bSuccess = true; break;                      // "OK"
                case 201 : break;                                       // "Created"
                case 202 : break;                                       // "Accepted"
                case 203 : break;                                       // "Non-Authoritative Information"
                case 204 : break;                                       // "No Content"
                case 205 : break;                                       // "Reset Content"
                case 206 : break;                                       // "Partial Content"
                case 207 : break;                                       // "Multi-Status (WebDAV)"
                case 208 : break;                                       // "Multi-Status (WebDAV)"
                case 226 : break;                                       // "IM Used (HTTP Delta encoding)"
                case 300 : break;                                       // "Multiple Choice"
                case 301 : break;                                       // "Moved Permanently"
                case 302 : break;                                       // "Found"
                case 303 : break;                                       // "See Other"
                case 304 : break;                                       // "Not Modified"
                case 305 : break;                                       // "Use Proxy 👎"
                case 306 : break;                                       // "unused"
                case 307 : break;                                       // "Temporary Redirect"
                case 308 : break;                                       // "Permanent Redirect"
                case 400 : break;                                       // "Bad Request"
                case 401 : break;                                       // "Unauthorized"
                case 402 : break;                                       // "Payment Required"
                case 403 : break;                                       // "Forbidden"
                case 404 : break;                                       // "Not Found"
                case 405 : break;                                       // "Method Not Allowed"
                case 406 : break;                                       // "Not Acceptable"
                case 407 : break;                                       // "Proxy Authentication Required"
                case 408 : break;                                       // "Request Timeout"
                case 409 : break;                                       // "Conflict"
                case 410 : break;                                       // "Gone"
                case 411 : break;                                       // "Length Required"
                case 412 : break;                                       // "Precondition Failed"
                case 413 : break;                                       // "Payload Too Large"
                case 414 : break;                                       // "URI Too Long"
                case 415 : break;                                       // "Unsupported Media Type"
                case 416 : break;                                       // "Requested Range Not Satisfiable"
                case 417 : break;                                       // "Expectation Failed"
                case 418 : break;                                       // "I'm a teapot"
                case 421 : break;                                       // "Misdirected Request"
                case 422 : break;                                       // "Unprocessable Entity (WebDAV)"
                case 423 : break;                                       // "Locked (WebDAV)"
                case 424 : break;                                       // "Failed Dependency (WebDAV)"
                case 425 : break;                                       // "Too Early"
                case 426 : break;                                       // "Upgrade Required"
                case 428 : break;                                       // "Precondition Required"
                case 429 : break;                                       // "Too Many Requests"
                case 431 : break;                                       // "Request Header Fields Too Large"
                case 451 : break;                                       // "Unavailable For Legal Reasons"
                default  : break;                                       // Should never happen
            }
            if ( bSuccess ) {
                // xmlHttp.status is 0 or 200
                cbkLoad(xmlHttp.responseText);
            } else {
                // All other xmlHttp.status values
                cbkFail(xmlHttp.responseText);
            }
        }
    };

    // () Finally perform the request [seq 20140704°1017]
    try {
        // If file to read does not exist, or we are on file system, we get
        //  exception "Failed to load resource: .. status of 404 (Not Found)"
        xmlHttp.send(null);
    }
    catch (ex) {

        // [line 20140704°1103]
        // note 20160624°0131 : To test below error messages, browse via file protocol
        //    the pages • 20160613°0211 Daftari → Manual → FadeInFiles with Firefox
        //    • and 20150211°1211 Daftari → Manual → Slideshow with Chrome
        var sMsg = "<b>Sorry, some feature on this page does not work.</b>"
                  + '\nFile <tt>' + sUrl + '</tt> ~~could not be read.'
                   + "\nYour browser said: "
                    + '<tt>' + ex.message + '</tt>.'                    // E.g. "A network error occurred".
                     ;

        // [condi 20140704°1104]
        // Process todo 20210516°1311 'Dismantle browser switch' {Daftari}
        if ( Trekta.Utils.bIs_Browser_Chrome && (location.protocol === 'file:') ) {

            // [line 20140704°1105]
            sMsg += "\nYour browser seems to be Chrome, and this does not ~~read files via file protocol."
                  + "\nThere are two <b>solutions</b>: (1) Use a different browser, e.g. Firefox or IE"
                   + "\nor (2) view this page from <tt>localhost</tt> with a HTTP server."
                    ;
        }
        else if ( Trekta.Utils.bIs_Browser_Firefox && (location.protocol === 'file:') ) {
            // [line 20140704°1106]
            sMsg += "\nYour browser seems to be <b>Firefox</b>, and this does not ~~read files"
                  + "\nwith a path going below the current directory via file protocol."
                   + "\nThere are two <b>solutions</b>: (1) Use a different browser, e.g. Chrome or IE"
                    + "\nor (2)  view this page from <tt>localhost</tt> with a HTTP server."
                     ;
        }
        else {
            sMsg += '\n [info 20160622°0131] Failed sending request ' + sUrl + '.';  // [line 20140704°1107]
        }
         // Use callback to deliver error message [line 20190405°0233]
        // Or perhaps rather just use a plain alert() .. or a popping up error box on the page?
        cbkLoad(sMsg);
    }
};

/**
 *  This function returns the path to the given script .. using regex
 *
 *  @id 20110820°2041
 *  @status working
 *  @see todo 20190316°0141 'call retrieveDafBaseFolderRel without canary'
 *  @see howto 20190209°0131 'retrieve this script path'
 *  @note There might be browser differences with the return value, e.g.
 *      - FF etc : scripts[i].src = 'http://localhost/manual/daftari/daftari.js'
 *      - IE     : scripts[i].src = 'daftari.js'
 *  @callers e.g. • CanvasGear func 20140815°1221 executeFrame • • •
 *  @param {string} sScCanary — The name of the canary script, e.g. '/sitmapdaf.js'.
 *  @return {string} — The wanted path, where the given script resides or empty string
 */
Trekta.Utils.retrieveDafBaseFolderAbs = function (sScCanary)
{
    'use strict';

    // () Prepare regex [seq 20160621°0142]
    var regexMatch = / /;                                               // Space between slashes prevents a syntax error
    var regexReplace = / /;
    var s = sScCanary.replace(/\./g, "\\.") + "$";                      // E.g. 'dafutils.js' to 'dafutils\.js$'
    regexMatch = new RegExp(s, '');                                     // E.g. /dafutils\.js$/
    s = '(.*)' + s;                                                     // Prepend group
    regexReplace = new RegExp(s, '');                                   // E.g. /(.*)dafutils\.js$/ ('/' seems automatically replaced by '\/')

    // () Do the job [algo 20110820°2042 'find specific script' prototype]
    var sPath = '';
    var scripts = document.getElementsByTagName('SCRIPT');              // Or 'script'
    if (scripts && scripts.length > 0) {
        for ( var i1 in scripts ) {
            var i2 = Number.parseInt(i1,10);                            // [marker 20210416°1633`xx GoCloCom] Serve 'restricted index type' // [line 20210416°1741]
            // note : There are browser differences, e.g.
            //    • FF etc : scripts[i].src = 'http://localhost/manual/daftari/daftari.js'
            //    • IE     : scripts[i].src = 'daftari.js'
            if (scripts[i2]) {                                          // If i2 is NaN, this evaluates to False // [marker 20210416°1633`59 GoCloCom] Restricted index type
                if ( scripts[i2].src.match(regexMatch) ) {              // E.g. /dafmenu\.js$/
                    sPath = scripts[i2].src.replace(regexReplace, '$1');  // E.g. /(.*)dafmenu.js$/
                }
            }
        }
    }

    return sPath;                                                       // E.g. "http://localhost/daftaridev/trunk/daftari/jsi"
};

/**
 *  This function shortens a long string by placing ellipsis in the middle
 *
 *  @id 20201203°1445
 *  @callers • 20110811°1921 Daf.Mnu.Meo.execElementEdit
 *  @param {string} sOrig — The string to be shortened
 *  @param {number} iMaxLen (integer) — The maximum lenght of the output string
 *  @return {string} — The wanted shortened string
 */
Trekta.Utils.sConfine = function(sOrig, iMaxLen)
{
    var sRet = '';
    if (sOrig.length > iMaxLen) {
        sRet = sOrig.substring(0, iMaxLen / 2)
                + ' … '
                 + sOrig.substring(sOrig.length - iMaxLen / 2)
                  ;
    }
    sRet = sRet.split('\n').join('☼');
    sRet = sRet.split('<').join('&lt;');
    sRet = sRet.split('>').join('&gt;');
    return sRet;
};

/**
 *  This function sets a cookie
 *
 *  @id 20110510°2126
 *  @See ref 20120828°1931 'QuirksMode → Cookies'
 *  @callers •
 *  @param {string} sCookieName — iExpirationDays is number of days until the cookie expires
 *  @param {string} sCookieValue — iExpirationDays is number of days until the cookie expires
 *  @param {number} iExpirationDays — iExpirationDays is number of days until the cookie expires
 *  @return {undefined} —
 */
Trekta.Utils.setCookie = function(sCookieName, sCookieValue, iExpirationDays)
{
    'use strict';

    var dExDate = new Date();
    dExDate.setDate(dExDate.getDate() + iExpirationDays);

    // Escape is deprecated, I boldly replace it by encodeURI [note 20190423°0641`03]
    var sValue = ( encodeURI(sCookieValue) + ((iExpirationDays === null)
                  ? ''
                   : '; Expires = ' + dExDate.toUTCString())
                    );

    var sCookie = sCookieName + '=' + sValue;

    // [line 20120828°1932] Make it valid for the whole site, not only a subdir
    sCookie += '; path=/';

    //alert('Trekta.Utils.setCookie() = ' + sCookie);
    document.cookie = sCookie;

    // Detect cookie availability and provide fallback [seq 20201228°1321]
    if (document.cookie.length < 1) {
        localStorage.setItem(sCookieName, sCookieValue);
    }
};

/**
 *  This function daisychains the given function on the windows.onload events
 *
 *  @id 20160614°0331
 *  @todo This bulky function may be obsolet by function add​Event​Listener.
 *     The present function supports below IE9, whereas add​Event​Listener
 *     supports only IE9 and above, but that is enough [todo 20190328°0947].
 *     See e.g. file 20210626°0711 wheel-of-fortune.js, line 20210820°1553.
 *  @see ref 20190328°0953 'MDN → addEventListener'
 *  @callers
 *  @param {Function} function — The function to be appended to the window.onload event
 *  @return {undefined} —
 */
Trekta.Utils.windowOnloadDaisychain = function(funczion)
{
    'use strict';

    // Is the onload handler already used?
    if ( window.onload ) {
        // Preserve existing function(s) and append our additional function
        var ld = window.onload;
        window.onload = function() {
            ld(null);
            funczion();
        };
    }
    else {
        // No other handlers are registered yet
        window.onload = function() {
            funczion();
        };
    }
};

/**
 *  This variable constitutes the onload ready flags for pullScriptBehind
 *
 *  @id 20190405°0345
 *  @note This flags solves issue 20190405°0331 'isScriptAlreadyLoaded unfaithful'
 *  @type {Array} —
 */
Trekta.Utils.aPulled = [];

/**
 *  This ~constant provides a flag whether the browser is Chrome or not
 *
 *  Explanation. The plain expression "navigator.appName.match(/Chrome/)"
 *  results in either True or Null. But I prefere the result being either
 *  True or False. This is achieved by wrapping the expression in the
 *  ternary operator, manually replacing Null by false.
 *
 *  @id 20160622°0221
 *  @todo : Browser detection inconsequently sometimes uses navigator.userAgent
 *     sometimes navigator.appName. Standardize this. [todo 20190209°0833]
 *  @type {boolean} —
 */
Trekta.Utils.bIs_Browser_Chrome = ( navigator.userAgent.match(/Chrome/) ? true : false );

/**
 *  This ~constant provides a flag whether the browser is Edge or not
 *
 *  @id 20190417°0217
 *  @type {boolean} —
 */
Trekta.Utils.bIs_Browser_Edge = ( navigator.userAgent.match(/Edge/) ? true : false );

/**
 *  This ~constant provides a flag whether the browser is Internet Exporer or not
 *
 *  @id 20150209°0941
 *  @see Todo 20190209°0837 'Refine IE detection algo' {Daftari}
 *  @note Only IE11 has 'window.msCrypto'. IE11 has different user agent string than other IE [see line 20190417°0215]
 *  @type {boolean} —
 */
Trekta.Utils.bIs_Browser_Explorer = (
    ( navigator.appName.match(/Explorer/)
     || window.msCrypto                                                 // [line 20190417°0215]
      ) ? true : false
);

/**
 *  This ~constant provides a flag whether the browser is Firefox or not
 *
 *  @id 20160624°0121
 *  @type {boolean} —
 */
Trekta.Utils.bIs_Browser_Firefox = ( navigator.userAgent.match(/Firefox/) ? true : false );

/**
 *  This property provides a flag whether the browser is Opera or not.
 *  Just nice to know, Opera seems to need no more extras anymore (2019).
 *
 *  @note 20190314°0411 : Opera 58 seem to need no more extra treatment.
 *  @note 20190314°0413 : In Opera 58 I saw this userAgent string
 *      • "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36
 *        (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.118"
 *  @id 20190107°0821
 *  @type {boolean} —
 */
Trekta.Utils.bIs_Browser_Opera = ( navigator.userAgent.match(/(Opera)|(OPR)/) ? true : false )

/**
 *  This property tells whether to pop up debug messages or not
 *
 *  @id 20190311°1521
 *  @type {boolean} —
 */
Trekta.Utils.bShow_Debug_Dialogs = false;

/**
 *  This property provides a constant false value
 *
 *  @id 20190407°0121
 *  @type {boolean} —
 */
Trekta.Utils.bToggle_FALSE = false;

/**
 *  This property provides a constant false value
 *
 *  @id 20190407°0122
 *  @type {boolean} —
 */
Trekta.Utils.bToggle_TRUE = true;

/**
 *  This flag tells whether to pull-behind minified scripts or not
 *
 *  @id 20190408°0115
 *  @callers •
 *  @type {boolean} —
 */
Trekta.Utils.bUseMinified = false;

/**
 *  This caches any output messages as long the page is not yet loaded
 *
 *  @id 20150516°0451
 *  @type {Array} —
 */
Trekta.Utils.InitialMessageCache = Array();

/**
 *  This const provides the ID for the general output area (yellow pane)
 *
 *  @id 20160618°0421
 *  @type {string} —
 */
Trekta.Utils.sFurniture_OutputArea_Id = "id20150321o0231_StandardOutputDiv";

/**
 *  This tells ..
 *
 *  @id 20160501°1622
 *  @callers •
 *  @type {string} —
 */
////Trekta.Utils.s_DaftariBaseFolderAbs = '';
Trekta.Utils.s_DaftariBaseFolderAbs = Trekta.Utils.s_DaftariBaseFolderAbs || '';  // Saveguard variable [chg 20240428°1905] This solves issue 20240428°1831 'Base folder broken'

/**
 *  This tells ..
 *
 *  @id 20160501°1623
 *  @callers •
 *  @type {string} —
 */
////Trekta.Utils.s_DaftariBaseFolderRel = '';
Trekta.Utils.s_DaftariBaseFolderRel = Trekta.Utils.s_DaftariBaseFolderRel || '';  // Saveguard [chg 20240428°1905`02] Solve issue 20240428°1831 'Base folder broken'

/**
 *  This ~static ~class provides a method to parse a command string
 *
 *  @id 20140926°0641
 *  @status Works
 *  @note Features :
 *     • Allow whitespaces [chg 20190405°0517]
 *     • Accept single quotes as well as double quotes [chg 20190405°0507]
 *  @callers • CanvasGear • page 20150210°0311 docs/testing.html
 *  @note Code inspired by ref 20140926°0621 'Krasimir: Simple command line parser in JS'
 *  @note See also ref 20140828°0832 'majstro: tokenizing with split'
 *  @return {undefined} —
 */
Trekta.Utils.CmdlinParser = ( function()
{
    'use strict';

    /**
     *  This function parses a commandline
     *
     *  @id 20140926°0642
     *  @param sCmdlin {string} The string to be parsed
     *  @return {Object} — Object with the read key-value-pairs
     */
    Trekta.Utils.parse = function(sCmdlin)
    {
        // Paranoia — advisably [seq 20140926°0653]
        if ( typeof sCmdlin === 'undefined' ) {
            sCmdlin = '';
        }

        // Prologue [loop 20140926°0654]
        var args = [];                                                  // Accumulates the found tokens
        var sQuoting = '';                                              // Stores quote while inside quoted area
        var sToken = '';                                                // Accumulates characters to one token

        // Scan characters [loop 20140926°0643]
        for ( var i1 = 0; i1 < sCmdlin.length; i1++ )
        {
            // Convenience [line 20190405°0512]
            var sChar = sCmdlin.charAt(i1);

            // Process blank [condi 20140926°0644]
            if ( (sChar === ' ') && (sQuoting === '') ) {

                // Ignore whitespace [seq 20190405°0513]
                if (sToken === '') {
                    continue;
                }

                // Finish current token [seq 20140926°0645]
                args.push(sToken);
                sToken = '';

            }
            // [condi 20190405°0514] experimental
            else if ( (sChar === '=') && (sQuoting === '') ) {

                // [seq 20190405°0515]
                if (sToken !== '') {
                    args.push(sToken);
                    sToken = '';
                }

                // Found token delimiter, finish current token [seq 20190405°0516]
                args.push('=');

            }
            else {

                // Accumulate one token [seq 20140926°0646]
                if ((sChar === "'") || (sChar === '"')) {
                    sQuoting = (sQuoting === '') ? sChar : '';          // Toggle quoting flag
                }
                else {
                    sToken += sChar;
                }
            }
        }
        args.push(sToken);
        // Now the plain token array is ready, the equal sign is also a token.

        // (B) Parse the found tokens [seq 20140926°1111]
        // summary : This algo points to the first token, then looks ahead for
        //  an equal sign. This has the advantage, that any solitary token is
        //  treated like a key as well, just later it will no more receive a value.
        // hint : One loop finishes one CmdsHash element/cell.

        // (B.1) Prologue [seq 20140926°1112]
        var oKvps = [];
        var sCurrKey = '';

        // (B.1) Loop over the token array  [seq 20140926°1113]
        //  Assemble key/value pairs from the equal signs
        for (var i2 = 0; i2 < args.length; i2++) {

            // (B.2) Possibly skip empty elements [seq 20140926°1114]
            // note : This cleaning could be done separately before the loop. As
            //     well it is not yet exactly clear, what happens with blank values.
            if (args[i2] === '') {                                      // Experimental
                continue;
            }

            // (B.3) Read key name and create key with empty value [seq 20140926°1115]
            sCurrKey = args[i2];
            oKvps[sCurrKey] = '<n/a>';                                  // '<n/a>' is a maker, may be replaced by null or the like

            // (B.4) Is next token an equal sign? [seq 20140926°1116]
            if ( args[i2 + 1] === '=' ) {

                // Complete current key/value pair with value [seq 20140926°1117]
                oKvps[sCurrKey] = args[i2 + 2];
                sCurrKey = '<n?a>';                                     // Reset

                // [seq 20140926°1118]
                i2++;                                                   // Forward to equal sign
                i2++;                                                   // Forward to this value
                continue;                                               // Forward to next key
            }
            else {
                // [seq 20140926°1119]
                continue;                                               // Forward to next key
            }
        }

        return oKvps;
    };

    // Curiously, if you place the opening curly bracket not behind the return
    //  but on the next line, the script will be broken [note 20160416°1311]
    return {
        parse : Trekta.Utils.parse
    };
})();
// ~ ✂ ~ ~ ~ ~ ~ ~ ~ Area 20190106°0307 Stop ~ ~ ~ ~ ~ ~ ~ ~ ~


// [line 20190411°0141]
Daf.Core.executeScriptLevel();
