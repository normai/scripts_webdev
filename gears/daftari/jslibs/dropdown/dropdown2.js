/*!
 * file : 20190422°0811 dropdown/dropdown.js
 * version : v0.2.1.~ (20210428°1051)
 * summary : This scrpt provides a dropdownmenu
 * note : This is the modified version of what came originally from
 *    http://code.iamkate.com/javascript/touch-friendly-drop-down-menus/Dropdown.src.js
 * note : Here the preserved original file header
 *    ---------------------------------
 *    Dropdown.js — Creates touch-friendly drop-down menus
 *    Created by Kate Morley - http://code.iamkate.com/
 *    - and released under the terms of the CC0 1.0 Universal legal code:
 *    http://creativecommons.org/publicdomain/zero/1.0/legalcode
 *    ---------------------------------
 */

/**
 * This creates the Dropdown object
 *
 * @type {Object}
 * @constructor
 */
var Dropdown = ( function() {

   // Delay, in milliseconds
   var DELAY = 250;

   // List of menus
   var menus = [];

   // Initialises the drop-down menus.
   function initialise() {

      // Listen for touch events on the document if appropriate
      if ('createTouch' in document) {
         document.body.addEventListener('touchstart', handleTouchStart, false);
      }

      // Loop over the menus, converting them
      var menus = document.querySelectorAll('ul.dropdown');
      for (var i = 0; i < menus.length; i ++) {
         applyTo(menus[i]);
      }
   }

   /**
    *  This handles a touch start event
    *
    * @param e — The event
    */
   function handleTouchStart(e) {

      // Determine whether any menu is open
      var isOpen = false;
      for (var i = 0; i < menus.length; i ++) {
         if (menus[i].isOpen) isOpen = true;
      }

      // Return immediately if all menus are closed
      if (! isOpen) return;

      // Move up the document tree until we reach the root node
      var node = e.target;
      while (node !== null) {

         // Return immediately if we are inside a drop-down menu
         if (/\bdropdown\b/.test(node.className)) return;

         // Move onto the parent node
         node = node.parentNode;
      }

      // Close all menus
      close();
   }

   /**
    *  This closes all menus except the specified menu
    *
    * @param menu — A menu not to close; this parameter is optional
    */
   function close(menu) {

      // Loop over the menus, closing them
      for (var i = 0; i < menus.length; i ++) {
         if (menus[i] !== menu) menus[i].close();
      }
   }

   /**
    *  This creates a drop-down menu
    *
    * @param node — Either the DOM node of the menu or the ID of the node
    */
   function applyTo(node) {

      // Fetch the DOM node if a string was supplied
      if (typeof node === 'string') {
         node = document.getElementById(node);
      }

      // Create and store the new menu
      menus.push(new Menu(node));
   }

   /**
    *  This creates a drop-down menu
    *
    * @param node - The DOM node of the menu
    */
   function Menu(node) {

      // Store the node
      this.node = node;

      // Update the class name
      node.className += ' dropdownJavaScript';

      // Listen for mouse events
      if ('addEventListener' in node) {
         node.addEventListener(
             'mouseover', this.bind(this.handleMouseOver), false);
         node.addEventListener('mouseout', this.bind(this.handleMouseOut), false);
         node.addEventListener('click',    this.bind(this.handleClick), false);
      } else {
         node.attachEvent('onmouseover', this.bind(this.handleMouseOver));
         node.attachEvent('onmouseout',  this.bind(this.handleMouseOut));
         node.attachEvent('onclick',     this.bind(this.handleClick));
      }

      // Listen for touch events if appropriate
      if ('createTouch' in document) {
         node.addEventListener('touchstart', this.bind(this.handleClick),false);
      }
   }

   // Whether the menu is open
   Menu.prototype.isOpen = false;

   // The timeout
   Menu.prototype.timeout = null;

   /**
    * This binds the specified function to the current object.
    *
    * @param f — the function
    */
   Menu.prototype.bind = function(f) {

      // Return the bound function
      var thisObject = this;
      return function() { f.apply(thisObject, arguments); };
   };

   /**
    *  This handles a mouse over event
    *
    * @param e — The event
    * @param immediate — True to open the menu without a delay
    */
   Menu.prototype.handleMouseOver = function(e, immediate) {

      // Clear the timeout
      this.clearTimeout();

      // Find the parent list item
      var item = ('target' in e ? e.target : e.srcElement);
      while (item.nodeName !== 'LI' && item !== this.node) item = item.parentNode;

      // If the target is within a list item, set the timeout
      if (item.nodeName === 'LI') {
         this.toOpen  = item;
         this.timeout =
             window.setTimeout(this.bind(this.open), (immediate ? 0 : DELAY));
      }
   };

   // Handles a mouse out event.
   Menu.prototype.handleMouseOut = function() {

      // Clear the timeout
      this.clearTimeout();

      // Set the timeout
      this.timeout = window.setTimeout(this.bind(this.close), DELAY);
   };

   /**
    *  This handles a click event
    *
    * @param e — The event
    */
   Menu.prototype.handleClick = function(e) {

      // Close any other menus
      close(this);

      // Find the parent list item
      var item = ('target' in e ? e.target : e.srcElement);
      while (item.nodeName !== 'LI' && item !== this.node) item = item.parentNode;

      // Check that the target is within a list item
      if (item.nodeName === 'LI') {

         // Check whether the item has a closed submenu
         var submenu = this.getChildrenByTagName(item, 'UL');
         if (submenu.length > 0 && ! /\bdropdownOpen\b/.test(item.className)) {

            // Open the submenu
            this.handleMouseOver(e, true);

            // Prevent the default action
            if ('preventDefault' in e) {
               e.preventDefault();
            } else {
               e.returnValue = false;
            }
         }
      }
   };

   // Clear the timeout.
   Menu.prototype.clearTimeout = function() {

      // Clear the timeout
      if (this.timeout) {
         window.clearTimeout(this.timeout);
         this.timeout = null;
      }
   };

   // Open the last item hovered over.
   Menu.prototype.open = function() {

      // Store that the menu is open
      this.isOpen = true;

      // Loop over the list items with the same parent
      var items = this.getChildrenByTagName(this.toOpen.parentNode, 'LI');
      for (var i = 0; i < items.length; i ++) {

         // Check whether there is a submenu
         var submenu = this.getChildrenByTagName(items[i], 'UL');
         if (submenu.length > 0) {

            // Check whether the submenu should be opened or closed
            if (items[i] !== this.toOpen) {

               // Close the submenu
               items[i].className =
                   items[i].className.replace(/\bdropdownOpen\b/g, '');
               this.close(items[i]);

            } else if (! /\bdropdownOpen\b/.test(items[i].className)) {

               // Open the submenu
               items[i].className += ' dropdownOpen';

               // Determine the location of the edges of the submenu
               var left = 0;
               var node = submenu[0];
               while (node) {
                  left += node.offsetLeft;
                  node = node.offsetParent;
               }
               right = left + submenu[0].offsetWidth;

               // Move the submenu to the right of the item if appropriate
               if (left < 0) items[i].className += ' dropdownLeftToRight';
 
               // Move the submenu to the left of the item if appropriate
               if (right > document.body.clientWidth) {
                  items[i].className += ' dropdownRightToLeft';
               }
            }
         }
      }
   };

   /**
    *  Closes the menus within the specified node.
    *
    * @param node — The node; if omitted, all menus are closed
    */
   Menu.prototype.close = function(node) {

      // If no node was specified, close all menus
      if (! node) {
         this.isOpen = false;
         node = this.node;
      }

      // Loop over the items, closing their submenus
      var items = node.getElementsByTagName('li');
      for (var i = 0; i < items.length; i ++) {
         items[i].className = items[i].className.replace(/\bdropdownOpen\b/g, '');
      }
   };

   /**
    *  Returns an array containing the children of the specified
    *   node with the specified tag name.
    *
    * @param node — The node
    * @param tagName — The tag name
    */
   Menu.prototype.getChildrenByTagName = function(node, tagName) {

      // Initialise the list of children
      var result = [];

      // Loop over the children, adding those with the right tag name to the list
      for (var i = 0; i < node.childNodes.length; i ++) {
         if (node.childNodes[i].nodeName === tagName) {
            result.push(node.childNodes[i]);
         }
      }

      // Return the children
      return result;
   };

   // Return the public API
   return {
      initialise : initialise,
      applyTo : applyTo
   };

})();
