<?php
/**
 * This file stores class SQLitConf to provide the SQLite configuration
 *
 * @file       : 20180331°0101
 * @encoding   : UTF-8-without-BOM
 */

namespace Trekta\Databese;

//use Trekta\Daftari as TD;
//use Trekta\Daftari\Globals as Glb;

/**
 * This static class shall provide the SQLite configuration
 *
 * @id 20180331°0111
 * @author ncm
 */
class SQLitConf {

   /**
    * @id 20180331°0121
    * @var String The path to the SQLite database file
    */
    public static $sPATH_TO_SQLITE_FILE = '';

   /**
    * The relative path to the sqlite database folder
    *
    * @id 20180331°0122
    * @var String
    */
    private static $sRelPathToSqlitFolder = './../../daftari.DATA/sqlit'; // This is relative to e.g. daftari/docs/sqlite7.html (remember issue 20201219°1413)

   /**
    * The SQLite database filename
    *
    * @id 20180331°0123
    * @var String
    */
    private static $sSqlitFilename = 'phpsqlite.db';


   /**
    * This function is to be called before accessing it's field(s).
    *
    * @id 20180331°0131
    * @note Compare seq 20110831°2043 'explore script environment'.
    *        Here we assume, folder 'daftari.DATA' already exists.
    * @note This function here is introduced because otherwise we cannot
    *    assign a calculated value to a field, e.g. one using realpath().
    */
   public static function init() {

      // (1) Set SQLite file [seq 20180331°0132]
      // (1.1) Try getting folder
      $sPath = realpath(self::$sRelPathToSqlitFolder);

      if (false) {
         // Possibly use this info trick while finding pathes [line 20201219°1415]
         $sWhereAmI = realpath('.');                                   // e.g. "G:\work\downtown\daftaridev\trunk\daftari\docs" if used from daftari/docs/sqlite7.html — Aha! [dbg 20201219°1411]
      }

      // (1.2) Paranoia
      if ($sPath === FALSE)
      {
         // (1.2.1) Create folder
         if (! mkdir(self::$sRelPathToSqlitFolder))
         {
            die('Error 20180330°0222 — Failed to create folders.');
         }

         // (1.2.2) Verify folder creation
         $sPath = realpath (self::$sRelPathToSqlitFolder);
         if ($sPath === FALSE)
         {
            die('Error 20180330°0223 — Fatal. Failed to create folders.');
         }
      }

      // (1.3) Directory is guaranteed, now append filename
      $sPath .= '/' . self::$sSqlitFilename;

      // (1.4) Finally set wanted field
      self::$sPATH_TO_SQLITE_FILE = $sPath;
   }
}

/* eof */
