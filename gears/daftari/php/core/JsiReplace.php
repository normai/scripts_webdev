<?php

/**
 * This ..
 *
 * file        : 20190414°0311
 * summary     : 
 * license     : GNU AGPL v3
 * copyright   : © 2019 - 2024 Norbert C. Maier
 * encoding    : UTF-8-without-BOM
 * status      : under construction
 */

declare(strict_types=1); // introduce new and experimentally [line 20190414°0411]

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

/**
 * This class ..
 *
 * @id 20190414°0313
 */
class JsiReplace
{
   /**
    * This function serves for evaluation and debug purposes. Chaotic and
    *  embryonic but contains some lines which were tricky to find out. Code
    *  is preserved to finish it for completely recursively iterating the DOM.
    *
    * @id 20190414°0611
    * @param $doc {\DOMDocument} The document or element to iterate
    * @return {string} Empty so far
    */
   private static function iterateDom ($doc)
   {
      $sRet = '';
      $sEco = Glb::$sTkNL . '☼ Iterate DOM :';
      $sTb = '   ';

      // [seq 20190414°0612]
      $deDoc = $doc->documentElement; // convenience
      $dnl1 = $deDoc->childNodes;
      foreach ($dnl1 as $dn1) {

         // Debug [seq 20190414°0613]
         $dnl2 = $dn1->childNodes;
         $sEco .= Glb::$sTkNL . $sTb . ' — nam = ' . $dn1->nodeName . ', nodtyp = ' . $dn1->nodeType;
         $sEco .= ' ' . gettype($dnl2)
                . ', listtyp = ' . ( (gettype($dnl2) === 'object') ? get_class($dnl2) : 'N/A' )
                 ;

         // Only DOMXxx elements are iterable, those are recognized as 'object' [condi 20190414°0614]
         if (gettype($dnl2) === 'object') {

            // Loop  [seq 20190414°0615]
            foreach ($dnl2 as $dn2) {

               // Some node types pollute the list [seq 20190414°0616]
               if ( $dn2->nodeName === '#comment' || $dn2->nodeName === '#text' ) {
                  continue;
               }

               // Output [seq 20190414°0617]
               $sEco .= Glb::$sTkNL . $sTb . '   TG=' . $dn2->nodeName ;

               // Get attributes [seq 20190414°0618]
               $atts = $dn2->attributes; // DOMNamedNodeMap
               if ( gettype($atts) === 'object') {
                  // Loop the attributes [seq 20190414°0619]
                  $sAts = ' AT:';
                  foreach ($atts as $sNam => $val) {                   // xVal is of type ~attrNode ~DOMAttr
                     $sAts .= ' ' . $sNam . '=' . $val->value;
                  }
                  $sEco .= $sAts;
               }
            }
         }
      }
      echo($sEco);
      return $sRet;
   }

   /**
    * This function shall replace the given elements content
    *
    * @id 20190414°0321
    * @see ref 20190414°0424 'stackoverflow → appendXML with special characters'
    * @see ref 20190414°0423 'stackoverflow → parse and process html/xml'
    * @see ref 20190414°0422 'stackoverflow → file-get-contents exhaust memory'
    * @callers Only • func 20190205°0441 JsiReplace::replaceDomElement
    * @param $sId {string} The ID attribute of the element to replace
    * @param $sFile {string} The path of the file to write
    * @param $sPut {string} The content to fill in
    * @return $sRet {string} Empty on success, ~~error message on fail
    */
   public static function replaceDomElement ($sId, $sFile, $sPut) : string
   {
      // Prologue [seq 20190414°0323]
      $sRet = '';
      $sEco = '';

      // Run mode 1 using text search [seq 20190414°0327]
      //  Runs independend and untouched by below DOMDocument entanglements
      if (self::$b_Mode_1_TextSearch_Run)
      {
         // Revert possible percent-encodings [seq 20190422°0231]
         // E.g. X:/data/http%27www.site.org/o.html to X:/data/http'www.site.org/o.html
         $sFile = urldecode($sFile);

         $xCntns = file_get_contents($sFile);
         $xCntns = self::run1_RegexSearch($xCntns, $sId, $sPut);

         if (self::$b_Mode_1_TextSearch_Run_Live) {
            $sFil7 = $sFile;
         }
         else {
            $s23 = substr($sFile, 0, strlen($sFile) - 5);
            $sFil7 = $s23 . '.v' . '1' . '.html';
         }

         if ($xCntns !== FALSE) {
            $iBytes = file_put_contents($sFil7, $xCntns);
            $sEco = Glb::$sTkNL . '☼ Save.a   1 : ' . $iBytes . ' bytes ' . $sFil7;
         }
         else {
            $sEco = Glb::$sTkNL . '☼ Save.a   1 : Nothing saved, nothing had changed';
         }
      }

      // Provide a DOMDocument object [seq 20190414°0325]
      // note : Two possibilities which seem to make no difference
      //   • $xCntns = file_get_contents($sFile); $doc->loadHTML($xCntns);
      //   • $doc->loadHTMLFile($sFile);
      $doc1 = new \DOMDocument;
      $doc1->validateOnParse = true;                                   // [line 20190414°0745]
      $doc1->preserveWhiteSpace = true;                                // just a try [line 20190414°0741] does not help, perhaps counterintuitive 'false' is wanted
      $doc1->encoding = 'UTF-8';                                       // just a try [line 20190414°0743] seems to make nothing
      $doc1->substituteEntities = false;                               // just a try [line 20190414°0748] does not help for entities
      $doc1->formatOutput = true;                                      // just a try [line 20190414°0742] does not help
      $doc1->loadHTMLFile($sFile);                                     // just a try [line 20190414°0744] seems to make no difference

      // Experiment [seq 20190414°0326]
      if (self::$b_IterateDom_Run) {
         self::iterateDom($doc1);
      }

      // Find target element for doc1 [line 20190414°0331]
      $elTarget = self::run_findElement($doc1, 'DafFurnitureCakecrumbs_Bag');

      // Debug [seq 20190414°0333]
      if (self::$b_Intro_PrintInput) {
         $sEco .= Glb::$sTkNL . '☼ Input : '    // finding : The Unicode chars seem fine here
                . Glb::$sTkNL . '           ---------------------------------- '
                . Glb::$sTkNL . $sPut
                . Glb::$sTkNL . '           ---------------------------------- '
                 ;
      }
      echo($sEco);

      // Run mode 2 createDocFrag [seq 20190414°0335]
      if (self::$b_Mode_2_CreateDocFrag_Run) {
         $ele2 = self::run2_DomCreateDocFrag($doc1, $sPut);
         self::run_replaceElementAndSave($elTarget, $ele2, $sFile, 2);
         $doc1 = null;
         $elTarget = null;
      }

      // () Run mode 3 new/load/import [seq 20190414°0336]
      if (self::$b_Mode_31_NewLoadImport_Run) {

         $doc1 = new \DOMDocument;
         $doc1->validateOnParse = true;                                // [line 20190414°0745`02]
         $doc1->preserveWhiteSpace = true;                             // [line 20190414°0741`02]
         $doc1->encoding = 'UTF-8';                                    // [line 20190414°0743`02]
         $doc1->substituteEntities = false;                            // [line 20190414°0748`02]
         $doc1->formatOutput = true;                                   // [line 20190414°0742`02]
         $doc1->loadHTMLFile($sFile);                                  // [line 20190414°0744`02]

         $elTarget = self::run_findElement($doc1, 'DafFurnitureCakecrumbs_Bag');

         $ele3 = $doc1->createElement('div');
         $ele3->encoding = 'UTF-8';                                    // Just a try [line 20190414°0746`13] makes no difference
         $ele3->preserveWhiteSpace = true;                             // Just a try [line 20190414°0746`11] makes no difference
         $ele3->formatOutput = true;                                   // Just a try [line 20190414°0746`12] makes no difference
         self::run3_DomNewLoadImport($ele3, $sPut);
         self::run_replaceElementAndSave($elTarget, $ele3, $sFile, 3);
      }

      return $sRet;
   }

   /**
    * This function finds an element ..
    *
    * @id 20190415°0151
    * @callers $sMark
    * @param $className {DOMDocumnet} The document to search inside
    * @param $offset {string} The search string, either an id or a class
    * @return {\DOMElement} The wanted element or FALSE if none is found
    */
   private static function run_findElement($doc, $sMark) // : \DOMElement
   {
      // Find target element [seq ]
      $elTarget = null;
      if (Glb::bToggle_FALSE) {
         // Wrong for now, but probably wanted later [line 20190414°0332]
         $elTarget = $doc->getElementById('id20160501o1721');          // DOMNode
      } else {
         // Search first of class [line 20190414°0717]
         $elTarget = self::run_findElment_ByClass($doc, 'div', $sMark); // 'DafFurnitureCakecrumbs_Bag'
      }

      // Debug [seq 20190414°0334]
      $sEco = Glb::$sTkNL . '☼ Found      :'
             . ' tag="' . ( $elTarget ? $elTarget->tagName : 'N/A' )
              . '", class=' . ( $elTarget ? $elTarget->getAttribute('class') : 'N/A' ) . '"'
               . ', type="' . ( $elTarget ? get_class($elTarget) : 'N/A' ) . '"' // DOMElement
                ;
      echo($sEco);

      return $elTarget;
   }

   /**
    * This function finds an element by class name
     * Explanation
    *   • The PHP DOMDocument classes have no method getElementByClass
    *   • This returns the nth element, n is given as (offset + 1)
    *   • To get list of all elements of this class, modification is needed
    *
    * @id 20190414°0721
    * @see Code after ref 20190414°0432 'stackoverflow → Getting DOM elements
    *       by classname', contribution by dav on 2014-Nov-3`04:53
    * @callers : Only • func 20190415°0151 JsiReplace:.run_findElement
    * @param $parentNode {DOMNode} The node to search, e.g. the document
    * @param $tagName {string} The tags to search
    * @param $className {string} The class name to search
    * @param $offset {integer} The index of the wanted element, starting at zero
    * @return {\DOMElement} The wanted element of the given class or FALSE if none is found
    */
   private static function run_findElment_ByClass ( &$parentNode
                                                   , $tagName
                                                    , $className
                                                     , $offset = 0
                                                      ) // : \DOMElement | Boolean
   {
      // Find element [seq 20190414°0723]
      $response = false;
      $childNodeList = $parentNode->getElementsByTagName($tagName);
      $tagCount = 0;
      for ($i = 0; $i < $childNodeList->length; $i++) {
         $temp = $childNodeList->item($i);
         if ( stripos($temp->getAttribute('class'), $className ) !== false) {
            if ($tagCount == $offset) {
               $response = $temp;
               break;
            }
            $tagCount++;
         }
      }
      return $response;
   }

   /**
    * This function replaces element and saves to file
    *
    * @id 20190415°0131
    * @return {string} Some answer
    * @param $elParent {DOMElement} The parent element, where the target resides
    * @param $elOld {DOMElement} The old element which is replaced
    * @param $elNew {DOMElement} The new element to be inserted
    * @param $sFilnam {string} The physical filename of the page
    * @param $iMode {integer} Mode 1, 2 or 3
    */
   private static function run_replaceElementAndSave($elOld, $elNew, $sFilnam, $iMode) : string {

      // Prologue [seq 20190415°0133]
      $sRet = '';

      // Adjustings [seq 20190414°0337]
      $elParent = $elOld->parentNode;
      $dnParent = $elParent;                   // probably superfluous // cast possible DOMElement to DOMNode

      // Get DOMDocument [line 20190415°0135]
      $doc = $dnParent->ownerDocument;

      // Debug [seq 20190414°0345]
      $sEco = Glb::$sTkNL . '☼ Replace  ' . $iMode . ' :'
             . ' elParent="' . $dnParent->nodeName . '"' . ' ' . get_class($dnParent)
              . ', elNew="' . $elNew->nodeName . '"' . ' ' . get_class($elNew)
               . ', elOld="' . $elOld->nodeName . '"' . ' ' . get_class($elOld)
                ;
      echo($sEco);

      // The actual task [seq 20190414°0347]
      $dnParent->replaceChild($elNew, $elOld);

      // Experiment, seems not to change anything [line 20190414°0743]
      $doc->normalizeDocument();

      // Build filename for write back [seq 20190414°0351]
      $s23 = substr($sFilnam, 0, strlen($sFilnam) - 5);
      $sFil2 = $s23 . '.v' . $iMode . '.html';

      // Write back to file [seq 20190414°0353]
      $i = $doc->saveHTMLFile($sFil2);
      $sEco = Glb::$sTkNL . '☼ Save.b   ' . $iMode . ' : ' . $i . ' bytes ' . $sFil2;

      echo($sEco);

      return $sRet;
   }

   /**
    * This function .. first experiment .. for evaluation purposes ..
    *  not useful, just a stub outputting a first message ..
    *
    * @id 20190414°0521
    * @todo Print warning if 'br' or 'img' tags are not self-closing
    * @callers • func 20190414°0321 replaceDomElement
    * @param $sCntns {string} The file contents
    * @param $sId {string} The element ID to search for
    * @param $sReplacement {string} The replacement fragment
    * @return $sRet {String|Boolean} The changed content or FALSE if nothing was to change
    */
   private static function run1_RegexSearch ($sCntns, $sId, $sReplacement) // : string
   {
      // Prologue [seq 20190414°0523]
      $sRet = '';
      $sPtStart = null;

      // Notification [seq 20190414°0525]
      $sEco = Glb::$sTkNL . '☼ Run mode 1 :';
      echo($sEco);

      // Search start location [seq 20190414°0527]
      $iCursor = 0;
      $iCursor = strpos($sCntns, 'class="' . $sId . '"'); // returns FALSE if needle was not found

      // Hm ..
      if ( $iCursor === FALSE) {
         return FALSE;
      }

      $iCursor = strpos($sCntns, '>', $iCursor);
      $sPtStart = $iCursor + 1;

      // Debug log [seq 20190414°0531]
      $sEco = Glb::$sTkNL . '      before : end=' . $iCursor
             . ' "' . Utils::restrict( substr($sCntns, 0 , $sPtStart), 65) . '"'
              ;
      echo($sEco);

      // Seek end of target area [seq 20190415°0411]
      // Explanation : Parse the plain HTML for the closing tag on 'level 1'
      $iLevel = 1;
      $a1 = array(); // avoid NetBeans complaining 'uninitialized' below // possible since PHP 5.4 : "$a1 = [];"
      while ($iLevel > 0) {

         // Get next tag [seq 20190415°0413]
         $b = preg_match("/\<.+?\>/", $sCntns, $a1, PREG_OFFSET_CAPTURE, $iCursor);
         if (! $b) {
            $iLevel = 0;
            $sEco = Glb::$sTkNL . '         rgx : Fatal. No end of target found, possible end of document.';
            echo($sEco);
            continue;
         }
         $sMtch = $a1[0][0];

         // Debug log [seq 20190415°0415]
         if ( Glb::bToggle_FALSE ) {
            $sEco = Glb::$sTkNL . '         rgx :'
                   . ' ofs=' . $iCursor . ' pos=' . $a1[0][1]
                    . ' lev=' . $iLevel . ' len=' . strlen($sMtch)
                     . ' "' . Utils::restrict($sMtch, 56) . '"'
                      ;
            echo($sEco);
         }

         // Save current position [seq 20190415°0417]
         $iCursor = $a1[0][1] + 1;

         // (q) Process find [seq 20190415°0421]
         // (q.1) Skip comments [seq 20190415°0423] '<!-- …>'
         if ( substr($sMtch, 1, 3) === '!--' ) {
            continue;
         }
         // (q.2) Skip self-closing tags [seq 20190415°0425] e.g. '<br />' and <img … />'
         else if ( substr($sMtch, strlen($sMtch) - 2, 1) === '/' ) {
            continue;
         }
         // (q.3) Process closing tags [seq 20190415°0427] '</…>'
         else if ( substr($sMtch, 1, 1) === '/' ) {
            $iLevel--;
         }
         // (q.4) Detect not-self-closing br and img tags [seq 20190415°0431]
         else if (( substr($sMtch, 1, 3) === 'img' ) || ( substr($sMtch, 1, 2) === 'br' )) {
            $sEco = Glb::$sTkNL . '              Warning — Not-self-closing br or img tag detected';
            echo($sEco);
         }
         // (q.5) Looks like normal tag [seq 20190415°0433]
         else {
            $iLevel++;
         }
      }

      // Adjust [line 20190414°0533]
      $iPtStop = $iCursor - 1;

      // Notification [line 20190414°0535]
      $sEco = Glb::$sTkNL . '       after :'
             . ' ' . Utils::restrict( substr($sCntns, $iPtStop), 80) . ' …'
              ;
      echo($sEco);

      // Do the replace [line 20190414°0537]
      $s_CLOSING_TAG_INDENT = ' ';                                     // const 20201129°1417, see JS seq 20201129°1421 'allow for closing tag indent'
      $sRet = substr($sCntns, 0,  $sPtStart)
             . $sReplacement
              . Glb::$sTkNL . $s_CLOSING_TAG_INDENT . substr($sCntns, $iPtStop)
               ;

      // [line 20190414°0541]
      return $sRet;
   }

   /**
    * Helper function using createDocumentFragment/appendXML.
    *  Not seriously considered to be used, is too picky with well-formed XML.
    *  Conserved just as example usage of DOMDocumentFragment.
    *
    * @id func 20190414°0541
    * @callers • none for now
    * @param $doc {DOMDocument}
    * @param $sPut {string}
    * @return {DOMDocumentFragment} The newly created element
    */
   private static function run2_DomCreateDocFrag($doc, $sPut) : \DOMDocumentFragment {

      // Try two [seq 20190414°0338]
      $frag = $doc->createDocumentFragment();

      // Dirty workaround against error "Entity 'nbsp' not defined" [line 20190414°0543]
      //  after ref 20190414°0424 'stackoverflow → appendXML with special characters'
      $sPutMod = preg_replace("/&nbsp;/", "__nbsp__", $sPut);

      $frag->appendXML($sPutMod);

      // Debug [seq 20190414°0343]
      $sEco = Glb::$sTkNL . '☼ Run mode 2 : {frag}'
             . ' nodeName="' . $frag->nodeName . '"'
              . ', cls="' . get_class($frag) . '"'
              . ', val="' . $frag->nodeValue . '"'
              . ', chlds="' . $frag->childNodes->length . '"'
               ;
      echo($sEco);

      return $frag;
   }

   /**
    * Helper function to create an element from HTML text fragment.
    *  Characteristics are • Create intermediate DOMDocument • Read given
    *  HTML fragment via loadHTML • Uses importNode to transfer the nodes
    *  to the target • No return value, operates on the given node/element
    *  • The point seems loadHTML, which is not available with other DOM types.
    *
    * @id func 20190414°0551
    * @see todo 20190414°0735 'dom inserting items left to fix'
    * @note Code after ref 20190414°0425 'stackoverflow → insert HTML to PHP DOMNode'
    * @param $parent {DOMElement} The newly created element to be filled
    * @param $source {string}
    * @return {void}
    */
   private static function run3_DomNewLoadImport($parent, $source)
   {
      // Sequence from user dav [seq 20190414°0553]
      $tmpDoc = new \DOMDocument();
      $tmpDoc->endcoding = 'UTF-8';                    // [line 20190414°0747] does not help
      $tmpDoc->preserveWhiteSpace = true;              // [line 20190414°0445] does not help
      $tmpDoc->formatOutput = true;                    // [line 20190414°0446] does not help
      $tmpDoc->loadHTML($source);
      foreach ($tmpDoc->getElementsByTagName('body')->item(0)->childNodes as $node) {
         $node = $parent->ownerDocument->importNode($node, true);
         $parent->appendChild($node);
      }

      // Debug [seq 20190414°0557]
      $sEco = Glb::$sTkNL . '☼ Run mode 3 : {parent}'
             . ' nodeName="' . $parent->nodeName . '"'
              . ', class="' . get_class($parent) . '"'
               ;
      if (self::$b_Mode_32_NewLoadImport_Print) {
         $sEco .= Glb::$sTkNL . 'Inspect inlay string from saveHTML (the source document seems fine, but the inlay has wrong chars)'
                . Glb::$sTkNL . '----------------------------------------------------------------------'
                . Glb::$sTkNL . $tmpDoc->saveHTML()                    // This chars are broken and it has doctype header HTML 4.0 Transitional//EN
                . Glb::$sTkNL . '----------------------------------------------------------------------'
                 ;
      }
      echo($sEco);
   }

   /**
    * This flag ..
    *
    * @id 20190415°0111
    * @var String
    */
   static private $b_Intro_PrintInput = FALSE;

   /**
    * This flag ..
    *
    * @id 20190415°0112
    * @var String
    */
   static private $b_IterateDom_Run = FALSE; // FALSE TRUE

   /**
    * This flag determines which of two possible replacement algorithms is used
    *
    * @id 20190415°0113
    * @var String
    */
   static private $b_Mode_1_TextSearch_Run = TRUE;

   /**
    * This flag ..
    *
    * @id 20190415°0114
    * @var String
    */
   static private $b_Mode_1_TextSearch_Run_Live = TRUE;

   /**
    * This flag ..
    *
    * @id 20190415°0115
    * @var String
    */
   static private $b_Mode_2_CreateDocFrag_Run = FALSE; // FALSE TRUE

   /**
    * This flag ..
    *
    * @id 20190415°0116
    * @var String
    */
   static private $b_Mode_31_NewLoadImport_Run = FALSE; // FALSE TRUE

   /**
    * This flag ..
    *
    * @id 20190415°0117
    * @var String
    */
   static private $b_Mode_32_NewLoadImport_Print = FALSE;
}

/**
 * This class shall provide .. ubiquitous utilities
 *
 * @id 20190415°0311
 */
class Utils
{
   /**
    * Restrict string length for nicer log lines
    *
    * @id 20190415°0321
    * @param $sText {string} The string to restrict
    * @param $iLen {integer} Maximum length of the wanted result string
    * @return {string} The wanted restricted string
    */
   static public function restrict($sText, $iLen) : string
   {
      if ( strlen($sText) > $iLen) {
         $iHalf = (int) ($iLen / 2) + 1;                               // Not sure about the '+ 1'
         $sText = substr($sText, 0, $iHalf)
                 . ' … '
                  . substr($sText, strlen($sText) - $iHalf, $iHalf)
                   ;
      }

      $sText = str_replace(array("\r", "\n"), '¤', $sText);

      return $sText;
   }
}
/* eof */
