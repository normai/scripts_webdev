<?php

/**
 * This module provides constant like values
 *
 * file      : 20110826°2241
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2024 Norbert C. Maier
 * authors   : ncm
 * encoding  : UTF-8-without-BOM
 * status    :
 * callers   : daftari.php
 */

namespace Trekta\Daftari;

// Initialize the non-const fields [seq 20190128°0315]
Globals::initialize();

/**
 * This class provides runtime constant-like values
 *
 * @id 20190128°0311
 * @callers Only daftari.php
 */
class Globals
{

   /**
    * Determine file system and URL key point self references
    *
    * @id method 20190204°0551
    * @note How exactly to do this job, is not obvious.
    * @note See how 20190204°0521 'retrieve FS paths and self URLs'
    * @note Somehow related is issue 20170914°1711 'find physical path of page'
    * @callers This class only
    */
   private static function findKeyPoints()
   {
      // The wanted key points [seq 20190204°0611]
      self::$Glr_sLocation_Daftari_Fs = '';
      self::$Glr_sLocation_Daftari_Url = '';
      self::$Glr_sLocation_ScanBase_Fs= '';
      self::$Glr_sLocation_ScanBase_Url = '';
      self::$Glr_sLocation_WebRoot_Fs = '';
      self::$Glr_sLocation_WebRoot_Url = '';

      // Provisory condition [seq 20190204°0621]
      if (self::$Lgn_bHostIsLocalhost)
      {
         // (A) Automatically calculate locations [seq 20190204°0623]
         // This is only reliable on localhost, and only possible if no alias
         //   or vhost is involved. Otherwise we need a manual configuration.

         // (A.1) This is only valid if no alias or vhost is used [seq 20190204°0631]
         // Value e.g. "X:/workspaces/"
         self::$Glr_sLocation_WebRoot_Fs = $_SERVER['DOCUMENT_ROOT'];

         // (A.2) Assemble web root URL [seq 20190204°0632]
         // note : This is only reliable on localhost
         // see : www.designcise.com/web/tutorial/how-to-check-for-https-request-in-php [ref 20220220°0922]
         // note : Value is e.g. "http://localhost"
         $s = (( $_SERVER['SERVER_PORT'] !== '80') ? (':' . $_SERVER['SERVER_PORT']) : '');
         $_SERVER['REQUEST_SCHEME'] = $_SERVER['REQUEST_SCHEME'] ?? "";        // Fix 20220222°0721 for fatal error "Undefined index: REQUEST_SCHEME"
         self::$Glr_sLocation_WebRoot_Url = $_SERVER['REQUEST_SCHEME']
                                           . '://' . $_SERVER['HTTP_HOST']
                                            . $s
                                             ;

         // (A.3) The Daftari folder [seq 20190204°0633]
         // Value e.g. "X:\workspaces\daftaridev\trunk\daftari"
         self::$Glr_sLocation_Daftari_Fs = realpath(__DIR__ . '\..\..');

         // (A.4) The Daftari URL [seq 20190204°0634]
         // Value e.g. "http://localhost/workspaces/daftaridev/trunk/daftari"
         $s = substr(self::$Glr_sLocation_Daftari_Fs, strlen(self::$Glr_sLocation_WebRoot_Fs));
         $s = str_replace("\\", '/', $s);
         self::$Glr_sLocation_Daftari_Url = self::$Glr_sLocation_WebRoot_Url . '/'. $s;

         // (A.5) The Gallery scan root folder [seq 20190204°0635]
         //  So far simply use the Daftari parent folder
         // Value e.g. "X:\workspaces\daftaridev\trunk"
         $s = realpath(__DIR__ . '\..\..\..');
         self::$Glr_sLocation_ScanBase_Fs = realpath(__DIR__ . '\..\..\..');

         // (A.6) The Gallery scan root URL [seq 20190204°0636]
         // Value e.g. "http://localhost/workspaces/daftaridev/trunk"
         $s = substr(self::$Glr_sLocation_ScanBase_Fs, strlen(self::$Glr_sLocation_WebRoot_Fs));
         $s = str_replace("\\", '/', $s);
         self::$Glr_sLocation_ScanBase_Url = self::$Glr_sLocation_WebRoot_Url . '/' . $s;
      }
      else
      {
         // (B) Manual configuration [seq 20190204°0624] not yet available
      }

      // Determine base paths [seq 20190204°0511] old code
      Globals::$Glr_sBasePathImgFolders = self::$Glr_sLocation_ScanBase_Fs;
      Globals::$Glr_sUrlImageRepoBase = self::$Glr_sLocation_ScanBase_Url;
      Globals::$Glr_sBasePathToUse = self::$Glr_sLocation_ScanBase_Url;

      // Debug info [seq 20190204°0607]
      if ( Globals::bToggle_FALSE )
      {
         // About the file system
         $s21 = $_SERVER['CONTEXT_DOCUMENT_ROOT'];     // e.g. "X:/workspaces/"
         $s22 = $_SERVER['DOCUMENT_ROOT'];             // ✱ e.g. "X:/workspaces/" taken from httpd.conf DocumentRoot
         $s23 = $_SERVER['SCRIPT_FILENAME'];           // e.g. "X:/workspaces/daftaridev/trunk/daftari/docs/gallery56raw.html"

         // About the URL
         $s11 = $_SERVER['HTTP_HOST'];                 // e.g. "localhost" taken from client request
         $s12 = $_SERVER['PHP_SELF'];                  // e.g. "/workspaces/daftaridev/trunk/daftari/docs/gallery56raw.html"
         $s13 = $_SERVER['REMOTE_ADDR'];               // ✱ e.g. "127.0.0.1" the addres to send the response
         $s14 = $_SERVER['REQUEST_SCHEME'];            // e.g. "http"
         $s15 = $_SERVER['SERVER_NAME'];               // e.g. "localhost" taken from httpd.conf ServerName
         $s16 = $_SERVER['SERVER_PORT'];               // e.g. "80"
      }
   }

   /**
    * This method provides the brick to showing the logged-in user on the page
    *
    * @id func 20120916°1911
    * @note At time of calling, may be there is no $_SESSION[Glb::SESKEY_username], even if session exists.
    * @return string The wanted username brick
    */
   public static function getUserInfoBrick()
   {
      $s = '';
      if ( self::$Ses_bIsLoggedin === TRUE )
      {
         $s = 'User = <b>' . self::$Ses_sUserShortName . '</b>';
      }
      return $s;
   }

   /**
    * This static method initializes the 'global runtime constant values'
    *
    * @id func 20190128°0321
    * @todo : The visibility is public because it is called from outside,
    *     means from script level. It rather should be private, this might
    *     be achieved by putting the call into a constructor.
    * @callers This script level only (this is why it is public but should be private)
    */
   public static function initialize()
   {
      // 'explore script environment' [seq 20110831°2043]
      self::$Ses_sDataDirFullname = dirname(dirname(dirname(__FILE__))) . '.DATA';
      self::$Cfg_sInifileFullname = self::$Ses_sDataDirFullname . '/' . 'daftari.ini';

      // [seq 20190128°0331]
      // todo : Here is the first of a series of filter_input usages. Make a bulk implementation.
      self::$Env_sRequestedScriptName = filter_input(INPUT_SERVER, 'SCRIPT_NAME', FILTER_DEFAULT);

      // [seq 20190128°0341]
      // todo : Refine this seqence to tell Linux from Mac
      // note : See ref 20190128°0343 and ref 20190128°0344
      if ( strcasecmp(substr(PHP_OS, 0, 3), 'WIN') == 0 )
      {
         self::$sTkNL = "\r\n";
      } else {
         self::$sTkNL = "\n";
      }

      // Determine being on localhost or not [seq 20190128°0443]
      // finding : Field 'REMOTE_ADDR' seems the only reliable information, about being
      //    on localhost or not. See howto 20190204°0521 'retrieve FS paths and self URLs'
      $sServer = $_SERVER[self::SVR_KEY_REMOTE_ADDR];
      if (($sServer === '127.0.0.1') || ($sServer === '::1'))
      {
         Globals::$Lgn_bHostIsLocalhost = TRUE;
      }

      // [line 20190204°0547]
      self::findKeyPoints();

      // Just for fun [seq 20190129°1023]
      self::outDbg_Fileinfo(__FILE__);
   }

   /**
    * This static method outputs some debug info about a given file
    *
    * @id 20190128°0521
    * @param $sFile {String} The file about which to output infos
    */
   public static function outDbg_Fileinfo($sFile)
   {
      // [seq 20110824°2125]
      $bDEBUG_OUTPUT_INTRO = FALSE; // TRUE; // FALSE;
      if ($bDEBUG_OUTPUT_INTRO)
      {
         // (1) prologue
         $s = '<div style="border:7px solid orange; background-color:yellow; padding:0.7em;">';
         $s .= '<table>';
         $s .= '<tr><td colspan="2">*** Debug 20110824°2125 : pathinfo() ***</td></tr>';
         $s .= '<tr><td>$sFile</td><td>= ' . $sFile . '</td></tr>';

         // (2)
         forEach (pathinfo($sFile) as $key => $value) {
            $s .= '<tr><td>sFile[' . $key . ']</td><td>= ' . $value . '</td></tr>';
         }

         // (3)
         $sDirnam = pathinfo($sFile)['dirname'] ?? '';
         $aPathinfo = pathinfo( $sDirnam );
         forEach ( $aPathinfo as $key => $value ) {
            $s .= '<tr><td>dirname[' . $key . ']</td><td>= ' . $value . '</td></tr>';
         }

         // (4) Footer
         $s .= '<tr> <td>datadirfull</td><td>= ' . self::$Ses_sDataDirFullname . '</td></tr>';
         $s .= '</table>';
         $s .= '</div>';
         echo ($s);
      }
   }

   /**
    * This static field stores the time retrieved from getBcTime
    *
    * @id field 20110826°0123
    * @var String
    */
   public static $Bct_sExecutionStartTime = '0';

   /**
    * This static field tells ..
    *
    * @id field 20110826°0527
    * @note : Value e.g.: $Ses_sDataDirFullname . '/' . 'daftari.ini'
    * @var String
    */
   public static $Cfg_sInifileFullname = '';

   /**
    * This static value tells the script's filename
    *
    * This string tells the filename of the requested script or page, even
    *  if the request was for a folder, without page name. Values e.g.:
    *   •  "/index.html"
    *   •  "/daftaridev/trunk/daftari/docs/cakecrumbs3.html"
    *
    * @id field 20190128°0332
    * @var String
    */
   public static $Env_sRequestedScriptName = '';

   /**
    * This static field tells the full path of the folder the called script is inside
    *
    * @id field 20110826°0553
    * @todo Implement initialization
    * @note Curiously not really used, juat for one debug message
    * @var String
    */
   public static $Env_sThisScriptDirFullname = '';

   /**
    * This static field tells the root folder the Gallery scanner.
    *  The value is set during class Globals initialization.
    *
    * @id field 20110826°0133
    * @var String
    */
   public static $Glr_sBasePathImgFolders = '';

   /**
    * This static field tells ..
    *
    * @id field 20110826°0137
    * @note This field is set below
    * @var String
    */
   public static $Glr_sBasePathToUse = '';

   /**
    * This static field tells ..
    *
    * @id field 20110826°0135
    * @note This field is set below
    * @var String
    */
   public static $Glr_sUrlImageRepoBase = '';

   /**
    * This static field tells whether the server is on localhost or not.
    *  The value is written in func 20190128°0321 Globals::initialize
    *
    * @id field 20110826°0127
    * @var Boolean
    */
   public static $Lgn_bHostIsLocalhost = FALSE;

   /**
    * This static field tells the Daftari key point as file system path
    *
    * @id field 20190204°0612
    * @var String
    */
   public static $Glr_sLocation_Daftari_Fs = '';

   /**
    * This static field tells the Daftari key point as URL
    *
    * @id field 20190204°0613
    * @var String
    */
   public static $Glr_sLocation_Daftari_Url = '';

   /**
    * This static field tells the Gallery scan base key point as file system path
    *
    * @id field 20190204°0614
    * @var String
    */
   public static $Glr_sLocation_ScanBase_Fs = '';

   /**
    * This static field tells the Gallery scan base key point as URL
    *
    * @id field 20190204°0615
    * @var String
    */
   public static $Glr_sLocation_ScanBase_Url = '';

   /**
    * This static field tells the web root key point as file system path
    *
    * @id field 20190204°0616
    * @var String
    */
   public static $Glr_sLocation_WebRoot_Fs = '';

   /**
    * This static field tells the web root key point as URL
    *
    * @id field 20190204°0617
    * @var String
    */
   public static $Glr_sLocation_WebRoot_Url = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0505
    * @var String
    */
   public static $Lgn_sMsg_ChangePw_Color = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0503
    * @var String
    */
   public static $Lgn_sMsg_ChangePw_Text = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0513
    * @var String
    */
   public static $Lgn_sMsg_PwForgotten_Text = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0525
    * @var String
    */
   public static $Lgn_sMsg_Register_Color = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0523
    * @var String
    */
   public static $Lgn_sMsg_Register_Text = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0535
    * @note Seems not really used yet
    * @var String
    */
   public static $Lgn_sMsg_Status_Color = '';

   /**
    * This static field holds the status message displayed on the login page
    *
    * @id field 20190129°0533
    * @var String
    */
   public static $Lgn_sMsg_Status_Text = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0545
    * @var String
    */
   public static $Lgn_sMsg_Unsubscribe_Color = '';

   /**
    * This static field tells ..
    *
    * @id field 20190129°0543
    * @var String
    */
   public static $Lgn_sMsg_Unsubscribe_Text = '';

   /**
    * This static field tells ..
    *
    * @id field 20110826°0453
    * @todo : Eliminate this?
    * @var Boolean
    */
   public static $Ses_bIsLoggedin = FALSE;

   /**
    * This static field tells the data dir fullname. It is calculated during
    *  the class initialization
    *
    * @id field 20110826°0525
    * @note Compare func 20110507°1223 DbTxt::__construct sequence 20111002°2021 'guarantee datadir'
    * @note Value e.g.: dirname(dirname(__FILE__)) . '.DATA'
    * @var String
    */
   public static $Ses_sDataDirFullname = '';

   /**
    * This static field tells ..
    *
    * @id field 20110423°0413
    * @var String
    */
   public static $Ses_sLocalhostInfo = '';

   /**
    * This static field tells the user shortname or blank if not logged-in
    *
    * @id field 20190205°0511
    * @var String
    */
   public static $Ses_sUserShortName = '';

   /**
    * Static field providing the OS specific newline character.
    *   The value is set during program initialisation
    *
    * @id field 20190128°0342
    * @var String
    */
   public static $sTkNL = "";

   /**
    * This constant field tells ..
    *
    * @id const 20110826°0143
    * @var String
    */
   const DEFVAL_GLR_iALBUM_COLUMNS = 4;

   /**
    * This constant field tells ..
    *
    * @id const 20110826°0145
    * @var String
    */
   const DEFVAL_GLR_iALBUM_ROWS = 2;

   /**
    * This constant field tells the default thumbnail size
    *
    * @id const 20110826°0147
    * @note Formerly line "const DEFVAL_GLR_sALBUM_THUMBSIZE = 'medium';"
    * @var String
    */
   const DEFVAL_GLR_iALBUM_THUMBSIZE = 1;

   /**
    * This constant field tells ..
    *
    * @id const 20110826°0153
    * @var String
    */
   const DEFVAL_GLR_iOBJECT_IMAGESIZE = 560;

   /**
    * This constant field tells ..
    *
    * @id const 20110826°0435
    * @note Seems not used
    * @todo Somehow implement a default thumbnail, but from the daftari folders
    * @var String
    */
   const DEFVAL_GLR_sTHUMB180LINE  = '   thumb0180 : http://localhost/workspaces/img12345678/img/19910805o0712.testbild2.x0180y0179q85.jpg';

   /**
    * This constant tells the GET array key for ..
    *
    * @id const 20110423°0553
    * @var String
    */
   const GET_KEY_AJX_file = 'file';

   /**
    * This constant tells the GET array key for ..
    *
    * @id const 20110423°0555
    * @var String
    */
   const GET_KEY_AJX_mode = 'mode';

   /**
    * This constant tells the GET array key for ..
    *
    * @id const 20110423°0557
    * @var String
    */
   const GET_KEY_AJX_tagid = 'tagid';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0155
    * @callers PageAlbum.php
    * @var String
    */
   const GET_KEY_GLR_album = 'album';

   /**
    * This constant provides .. used in ~~gallery56object.html
    *
    * @id const 20110826°0157
    * @var String
    */
   const GET_KEY_GLR_object = 'object';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0213
    * @callers PageAlbum.php
    * @var String
    */
   const GET_KEY_GLR_page = 'page';

   /**
    * This constant is the Enum value to access the files via local files ystem
    *
    * @id const 20110423°0453
    * @var String
    */
   const GLR_ACCESSSTYLE_localdrive = 'localdrive';

   /**
    * This constant is the Enum value to access the files via SVN WebDAV protocol
    *
    * @id const 20110423°0455
    * @var String
    */
   const GLR_ACCESSSTYLE_svnclient = 'svn-client';

   /**
    * This constant provides an array index for the Album field
    *   in the ~~ ShadowBuddy array. Todo : Clear the difference to field 'albums'.
    *
    * @id const 20110423°0121
    * @var String
    */
   const GLR_BUDY_KEY__album = 'album';

   /**
    * This constant provides an array index for the Albums field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0123
    * @var String
    */
   const GLR_BUDY_KEY_albums = 'albums';

   /**
    * This tells the Buddy array index for the Authors field
    *
    * @id const 20110423°0145
    * @var String
    */
   const GLR_BUDY_KEY_author = 'author';

   /**
    * This constant tells the Buddy array key for the Archive field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0125
    * @var String
    */
   const GLR_BUDY_KEY_archive = 'archive';

   /**
    * This constant provides an array index for the Copyright field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0127
    * @var String
    */
   const GLR_BUDY_KEY_copyright = 'copyright';

   /**
    * This constant provides an array index for the Corename field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0131
    * @var String
    */
   const GLR_BUDY_KEY_corename = 'corename';

   /**
    * This constant tells the ShadowBuddy array key for the .. field
    *
    * @id const 20110423°0423
    * @var String
    */
   const GLR_BUDY_KEY_description = 'description';                     // GLR_SHDW_KEY_?

   /**
    * This constant provides an array index for the FileFamily field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0133
    * @var String
    */
   const GLR_BUDY_KEY_filefamily = 'filefamily';

   /**
    * This constant tells the ShadowBuddy array key for the .. field
    *
    * @id const 20110423°0425
    * @var String
    */
   const GLR_BUDY_KEY_inscription = 'inscription';                     // GLR_SHDW_KEY_?

   /**
    * This constant provides an array index for the License field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0135
    * @var String
    */
   const GLR_BUDY_KEY_license = 'license';

   /**
    * This constant tells the ShadowBuddy array key for the .. field
    *
    * @id const 20110423°0427
    * @var String
    */
   const GLR_BUDY_KEY_location = 'location';                           // GLR_SHDW_KEY_?

   /**
    * This constant tells the ShadowBuddy array key for the .. field
    *
    * @id const 20110423°0433
    * @var String
    */
   const GLR_BUDY_KEY_objecttype = 'objecttype';                       // GLR_SHDW_KEY_?

   /**
    * This constant tells the ShadowBuddy array key for the .. field
    *
    * @id const 20110423°0435
    * @var String
    */
   const GLR_BUDY_KEY_owner = 'owner';                                 // GLR_SHDW_KEY_?

   /**
    * This constant provides an array index for the Policy field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0137
    * @var String
    */
   const GLR_BUDY_KEY_policy = 'policy';

   /**
    * This constant provides an array index for the Timestamp field
    *   in the ShadowBuddy array
    *
    * @id const 20110423°0141
    * @var String
    */
   const GLR_BUDY_KEY_timestamp = 'timestamp';

   /**
    * This tells the ShadowBuddy array key for the .. field
    *
    * @id const 20110423°0143
    * @var String
    */
   const GLR_BUDY_KEY_title = 'title';


   /**
    * This constitutes an Enum for the selectable thumbnail sizes
    *
    * Why define an Enum this way?
    * (1) There exists class SplEnum for native Enums, but this  requires
    *      an additional class, but I want have it inside class Globals.
    * (2) Since above PHP 5.6 constant array are possible, so let's try this.
    *
    * @id const 20190130°0121
    * @var Array
    */
   const GLR_ENUM_THUMBSIZES = array('Small', 'Medium', 'Large');

   /**
    * This tells the Enum array index for Thumbnail_Size_Large
    *
    * @id const 20190130°0122
    * @var Integer
    */
   const GLR_ENUM_THUMBSIZE_Large = 2;

   /**
    * This tells the Enum array index for Thumbnail_Size_Medium
    *
    * @id const 20190130°0123
    * @var Integer
    */
   const GLR_ENUM_THUMBSIZE_Medium = 1;

   /**
    * This tells the Enum array index for Thumbnail_Size_Small
    *
    * @id const 20190130°0124
    * @var Integer
    */
   const GLR_ENUM_THUMBSIZE_Small = 0;

   /**
    * This constant tells the BuddyShadow array key for the AdditionalFilenameComponent field.
    *  The Additionals are any additional components to the object/file/image e.g. 'BACKUP'
    *
    * @id const 20110423°0611
    * @var String
    */
   const GLR_SHDW_KEY_addi = 'addi';

   /**
    * This constant tells the BuddyShadow array key for the Bits field
    *
    * @id const 20110423°0613
    * @var String
    */
   const GLR_SHDW_KEY_bits = 'bits';

   /**
    * This constant tells the BuddyShadow array key for the Chan field
    *
    * @id const 20110423°0615
    * @todo Check why do exist fields 'chan' plus 'channels'. What is the
    *        difference? Can they be merged? [todo 20190130°0133 'check channels']
    * @var String
    */
   const GLR_SHDW_KEY_chan = 'chan';

   /**
    * This constant tells the BuddyShadow array key for the Channels field
    *
    * @id 20110423°0615
    * @note See todo 20190130°0133 'check channels'
    * @var String
    */
   const GLR_SHDW_KEY_channels = 'channels';

   /**
    * This constant tells the BuddyShadow array key for the .. field
    *
    * @id const 20110423°0617
    * @var String
    */
   const GLR_SHDW_KEY_err = 'err';
   /**
    * This constant tells the BuddyShadow array key for the FilenameExtension field
    *
    * @id const 20110423°0621
    * @var String
    */
   const GLR_SHDW_KEY_ext = 'ext';

   /**
    * This constant tells the BuddyShadow array key for the Files field (?)
    *
    * @id const 20110423°0655
    * @var String
    */
   const GLR_SHDW_KEY_file = 'file';

   /**
    * This constant tells the BuddyShadow array key for the FilesArray field
    *
    * @id const 20110423°0623
    * @var String
    */
   const GLR_SHDW_KEY_files = 'files';

   /**
    * This constant tells the BuddyShadow array key for the FlavourString field
    *
    * @id const 20110423°0625
    * @var String
    */
   const GLR_SHDW_KEY_flav = 'flav';

   /**
    * This constant tells the BuddyShadow array key for the Percent field
    *
    * @id const 20110423°0627
    * @var String
    */
   const GLR_SHDW_KEY_fpc = 'fpc';

   /**
    * This constant tells the BuddyShadow array key for the Qality field
    *
    * @id const 20110423°0631
    * @var String
    */
   const GLR_SHDW_KEY_fpq = 'fpq';

   /**
    * This constant tells the BuddyShadow array key for the Graphicstype field
    *
    * @id const 20110423°0633
    * @var String
    */
   const GLR_SHDW_KEY_gtyp = 'gtyp';

   /**
    * This constant tells the BuddyShadow array key for the .. field
    *
    * @id const 20110423°0635
    * @var String
    */
   const GLR_SHDW_KEY_height = 'height';

   /**
    * This constant tells the BuddyShadow array key for the .. field
    *
    * @id const 20110423°0637
    * @var String
    */
   const GLR_SHDW_KEY_maxdim = 'maxdim';

   /**
    * This constant tells the BuddyShadow array key for the Mimetype field
    *
    * @id const 20110423°0641
    * @var String
    */
   const GLR_SHDW_KEY_mime = 'mime';

   /**
    * This constant tells the BuddyShadow array key for the .. field
    *
    * @id const 20110423°0643
    * @var String
    */
   const GLR_SHDW_KEY_pext = 'pext';

   /**
    * This constant tells the BuddyShadow array key for the Filesize field
    *
    * @id const 20110423°0645
    * @var String
    */
   const GLR_SHDW_KEY_size = 'size';

   /**
    * This constant tells the .. key for the URL attribute
    *
    * @id const 20190204°0751
    * @var String
    */
   const GLR_SHDW_KEY_url = 'url';

   /**
    * This constant tells the BuddyShadow array key for the Version field
    *
    * @id const 20110423°0647
    * @var String
    */
   const GLR_SHDW_KEY_ver = 'ver';

   /**
    * This constant tells the BuddyShadow array key for the .. field
    *
    * @id const 20110423°0651
    * @var String
    */
   const GLR_SHDW_KEY_width = 'width';

   /**
    * This constant tells the BuddyShadow array key for the XmlError field
    *
    * @id const 20110423°0653
    * @var String
    */
   const GLR_SHDW_KEY_xmlerror = 'xmlerror';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0441
    * @var String
    */
   const GLR_SHDX_KEY_html = 'html';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0443
    * @var String
    */
   const GLR_SHDX_KEY_nheight = 'nheight';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0445
    * @var String
    */
   const GLR_SHDX_KEY_nmaxdim = 'nmaxdim';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0447
    * @var String
    */
   const GLR_SHDX_KEY_nwidth = 'nwidth';

   /**
    * This constant provides .. a hardcoded flag to switch logging on/off
    *
    * @id const 20110826°0113
    * @note Generated on https://www.random.org [ref 20190128°0136]
    * @var String
    */
   const GLR_bWRITE_ERRORLOG = TRUE;

   /**
    * This constant field provides ..
    *
    * @id field 20110826°0115
    * @check 20110518°1948 : How to find the optimum number or better make value self-adaptive?
    * @note Archives img12670204, img19890101, imgp20080701 are good candidates to test this value.
    * @note The optimum number depends on:
    *    • The number of images shown on the album page (all must be processed in one request)
    *    • The available system resources, e.g concurring processes
    * @note Values tried so far (May 2011) are:
    *     — $iGETIMAGESIZE_MAX_SIZE = 400000;  — Has proven faithful (with 8 images per page)
    *     — $iGETIMAGESIZE_MAX_SIZE = 1400000; — Might cause 30-sec-timeouts with 8 images per page
    *     — $iGETIMAGESIZE_MAX_SIZE = 3000000; — Experimental
    *     — $iGETIMAGESIZE_MAX_SIZE = 4000000; — May cause 30-sec-timeout with 8 images per page
    * @var String
    */
   const GLR_iGETIMAGESIZE_MAX_SIZE = 3000000;

   /**
    * This constant field provides ..
    *
    * @id const 20110826°0117
    * @note Empirical finding : 8 will leave two trailing '0' in the used numbers
    * @var Integer
    */
   const GLR_iPRECISION_BC_TIMEGAUGING = 8;

   /**
    * This constant provides ..
    *
    * @id const 20110826°0215
    * @var String
    */
   const GLR_sALBUMSFOLDER = 'dbtxt/albums/';

   /**
    * This constant provides a link fragment used in PageRaw.php and PageAlbum.php
    *
    * @id const 20110826°0217
    * @var String
    */
   const GLR_sLINKPART_RAWFOLDER = 'rawfolder.';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0223
    * @callers PageRaw.php and PageAlbum.php
    * @var String
    */
   const GLR_sPATHPART_INTERCALARY = 'trunk/img';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0537
    * @note Seems not much used yet
    * @var String
    */
   const INI_sINIKEY_Creationtime = 'Creationtime';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0545
    * @note Seems not much used yet
    * @var String
    */
   const INI_sINIKEY_sSqlCommand = 'sSqlCommand';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0543
    * @note Seems not much used yet
    * @var String
    */
   const INI_sINIKEY_TestUtf8 = 'TestUtf8';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0535
    * @note Seems not much used yet
    * @var String
    */
   const INI_sSECTION_General = 'General';

   /**
    * This constant provides one random string
    *
    * @id const20190128°0431
    * @note Generated on https://www.random.org [ref 20190128°0136]
    * @var String
    */
   const LGN_sMD5SALTONE = 'pfpcwnbhycbocmm';

   /**
    * This constant provides another random string
    *
    * @id const20190128°0441
    * @var String
    */
   const LGN_sMD5SALTTWO = 'ybhqbusicvetqec';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0712
    * @var String
    */
   const PHP_XCP_KEY_code = 'code';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0713
    * @var String
    */
   const PHP_XCP_KEY_column = 'column';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0714
    * @var String
    */
   const PHP_XCP_KEY_file = 'file';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0715
    * @var String
    */
   const PHP_XCP_KEY_level = 'level';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0716
    * @var String
    */
   const PHP_XCP_KEY_line = 'line';

   /**
    * This constant tells the BuddyShadow array FileFamilyExtra key for the .. field
    *
    * @id const 20110423°0717
    * @var String
    */
   const PHP_XCP_KEY_message = 'message';

   /**
    * This constant tells the .. array key for ..
    *
    * @id const 20190129°0551
    * @var String
    */
   const POSTKEY_GLR_album_form_was_sent = 'album_form_was_sent';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0431
    * @var String
    */
   const POSTKEY_GLR_seetings_album_maxrows = 'settings_album_maxrows';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0432
    * @var String
    */
   const POSTKEY_GLR_settings_album_columns = 'settings_album_columns';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0433
    * @var String
    */
   const POSTKEY_GLR_submit_albums_add = 'submit_albums_add';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0434
    * @var String
    */
   const POSTKEY_GLR_submit_albums_remove = 'submit_albums_remove';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0435
    * @var String
    */
   const POSTKEY_LGN_changepass_button_register = 'changepass_button_register';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0436
    * @var String
    */
   const POSTKEY_LGN_changepass_text_kennwort1 = 'changepass_text_kennwort1';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0437
    * @var String
    */
   const POSTKEY_LGN_changepass_text_kennwort2 = 'changepass_text_kennwort2';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0438
    * @var String
    */
   const POSTKEY_LGN_forgotpass_button_forgotpass = 'forgotpass_button_forgotpass';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0441
    * @var String
    */
   const POSTKEY_LGN_login_button_enter = 'login_button_enter';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0442
    * @var String
    */
   const POSTKEY_LGN_login_form_was_sent = 'login_form_was_sent';

   /**
    * This constant tells the POST array key for ..
    *
    * @id const 20110423°0417
    * @var String
    */
   const POSTKEY_LGN_register_button_register = 'register_button_register';

   /**
    * This constant tells the POST array key for ..
    *
    * @id const 20110423°0423
    * @var String
    */
   const POSTKEY_LGN_register_text_emailaddress = 'register_text_emailaddress';

   /**
    * This constant tells the POST array key for ..
    *
    * @id const 20110423°0425
    * @var String
    */
   const POSTKEY_LGN_register_text_kennwort = 'register_text_kennwort';

   /**
    * This constant tells the .. array key for ..
    *
    * @id const 20110423°0415
    * @var String
    */
   const POSTKEY_LGN_register_text_userfullname = 'register_text_userfullname';

   /**
    * This constant tells the POST array key for ..
    *
    * @id const 20110423°0427
    * @var String
    */
   const POSTKEY_LGN_register_text_username = 'register_text_username';

   /**
    * This constant tells the $_SESSION[] key for a user setting
    *
    * @id const 20190129°0553
    * @var String
    */
   const POSTKEY_LGN_submit = 'submit';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0443
    * @var String
    */
   const POSTKEY_LGN_unsubscribe_button_really_unsubscribe = 'unsubscribe_button_really_unsubscribe';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0444
    * @var String
    */
   const POSTKEY_LGN_unsubscribe_button_unsubscribe = 'unsubscribe_button_unsubscribe';

   /**
    * This constant tells the .. key for a user setting
    *
    * @id const 20190129°0555
    * @var String
    */
   const POSTKEY_LGN_user = 'user';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0445
    * @var String
    */
   const POSTKEY_SET_settings_checkboxes = 'settings_checkboxes';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0446
    * @var String
    */
   const POSTKEY_SET_settings_form_was_sent = 'settings_form_was_sent';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0447
    * @var String
    */
   const POSTKEY_TOL_DNS_hostname = 'hostname';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0448
    * @var String
    */
   const POSTKEY_TOL_DNS_ipaddress = 'ipaddress';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0451
    * @var String
    */
   const POSTKEY_TOL_UPL_numfiles = 'numfiles';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0452
    * @var String
    */
   const POSTKEY_TOL_UPL_regnum = 'regnum';

   /**
    * This tells the POST array key for element ..
    *
    * @id const 20110423°0453
    * @var String
    */
   const POSTKEY_TOL_UPL_sendfiles = 'sendfiles';

   /**
    * This constant is the SESSION array key for the MaintenanceMode flag.
    *  If this flag is switched on, e.g. the ShadowBuddy files are built.
    *  If this flag is switched off, e.g. the Gallery runs only on the
    *  already created data.
    *
    * @id const 20110826°0347
    * @var String
    */
   const SESKEY_MaintenanceMode = 'MaintenanceMode';

   /**
    * Constant $_SESSION key for the main debug flag, which is a user setting boolean.
    *
    * @id 20110826°0257
    * @var String
    */
   const SESKEY_bDebug = 'bDebug';

   /**
    * Constant $_SESSION key for the flag to show session info or not.
    *
    * @id 20110826°0313
    * @var String
    */
   const SESKEY_bMore010_sessionInfo = 'bMore010';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0315
    * @var String
    */
   const SESKEY_bMore020 = 'bMore020';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0317
    * @var String
    */
   const SESKEY_bMore030 = 'bMore030';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0323
    * @var String
    */
   const SESKEY_bMore040 = 'bMore040';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0327
    * @var String
    */
   const SESKEY_bMore060inivarCanary  = 'bMore060inivarCanary';

   /**
    * This constant provides .. usersetting
    *   .. default = TRUE so far (as long as svnclient readings are not cached)
    *
    * @id const 20110826°0345
    * @var String
    */
   const SESKEY_bReadlocaldrive = 'bAccessLocalDrive';

   /**
    * This constant field tells the $_SESSION[] key for a user setting
    *
    * @id 20110826°0237
    * @var String
    */
   const SESKEY_iAlbumColumns = 'iAlbumColumns';

   /**
    * This constant field tells the $_SESSION key for user setting
    *
    * @id 20110826°0243
    * @var String
    */
   const SESKEY_iAlbumMaxRows = 'iAlbumMaxRows';

   /**
    * This constant provides user setting
    *
    * @id const 20110826°0343
    * @var String
    */
   const SESKEY_iObjectImagesize = 'iObjectImagesize';

   /**
    * This constant field tells a $_SESSION key. This array element either
    *  stores the login hash value, or the element is unset
    *
    * @id 20110826°0245
    * @var String
    */
   const SESKEY_login = 'login';

   /*
    * issue 20110508°1933 'is storing plain text pw useful'
    * text : In the POST and in the SESSION array with key SESKEY_pass
    *    the plaintext password is stored. Is this useful or necessary?
    *     Is it useful to keep this element or, better eliminate it?
    * usage : Used in Login class to calculate the hash from.
    * status : open
    */

   /**
    * This constant field tells the $_SESSION key for the array element
    *  storing the password in plain text.
    *
    * @todo : Clear issue 20110508°1933 'is storing plain text pw useful'
    * @id 20110826°0247
    * @var String
    */
   const SESKEY_pass = 'pass';

   /**
    * This constant provides ..
    *  .. the values can be none, guest, user, full
    *
    * @id const 20110826°0353
    * @var String
    */
   const SESKEY_privileges = 'privileges';

   /**
    * This constant field tells the $_SESSION key for the request counter,
    *  which is of type integer.
    *
    * @id 20190129°0557
    * @var String
    */
   const SESKEY_requestcount = 'requestcount';

   /**
    * This constant provides .. usersetting
    *
    * @id const 20110826°0337
    * @var String
    */
   const SESKEY_sAlbumThumbsize = 'sAlbumThumbsize';

   /**
    * This constant field tells the $_SESSION key for the user's full name
    *
    * @id 20110826°0255
    * @var String
    */
   const SESKEY_userfullname = 'userfullname';

   /**
    * Constant telling the $_SESSION key for the user's short name
    *
    * @id 20110826°0253
    * @var String
    */
   const SESKEY_username = 'username';

   /**
    * This constant provides a value for the 'privileges' session key
    *
    * @id const 20110826°0355
    * @var String
    */
   const SESVAL_PRIVIL_full = 'full';

   /**
    * This constant provides a value for the 'privileges' session key
    *
    * @id const 20110826°0357
    * @var String
    */
   const SESVAL_PRIVIL_guest = 'guest';

   /**
    * This constant provides a value for the 'privileges' session key
    *
    * @id const 20110826°0413
    * @var String
    */
   const SESVAL_PRIVIL_none = 'none';

   /**
    * This constant provides a value for the 'privileges' session key
    *  .. just an idea, not used yet?
    *
    * @id const 20110826°0415
    * @var String
    */
   const SESVAL_PRIVIL_user = 'user';

   /**
    * This constant provides .. priviledged
    *
    * @id const 20110826°0225
    * @var String
    */
   const SESVAL_sUSERNAME_admin = 'admin';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0227
    * @var String
    */
   const SESVAL_sUSERNAME_guest = 'guest';

   /**
    * This constant provides .. priviledged
    *
    * @id const 20110826°0233
    * @var String
    */
   const SESVAL_sUSERNAME_jenny = 'jenny';

   /**
    * This constant provides ..
    *
    * @id const 20110826°0235
    * @var String
    */
   const SESVAL_sUSERNAME_none = 'none';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0511
    * @var String
    */
   const SVR_KEY_CONTEXT_DOCUMENT_ROOT = 'CONTEXT_DOCUMENT_ROOT';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0513
    * @var String
    */
   const SVR_KEY_DOCUMENT_ROOT = 'DOCUMENT_ROOT';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0515
    * @var String
    */
   const SVR_KEY_HTTP_HOST = 'HTTP_HOST';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0517
    * @var String
    */
   const SVR_KEY_HTTP_REFERER = 'HTTP_REFERER';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id 20110423°0521
    * @todo Stop using this.
    * @note Remember ref 20190201°0123 'undef var php_self'
    * @var String
    */
   const SVR_KEY_PHP_SELF = 'PHP_SELF';

   /**
    * This constant tells the SERVER array key for the request IP address
    *
    * @id const 20190204°0531
    * @var String
    */
   const SVR_KEY_REMOTE_ADDR = 'REMOTE_ADDR';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0523
    * @note Not much used, just for fun
    * @var String
    */
   const SVR_KEY_REQUEST_URI = 'REQUEST_URI';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0525
    * @var String
    */
   const SVR_KEY_SCRIPT_FILENAME = 'SCRIPT_FILENAME';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0527
    * @var String
    */
   const SVR_KEY_SERVER_NAME = 'SERVER_NAME';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0531
    * @var String
    */
   const SVR_KEY_SCRIPT_URI = 'SCRIPT_URI';

   /**
    * This constant tells the SERVER array key for ..
    *
    * @id const 20110423°0533
    * @callers : Seems not used
    * @var String
    */
   const SVR_KEY_SCRIPT_URL = 'SCRIPT_URL';

   /**
    * This constant field provides ..
    *
    * @id const 20110826°0423
    * @note This fields are called 'fieldindex' (as opposed to a 'fieldnumber')
    *        Field here does not mean class field but table field.
    * @var Integer
    */
   const TBL_ACCTS_iFDX_Email = 0;

   /**
    * This constant field provides ..
    *
    * @id const 20110826°0433
    * @var Integer
    */
   const TBL_ACCTS_iFDX_Fullname = 3;

   /**
    * This constant field provides ..
    *
    * @id const 20110826°0427
    * @var Integer
    */
   const TBL_ACCTS_iFDX_Pwhash = 2;

   /**
    * This constant field provides ..
    *
    * @id const 20110826°0425
    * @var Integer
    */
   const TBL_ACCTS_iFDX_Shortname = 1;

   /**
    * This constant field provides ..
    *
    * @id const 20110826°0417
    * @note The fields of this table must be in sync with table definition in class.dbtxt.php
    * @note Prefix 'iFLDNDX' means field index starting with zero,
    *          as opposed to a field number starting with one.
    * @var String
    */
   const TBL_ACCTS_sTABLENAME = "accounts";


   /**
    * This constant tells fieldname Album in table Albums
    *
    * @id const 20110423°0221
    * @callers Not yet in use
    * @var String
    */
   const TBL_ALBMS_sFNA_album = 'album';

   /**
    * This constant tells fieldname Owner in table Albums
    *
    * @id const 20110423°0222
    * @callers Not yet in use
    * @var String
    */
   const TBL_ALBMS_sFNA_owner = 'owner';

   /**
    * This constant tells the name ofn table Albums
    *
    * @id const 20110423°0211
    * @callers Not yet in use
    * @var String
    */
   const TBL_ALBMS_sTBLNAME = 'albums';

   /**
    * This constant tells fieldname Owner in table Owners
    *
    * @id const 20110423°0321
    * @callers Not yet in use
    * @var String
    */
   const TBL_OWNRS_sFNA_owner = 'owner';

   /**
    * This constant tells fieldname Permission in table Owners
    *
    * @id const 20110423°0322
    * @callers Not yet in use
    * @var String
    */
   const TBL_OWNRS_sFNA_permission = 'permission';

   /**
    * This constant tells the name of table Owners
    *
    * @id const 20110423°0311
    * @callers Not yet in use
    * @var String
    */
   const TBL_OWNRS_sTBLNAME = 'owners';

   /**
    * This constant tells fieldname RootOne in table TargetMap
    *
    * @id const 20170925°0522
    * @var String
    */
   const TBL_TGTMAP_sFNA_rootOne = 'rootOne';

   /**
    * This constant tells fieldname RootTwo in table TargetMap
    *
    * @id const 20170925°0523
    * @var String
    */
   const TBL_TGTMAP_sFNA_rootTwo = 'rootTwo';

   /**
    * This constant tells fieldname Trigger in table TargetMap
    *
    * @id const 20170925°0524
    * @var String
    */
   const TBL_TGTMAP_sFNA_trigger = 'trigger';

   /**
    * This constant tells fieldnumber of field RootOne in table TargetMap
    *
    * @id const 20170925°0512
    * @var Integer
    */
   const TBL_TGTMAP_iFNO_rootOne = 1;

   /**
    * This constant tells fieldnumber of field RootTwo in table TargetMap
    *
    * @id const 20170925°0513
    * @var Integer
    */
   const TBL_TGTMAP_iFNO_rootTwo = 3;

   /**
    * This constant tells fieldnumber of field Trigger in table TargetMap
    *
    * @id const 20170925°0514
    * @var Integer
    */
   const TBL_TGTMAP_iFNO_trigger = 2;

   /**
    * This constant tells the name of table TargeMap
    *
    * @id const 20170925°0511
    * @var String
    */
   const TBL_TGTMAP_sTBLNAME = 'targetmap';

   /**
    * This constant provides a double quote character
    *
    * @callers Curiously none at all so far
    * @id 20190128°0355
    * @var String
    */
   const TK_Qt = "\"";

   /**
    * This constant field provides a plain false value
    *
    * @id const 20190406°0211
    * @var Boolean
    */
   const bToggle_FALSE = FALSE;

   /**
    * This constant field provides a plain false value
    *
    * @id const 20190406°0212
    * @var Boolean
    */
   const bToggle_TRUE = TRUE;
}

// eof
