<?php

/**
 * This module builds the text body for the Daftari login page.
 *
 * file      : 20110424°1623
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2024 Norbert C. Maier
 * authors   : Norbert C. Maier
 * status    : Applied
 * encoding  : UTF-8-without-BOM
 * callers   : • Only login.html
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

$sDbg = __DIR__;
$sDir = "G:/work/downtown/daftaridev/trunk/daftari/php/core";          //// Bad workaround — See issue 20220222°0751
$sDir = __DIR__;
include_once($sDir . '/../core/Session.php');                          // Must be first code line in file [line 20121028°1811]
include_once($sDir . '/../core/Globals.php');
include_once($sDir . '/../sitemop/DbTxt.php');

/**
 * This class builds the Login page
 *
 * @id 20190129°0711
 * @callers
 */
class LoginPage
{

   /**
    * This function processes the page post event
    *
    * @id func 20110507°2121
    * @callers ..
    */
   private static function evaluate_post()
   {
      $sCR = Glb::$sTkNL;

      global $sWelcometext;                                            // For unregister = unsubscribe

      // () Read checkboxes [seq 20110507°2201]
      if (! isset($_POST[Glb::POSTKEY_SET_settings_checkboxes]))       // 'settings_checkboxes'
      {
         // Otherwise, errors below if nothing is checked
         $_POST[Glb::POSTKEY_SET_settings_checkboxes] = array();       // 'settings_checkboxes'
      }
      $_SESSION[Glb::SESKEY_bDebug] = in_array ( 'debug'               // 'debug'
                                                , $_POST[Glb::POSTKEY_SET_settings_checkboxes]  // 'settings_checkboxes'
                                                 );
      $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = in_array ( 'more010'
                                                              , $_POST[Glb::POSTKEY_SET_settings_checkboxes]  // 'settings_checkboxes'
                                                               );
      $_SESSION[Glb::SESKEY_bMore060inivarCanary] = in_array ( 'more060inivarCanary'
                                                              , $_POST[Glb::POSTKEY_SET_settings_checkboxes]  // 'settings_checkboxes'
                                                               );
      $_SESSION[Glb::SESKEY_bReadlocaldrive] = in_array ( 'accesslocaldrive'
                                                         , $_POST[Glb::POSTKEY_SET_settings_checkboxes]  // 'settings_checkboxes'
                                                          );
      $_SESSION[Glb::SESKEY_MaintenanceMode] = in_array ( Glb::SESKEY_MaintenanceMode
                                                         , $_POST[Glb::POSTKEY_SET_settings_checkboxes]  // 'settings_checkboxes'
                                                          );

      // () Read other settings [seq 20110507°2203]
      //----------------------------
      // Temporary shutdown 20120912°1701
      if (Glb::bToggle_FALSE)
      {
         $_SESSION[Glb::SESKEY_iAlbumColumns] = $_POST[Glb::POSTKEY_GLR_settings_album_columns]; // 'settings_album_columns'
         $_SESSION[Glb::SESKEY_iAlbumMaxRows] = $_POST[Glb::POSTKEY_GLR_seetings_album_maxrows]; // 'settings_album_maxrows'
         $_SESSION[Glb::SESKEY_sAlbumThumbsize] = $_POST[Glb::SESKEY_sAlbumThumbsize];
         $_SESSION[Glb::SESKEY_iObjectImagesize] = $_POST[Glb::SESKEY_iObjectImagesize]; // Experimental re-use of global constant [const 20110514°1331]
      }
      //----------------------------

      // () Evaluate login button [seq 20110507°2205]
      if (isset($_POST[Glb::POSTKEY_LGN_submit]))                      // 'submit'
      {
         // Basic switch [seq 20120608°1511]
         switch ($_POST[Glb::POSTKEY_LGN_submit])                      // 'submit'
         {
            case "Ausloggen" :
               print ("<p>Ausloggen ...</p>");
               break;
               //exit;                                                 // ???
            default :
               break;
         }

         $sDbg = __DIR__;
         $sDir = "G:/work/downtown/daftaridev/trunk/daftari/php/core"; // Bad workaround — See issue 20220222°0751
         require_once(__DIR__ . './Login.php');

         // () Condition [seq 20110507°2207]
         if (Glb::$Lgn_sMsg_Status_Text == '')
         {
            // () Paranoia
            // This shall never happen, the case already has to be caught by Login.php
            if ($_POST[Glb::POSTKEY_LGN_user] == '')                   // 'user'
            {
               Glb::$Lgn_sMsg_Status_Text = 'Die Felder Name und Kennwort müssen ausgefüllt sein!';
            }
         }
         else
         {
            //--------------------
            // This seems to be the continuation of the settings sequence in Login.php
            // Todo: Merge the two sequences, either here or there [todo 20110508°2121]
            if (Glb::bToggle_FALSE)
            {
               $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_none;
               switch ($_SESSION[Glb::SESKEY_username])
               {
                  case (Glb::SESVAL_sUSERNAME_admin) : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_full  ; break;
                  case (Glb::SESVAL_sUSERNAME_guest) : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_guest ; break;
                  case (Glb::SESVAL_sUSERNAME_jenny) : $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_full  ; break;
                  default : break;
               }
            }
            //--------------------
         }
      }

      // () Process register button [seq 20110507°2211]
      if (isset($_POST[Glb::POSTKEY_LGN_register_button_register]))    // 'register_button_register'
      {
         // () Preparations [seq 20110507°2213]
         $sHash = md5 ( Glb::LGN_sMD5SALTONE
                 . $_POST[Glb::POSTKEY_LGN_register_text_kennwort]     // 'register_text_kennwort'
                  . Glb::LGN_sMD5SALTTWO
                   );
         $aRecord = array( Glb::TBL_ACCTS_iFDX_Email => $_POST[Glb::POSTKEY_LGN_register_text_emailaddress]    // 0 => 'register_text_emailaddress'
                          , Glb::TBL_ACCTS_iFDX_Shortname => $_POST[Glb::POSTKEY_LGN_register_text_username]   // 1 => 'register_text_username'
                           , Glb::TBL_ACCTS_iFDX_Pwhash => $sHash                                              // 2 =>
                            , Glb::TBL_ACCTS_iFDX_Fullname => $_POST[Glb::POSTKEY_LGN_register_text_userfullname]  // 3 => 'register_text_userfullname'
                             );

         // Provide account table (for below)
         $db = new DbTxt(Glb::TBL_ACCTS_sTABLENAME, Glb::$Ses_sDataDirFullname);

         // (Step 1) Validate user input [seq 20110507°2215]
         if ( $aRecord[Glb::TBL_ACCTS_iFDX_Email] == ''
             or $aRecord[Glb::TBL_ACCTS_iFDX_Shortname] == ''
              or $aRecord[Glb::TBL_ACCTS_iFDX_Pwhash] == ''
               or $aRecord[Glb::TBL_ACCTS_iFDX_Fullname] == ''
                )
         {
            // Todo: Actually, only the emailaddress is really needed, the others could be deduced from this
            Glb::$Lgn_sMsg_Register_Text = 'Bitte füllen Sie alle Felder aus!';
         }
         else
         {
            // (Step 2) Validate emailaddress [seq 20110507°2217]
            $aRecByEmailadr = $db->lookupRecord
                             ( Glb::TBL_ACCTS_iFDX_Email
                              , $_POST[Glb::POSTKEY_LGN_register_text_emailaddress]  // 'register_text_emailaddress'
                               );
            if (sizeof($aRecByEmailadr) > 0)
            {
               Glb::$Lgn_sMsg_Register_Text = 'Registrierung nicht möglich,<br>die Email-Adresse <b>' . $aRecByEmailadr[Glb::TBL_ACCTS_iFDX_Email] . '</b> ist bereits registriert.';
               Glb::$Lgn_sMsg_Register_Text .= $sCR . '&nbsp; <input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
            }
            else
            {
               // (Step 3) Validate shortname (= username) [seq 20110507°2221]
               $aRecByShortname = $db->lookupRecord
                                 ( Glb::TBL_ACCTS_iFDX_Shortname
                                  , $_POST[Glb::POSTKEY_LGN_register_text_username]  // 'register_text_username'
                                   );
               if (sizeof($aRecByShortname) > 0)
               {
                  Glb::$Lgn_sMsg_Register_Text = 'Registrierung nicht möglich,<br>der Benutzername <b>' . $aRecByShortname[Glb::TBL_ACCTS_iFDX_Shortname] . '</b> ist bereits registriert.';
                  Glb::$Lgn_sMsg_Register_Text .= $sCR . '&nbsp; <input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
               }
               else
               {
                  // (Step 4) Validate fullname [seq 20110507°2223]
                  $aRecByFullname = $db->lookupRecord
                                   ( Glb::TBL_ACCTS_iFDX_Fullname                      // 3
                                    , $_POST[Glb::POSTKEY_LGN_register_text_userfullname])  // 'register_text_userfullname'
                                     ;
                  if (sizeof($aRecByFullname) > 0)
                  {
                     Glb::$Lgn_sMsg_Register_Text = 'Registrierung nicht möglich,<br>der Benutzername <b>' . $aRecByFullname[Glb::TBL_ACCTS_iFDX_Fullname] . '</b> ist bereits registriert.';
                     Glb::$Lgn_sMsg_Register_Text .= $sCR . '&nbsp; <input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
                  }
                  else
                  {
                     // (Step 5) Finally start the job [seq 20110507°2225]
                     $xRet = $db->addrec($aRecord);

                     if ($xRet === true)
                     {
                        Glb::$Lgn_sMsg_Register_Color = '#00a000';
                        Glb::$Lgn_sMsg_Register_Text = 'Sie wurden erfolgreich registriert:';
                        Glb::$Lgn_sMsg_Register_Text .= '<br>Name = <b>' . $aRecord[Glb::TBL_ACCTS_iFDX_Fullname] . '</b>';
                        Glb::$Lgn_sMsg_Register_Text .= '<br>Username = <b>' . $aRecord[Glb::TBL_ACCTS_iFDX_Shortname] . '</b>';
                        Glb::$Lgn_sMsg_Register_Text .= '<br>Email-Adresse = <b>' . $aRecord[Glb::TBL_ACCTS_iFDX_Email] . '</b>';
                        Glb::$Lgn_sMsg_Register_Text .= '<br>Sie wurden aber nicht automatisch eingeloggt, tun Sie das bitte jetzt selber.';
                     }
                     else
                     {
                        Glb::$Lgn_sMsg_Register_Text = 'Event 20110508°1911';   // 'Fatal' must never happen — todo: Know the situation better
                     }
                     Glb::$Lgn_sMsg_Register_Text .= $sCR . '&nbsp; <input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
                  }
               }
            }
         }
      }

      // () Process changepass button [seq 20110507°2227]
      if (isset($_POST[Glb::POSTKEY_LGN_changepass_button_register]))  // 'changepass_button_register'
      {
         // (Step 1) Validate input [seq 20110507°2231]
         if ($_POST[Glb::POSTKEY_LGN_changepass_text_kennwort1]        // 'changepass_text_kennwort1'
             !== $_POST[Glb::POSTKEY_LGN_changepass_text_kennwort2]    // 'changepass_text_kennwort2'
              )
         {
            Glb::$Lgn_sMsg_ChangePw_Text = 'Die beiden eingegebenen Felder sind nicht gleich.';
            Glb::$Lgn_sMsg_ChangePw_Text .= $sCR . '&nbsp;<input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
         }
         else
         {
            // (Step 2) Validate password [seq 20110507°2233]
            if ($_POST[Glb::POSTKEY_LGN_changepass_text_kennwort1] == '')  // 'changepass_text_kennwort1'
            {
               Glb::$Lgn_sMsg_ChangePw_Text = 'Das Kennwort darf nicht leer sein.';
               Glb::$Lgn_sMsg_ChangePw_Text .= $sCR . '&nbsp;<input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
            }
            else
            {

               // (Step 3) Prepare record [seq 20110507°2235]
               $db = new DbTxt(Glb::TBL_ACCTS_sTABLENAME, Glb::$Ses_sDataDirFullname);
               $aRec = $db->lookupRecord(Glb::TBL_ACCTS_iFDX_Shortname,$_SESSION[Glb::SESKEY_username]);

               // Paranoia
               if (sizeof($aRec) < 1)
               {
                  Glb::$Lgn_sMsg_ChangePw_Text = 'Error 20110508°2221';       // fatal - must theoretically never happen
               }
               else
               {
                  // (Step 3.1) Write record [seq 20110507°2237]
                  $hash = md5(Glb::LGN_sMD5SALTONE
                         . $_POST[Glb::POSTKEY_LGN_changepass_text_kennwort1]  // 'changepass_text_kennwort1'
                          . Glb::LGN_sMD5SALTONE)
                           ;
                  $aRec[Glb::TBL_ACCTS_iFDX_Pwhash] = $hash;
                  $b = $db->writeRecord($aRec);
                  if ($b !== true) {
                     Glb::$Lgn_sMsg_ChangePw_Text = 'Error 20110509°0243';  // fatal
                  }
                  else {
                     //
                     Glb::$Lgn_sMsg_ChangePw_Color = 'green';
                     Glb::$Lgn_sMsg_ChangePw_Text = 'Kennwort wurde geändert.';
                     Glb::$Lgn_sMsg_ChangePw_Text .= $sCR . '&nbsp;<input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
                  }
               }
            }
         }
      }

      // Evaluate unsubscribe button step one [seq 20110507°2241]
      if (isset($_POST[Glb::POSTKEY_LGN_unsubscribe_button_unsubscribe]))  // 'unsubscribe_button_unsubscribe'
      {
         $db = new DbTxt(Glb::TBL_ACCTS_sTABLENAME, Glb::$Ses_sDataDirFullname);
         $aRec = $db->lookupRecord(Glb::TBL_ACCTS_iFDX_Shortname,$_SESSION[Glb::SESKEY_username]);
         if (sizeof($aRec) < 1)
         {
            Glb::$Lgn_sMsg_Unsubscribe_Text = 'Error 20110508°1853';   // Fatal — Can theoretically never happen
         }
         else if ($aRec[Glb::TBL_ACCTS_iFDX_Shortname] == Glb::SESVAL_sUSERNAME_guest)  // 'guest'
         {
            Glb::$Lgn_sMsg_Unsubscribe_Text = 'Das Gast-Konto können Sie nicht kündigen.';
            Glb::$Lgn_sMsg_Unsubscribe_Text .= $sCR . '&nbsp;<input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
         }
         else
         {
            Glb::$Lgn_sMsg_Unsubscribe_Color = 'green';
            Glb::$Lgn_sMsg_Unsubscribe_Text = 'Möchten Sie wirklich Ihr Konto löschen?';
            Glb::$Lgn_sMsg_Unsubscribe_Text .= '<br>(Email = <b>' . $aRec[Glb::TBL_ACCTS_iFDX_Email] . '</b>';
            Glb::$Lgn_sMsg_Unsubscribe_Text .= ', Name = <b>' . $aRec[Glb::TBL_ACCTS_iFDX_Fullname] . '</b>';
            Glb::$Lgn_sMsg_Unsubscribe_Text .= ', Username = <b>' . $aRec[Glb::TBL_ACCTS_iFDX_Shortname] . '</b>)';
            Glb::$Lgn_sMsg_Unsubscribe_Text .= $sCR . '<br><input type="submit" name="unsubscribe_button_really_unsubscribe" value="Ja, wirklich löschen">';
            Glb::$Lgn_sMsg_Unsubscribe_Text .= $sCR . '&nbsp;&nbsp;&nbsp;<input type="submit" name="generic_button_ok_NOT_EVALUATED" value="Abbruch">';
         }
      }

      // Evaluate unsubscribe button step two [seq 20110507°2243]
      if (isset($_POST[Glb::POSTKEY_LGN_unsubscribe_button_really_unsubscribe]))  // 'unsubscribe_button_really_unsubscribe'
      {
         $db = new DbTxt(Glb::TBL_ACCTS_sTABLENAME, Glb::$Ses_sDataDirFullname);
         $aRec = $db->lookupRecord(Glb::TBL_ACCTS_iFDX_Shortname,$_SESSION[Glb::SESKEY_username]);
         if (sizeof($aRec) > 0)
         {
            // () [seq 20110507°2245]
            $b = $db->deleteRecord($aRec[Glb::TBL_ACCTS_iFDX_Email]);
            if ($b === true)
            {
               //-----------------
               // Logout (compare the unused libs/login_php_v1.3/logout.php)
               unset($_SESSION[Glb::SESKEY_login]);
               Glb::$Ses_bIsLoggedin = false;
               //-----------------

               $sWelcometext = '<span style="color:Green;">Hallo <b>' . $aRec[Glb::TBL_ACCTS_iFDX_Fullname] . '</b> (<b>' . $aRec[Glb::TBL_ACCTS_iFDX_Shortname] . '</b>),';
               $sWelcometext .= '<br>Sie haben Ihr Konto ' . $aRec[Glb::TBL_ACCTS_iFDX_Email] . ' gelöscht.';
               $sWelcometext .= '<br>Sie sind nicht mehr eingeloggt.</span>';

               //--------------------------
               // [seq 20110508°2123`02]                               //// [Marker 20210526°1011 Merge sequences]
               // Todo : Sequence exists three times — Merge them

               // Issue 20161008°0851 When terminating the account, this two
               //  variables seem no more available (see screenshot 20161008°0852)
               //  So empirically just shutdown the sequence.
               if (Glb::bToggle_FALSE) {
                  $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_none;
                  $_SESSION[Glb::SESKEY_bDebug] = false;
               }
               //--------------------------
            }
            else
            {
               Glb::$Lgn_sMsg_Unsubscribe_Text .= 'Event 20110508°1851 passed.'; // FATAL
            }
         }
      }

      // Evaluate passwordforgotten button [seq 20110507°2247]
      if (isset($_POST[Glb::POSTKEY_LGN_forgotpass_button_forgotpass]))
      {
         Glb::$Lgn_sMsg_PwForgotten_Text = 'The password-forgotten function is not yet available';
         Glb::$Lgn_sMsg_PwForgotten_Text .= $sCR . '&nbsp;<input type="submit" name="generic_button_ok_NOT_EVALUATED" value="OK">';
      }

      // Evaluate passwordforgotten button [seq 20110507°2251]
      if (isset($_POST[Glb::POSTKEY_LGN_login_button_enter]))
      {
         // Jobs are already done high above at beginning of evaluate_post()
         //  .. except e.g. actualizing Glb::$Ses_sLocalhostInfo?
      }
   }

   /**
    * This static method runs the Login page main procedure
    *
    * @id 20190129°0721
    * @callers This script level only
    */
   public static function runLoginPage()
   {

      $sCR = Glb::$sTkNL;

      self::$sSesLocalhostInfo = Glb::$Ses_sLocalhostInfo;

      // Build the Moccasin ScriptInfo [seq 20160611°0151]
      $sScriptInfo = '';
      $sScriptInfo .= $sCR . '<pre style="background-color:Moccasin; font-size:small;">'; // Formerly yellow 20181228°1022
      $sScriptInfo .= $sCR . '*********** Moccasin ScriptInfo  ************';
      if ( Glb::$Lgn_bHostIsLocalhost === true ) {
         $sScriptInfo .= $sCR . " Glb::\$Ses_sDataDirFullname   = " . Glb::$Ses_sDataDirFullname; // E.g. X:\workspaces\daftaridev\trunk\daftari.DATA
         $sScriptInfo .= $sCR . " Glb::\$Cfg_sInifileFullname   = " . Glb::$Cfg_sInifileFullname; // E.g. \daftari.ini
         $sScriptInfo .= $sCR . '*********************************************';
      } else {
         $sScriptInfo .= $sCR . 'This is the Moccasin debug box.';
      }
      $sScriptInfo .= $sCR . '</pre>';

      Glb::$Ses_bIsLoggedin = false;                                   // Is initialized as false anyway

      if (isset($_SESSION[Glb::SESKEY_login]))
      {
         // Why does a login appear empty here, if it was unset in outlog.php?
         // (Workaround 20120916°1711) Unset such login.
         // [todo 20120916°1712] And there is a entry $_SESSION[''] which should be cleaned up.
         if ($_SESSION[Glb::SESKEY_login] == '')
         {
            unset($_SESSION[Glb::SESKEY_login]);
         }
         else
         {
            Glb::$Ses_bIsLoggedin = true;                              // To be eliminated?
         }
      }

      // Provide welcometext [seq 20110506°0044]
      if ( isset($_SESSION[Glb::SESKEY_login]) )                       // 'login'
      {
         $sWelcometext = ''
                        . 'Hallo <b>' . $_SESSION[Glb::SESKEY_username] . '</b>'
                         . ' (<b>' . $_SESSION[Glb::SESKEY_userfullname] . '</b>)'
                          . ', you are logged-in.'
                           ;
      }
      else
      {
         $sWelcometext = 'Hello <b>Guest</b>, you are not logged in.';

         // Clean values for guests [seq 20110508°2123`03] // [Marker 20210526°1011 Merge sequences]
         // Todo : This sequence exists three times. Merge those places!
         $_SESSION[Glb::SESKEY_privileges] = Glb::SESVAL_PRIVIL_none;
         $_SESSION[Glb::SESKEY_bDebug] = false;
         $_SESSION[Glb::SESKEY_bMore010_sessionInfo] = false;
         $_SESSION[Glb::SESKEY_bMore020] = false;
         $_SESSION[Glb::SESKEY_bMore030] = false;
         $_SESSION[Glb::SESKEY_bMore040] = false;
         $_SESSION[Glb::SESKEY_bMore060inivarCanary] = false;
         $_SESSION[Glb::SESKEY_username] = '';
         $_SESSION[Glb::SESKEY_userfullname] = '';
         //--------------------------
      }

      // Initialize textbricks
      Glb::$Lgn_sMsg_Status_Text = '';
      Glb::$Lgn_sMsg_Status_Color = 'red';
      Glb::$Lgn_sMsg_Register_Text = '';
      Glb::$Lgn_sMsg_Register_Color = 'red';
      Glb::$Lgn_sMsg_ChangePw_Text = '';
      Glb::$Lgn_sMsg_ChangePw_Color = 'red';
      Glb::$Lgn_sMsg_Unsubscribe_Text = '';
      Glb::$Lgn_sMsg_Unsubscribe_Color = "red";
      Glb::$Lgn_sMsg_PwForgotten_Text = '';

      // React on all kind of form posting
      if (isset($_POST[Glb::POSTKEY_LGN_login_form_was_sent]))         // 'login_form_was_sent'
      {
         self::evaluate_post();
      }

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Start html generation
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      $sOut = '';

      // Introductory text
      $sOut = $sOut
         . $sCR . '<div class="LoginFormPanelWhite" id="id20111227o2112">'
         . $sCR . '<form action="" method="post" style="text-align:left;">'
         . $sCR . '<input type="hidden" name="login_form_was_sent" value="yes">'
         // Try paragraph before table to extract welcome text from there to the h1 line
         . $sCR . '<p style="margin-top:0.1em; margin-bottom:0.1em; margin-left:3.7em;">'
         . $sCR . '<big>' . $sWelcometext . '</big> &nbsp;'
          ;
      if (Glb::$Ses_bIsLoggedin)
      {
         $sOut .= $sCR . '                <a href="./../php/core/Logout.php">Logout</a>';
      }
      $sOut = $sOut
         . $sCR . '</p>'
         . $sCR . '</div><!-- id="id20111227o2112" -->'
         . $sCR . '<p class="Subtitle">Login</p>'
         . $sCR . '<div class="LoginFormPanelWhite" id="id20111227o2113">'
         . $sCR . ' <table border="0" width="98%">'
          ;

      // Login section
      if (Glb::$Ses_bIsLoggedin)
      {
         $s = "Logged in";
      }
      else
      {
         $s = "Not logged in";
      }

      // New no differnt layout for each loggin-flavour but one for all [seq 20111228°1531]
      if (Glb::bToggle_TRUE)  // if (Glb::$Ses_bIsLoggedin)
      {
         $sUsername = '';                                              // 'guest'
         $sOut = $sOut
            . $sCR . '  <tr>'
            . $sCR . '   <td rowspan="2" style="padding-left:1.7em;">'
            . $sCR . '                Input your credentials&nbsp;:'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-bottom:0em;text-align:right;">'
            . $sCR . '                Username :' // shortname
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-bottom:0em;">'
            . $sCR . '             <input type=text name="user" value="' . $sUsername . '">' // For Login.php
            . $sCR . '                <!-- select>'
            . $sCR . '                 <option value="admin">Admin</option>'
            . $sCR . '                 <option value="gast" selected="selected">Gast</option>'
            . $sCR . '                 <option value="guest">Guest</option>'
            . $sCR . '                 <option value="member">Member</option>'
            . $sCR . '                 <option value="nobody">Nobody</option>'
            . $sCR . '                 <option value="somebody">Somebody</option>'
            . $sCR . '                </select -->'
            . $sCR . '   </td>'
            . $sCR . '   <td style="color:silver;">'
            . $sCR . '                &nbsp;E.g. gast'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
            . $sCR . '  <tr>'
            . $sCR . '   <td style="padding-top:0em;text-align:right;">'
            . $sCR . '                Password :'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em;">'
            . $sCR . '                <input type=password name="pass" value="">'
            . $sCR . '   </td>'
            . $sCR . '   <td style="text-align:right;">'
            . $sCR . '                <input type="submit" name="submit" value="Einloggen">'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;
         if (Glb::$Lgn_sMsg_Status_Text !== '')
         {
            $sOut = $sOut
               . $sCR . '  <tr>'
               . $sCR . '   <td>'
               . $sCR . '            Nachricht Registercolor :'
               . $sCR . '   </td>'
               . $sCR . '   <td colspan="2">'
               . $sCR . '            <p><span style="color:' . Glb::$Lgn_sMsg_Register_Color . ';">' . Glb::$Lgn_sMsg_Status_Text . '</span>'
               . $sCR . '   </td>'
               . $sCR . '  </tr>'
                ;
         }
      }

      // Section delimiter
      $sOut = $sOut
         . $sCR . ' </table>'
         . $sCR . ' </div><!-- id="id20111227o2113" -->'
         . $sCR . ' <p class="Subtitle">Register</p>' // Registrieren
         . $sCR . ' <div class="LoginFormPanelWhite" id="id20111227o2114">'
         . $sCR . ' <table border="0" width="98%">'
          ;

      // Here $sOut seems to break register section [debug 20180303°1121]
      if (Glb::bToggle_TRUE)  // if (! Glb::$Ses_bIsLoggedin)
      {
         $sOut = $sOut
           . $sCR . '  <tr>'
           . $sCR . '   <td rowspan="4" style="padding-left:1.7em;">'
           . $sCR . '                 Input your data&nbsp;:'
           . $sCR . '                  <p class="Notez">'
           . $sCR . '                   Note: The username must not contain blanks.'
           . $sCR . '                  </p>'
           . $sCR . '   </td>'
           . $sCR . '   <td style="padding-bottom:0em; text-align:right">'
           . $sCR . '                Email addresse&nbsp;:'
           . $sCR . '   </td>'
           . $sCR . '   <td style="padding-bottom:0em;">'
            ;

         $sYourEmailaddress = '';                                      // E.g. 'me@me.lum';
         $sYourUsername = '';                                          // E.g. 'me';
         $sYourUserfullname = '';                                      // E.g. 'Me Lummerland';

         $sOut = $sOut
            . $sCR . '                 <input type="text" name="register_text_emailaddress" value="' . $sYourEmailaddress . '">'
            . $sCR . '   </td>'
            . $sCR . '   <td style="color:silver">'
            . $sCR . '                 &nbsp;E.g.&nbsp;me@me.lum'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'

            . $sCR . '  <tr>'
            . $sCR . '   <td style="padding-top:0em; padding-bottom:0em; text-align:right">'
            . $sCR . '                 Username&nbsp;:'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em; padding-bottom:0em;">'
            . $sCR . '                 <input type="text" name="register_text_username" value="' . $sYourUsername . '">'
            . $sCR . '   </td>'
            . $sCR . '   <td style="color:silver">'
            . $sCR . '                 &nbsp;E.g.&nbsp;me'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'

            . $sCR . '  <tr>'
            . $sCR . '   <td style="padding-top:0em; padding-bottom:0em; text-align:right">'
            . $sCR . '                 Voller Name&nbsp;:'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em; padding-bottom:0em;">'
            . $sCR . '                 <input type="text" name="register_text_userfullname" value="' . $sYourUserfullname . '">'
            . $sCR . '   </td>'
            . $sCR . '   <td style="color:silver">'
            . $sCR . '                 &nbsp;E.g.&nbsp;Me&nbsp;Lum'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'

            . $sCR . '  <tr>'
            . $sCR . '   <td style="padding-top:0em; text-align:right">'
            . $sCR . '                 Kennwort&nbsp;:'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em;">'
            . $sCR . '                 <input type="password" name="register_text_kennwort" value="">'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'

            . $sCR . '  <tr>'
            . $sCR . '   <td>'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em; text-align:right">'
            . $sCR . '                 <i>Kennwort&nbsp;again&nbsp;</i>:'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em;">'
            . $sCR . '                 <input type="password" name="register_text_kennwort_2" value="">'
            . $sCR . '   </td>'
            . $sCR . '   <td style="text-align:right;">'
            . $sCR . '                 <input type="submit" name="register_button_register" value="Registrieren">'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;

         if (Glb::$Lgn_sMsg_Register_Text !== '')
         {
            $sOut = $sOut
               . $sCR . '  <tr>'
               . $sCR . '   <td colspan="3">'
               . $sCR . '            <p><span style="color:' . Glb::$Lgn_sMsg_Register_Color . ';">' . Glb::$Lgn_sMsg_Register_Text . '</span></p>'
               . $sCR . '   </td>'
               . $sCR . '  </tr>'
                ;
         }

      }

      // Here $sOut seems broken [debug 20180303°1121]
      // Section delimiter
      $sOut = $sOut
         . $sCR . ' </table>'
         . $sCR . ' </div><!-- id="id20111227o2114" -->'
         . $sCR . ' <p class="Subtitle">Manage account</p>'
         . $sCR . ' <div class="LoginFormPanelWhite" id="id20111227o2115">'
         . $sCR . ' <table border="0" width="98%">'
          ;

      // Change password
      $sSkUsername = $_SESSION[Glb::SESKEY_username] ?? ''; // [line 20190312°0111`01] avoid PhanTypeArraySuspiciousNullable
      if ( (Glb::$Ses_bIsLoggedin) && ($sSkUsername !== Glb::SESVAL_sUSERNAME_guest))
      {
         $sOut = $sOut
            . $sCR . '  <tr>'
            . $sCR . '   <td rowspan="2">'
            . $sCR . '                Kennwort ändern'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-bottom:0em; text-align:right">'
            . $sCR . '                Neues&nbsp;Kennwort&nbsp;:'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-bottom:0em;">'
            . $sCR . '                <input type=password name="changepass_text_kennwort1" value="">'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
            . $sCR . '  <tr>'
            . $sCR . '  <td style="padding-top:0em; text-align:right">'
            . $sCR . '                Wiederholung :'
            . $sCR . '   </td>'
            . $sCR . '   <td style="padding-top:0em;">'
            . $sCR . '                <input type=password name="changepass_text_kennwort2" value="">'
            . $sCR . '                <input type="submit" name="changepass_button_register" value="Kennwort ändern">'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;
         if (Glb::$Lgn_sMsg_ChangePw_Text !== '')
         {
            $sOut = $sOut
               . $sCR . '  <tr>'
               . $sCR . '   <td colspan="3">'
               . $sCR . '            <p><span style="color:' . Glb::$Lgn_sMsg_ChangePw_Color . ';">' . Glb::$Lgn_sMsg_ChangePw_Text . '</span></p>'
               . $sCR . '   </td>'
               . $sCR . '  </tr>'
                ;
         }
         $sOut = $sOut
            . $sCR . '  <tr>'
            . $sCR . '   <td colspan="3">'
            . $sCR . '                <p><hr style="border:solid #88ff88 1px;" /></p>'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;
      }

      // Unregister means unsubscribe section
      $sSkUsername = $_SESSION[Glb::SESKEY_username] ?? '';            // [line 20190312°0111`02] avoid PhanTypeArraySuspiciousNullable
      if ( (Glb::$Ses_bIsLoggedin) && ($sSkUsername !== Glb::SESVAL_sUSERNAME_guest))
      {
         $sOut = $sOut
            . $sCR . '  <tr>'
            . $sCR . '   <td rowspan="1">'
            . $sCR . '                Benutzerkonto kündigen und löschen'
            . $sCR . '   </td>'
            . $sCR . '   <td>'
            . $sCR . '   </td>'
            . $sCR . '   <td>'
            . $sCR . '                <input type="submit" name="unsubscribe_button_unsubscribe" value="Kündigen">'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;
         if (Glb::$Lgn_sMsg_Unsubscribe_Text !== '')
         {
            $sOut = $sOut
               . $sCR . '  <tr>'
               . $sCR . '   <td colspan="3">'
               . $sCR . '           <p>'
               . $sCR . '            <span style="color:' . Glb::$Lgn_sMsg_Unsubscribe_Color . ';">' . Glb::$Lgn_sMsg_Unsubscribe_Text . '</span>'
               . $sCR . '           </p>'
               . $sCR . '   </td>'
               . $sCR . '  </tr>'
                ;
         }
         $sOut = $sOut
            . $sCR . '  <tr>'
            . $sCR . '   <td colspan="3">'
            . $sCR . '                <p><hr style="border:solid #88ff88 1px;" /></p>'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;
      }

      // Password forgotten section
      $sOut = $sOut
         . $sCR . '  <tr>'
         . $sCR . '   <td style="color:LightSlateGray; padding-left:1.7em; padding-right:1.1em;">'
         . $sCR . '                Password forgotten? Order a new one by email.'
         . $sCR . '                <span style="font-style:italic;">(Function not yet available.)</span>'
         . $sCR . '   </td>'
         . $sCR . '   <td style="color:LightSlateGray; text-align:right;">'
         . $sCR . '               <tt><i>Your&nbsp;email&nbsp;address</i></tt>&nbsp;:&nbsp;'
         . $sCR . '   </td>'
         . $sCR . '   <td>'
         . $sCR . '               <input type=text name="forgotpass_text_emailaddress" value="">'
         . $sCR . '               <input type="submit" name="forgotpass_button_forgotpass" value="Get new password">'
         . $sCR . '   </td>'
         . $sCR . '  </tr>'
          ;

      if (Glb::$Lgn_sMsg_PwForgotten_Text !== '')
      {
         $sOut = $sOut
            . $sCR . '  <tr>'
            . $sCR . '   <td colspan="3">'
            . $sCR . '               <span style="color:' . Glb::$Lgn_sMsg_Register_Color . ';">' . Glb::$Lgn_sMsg_PwForgotten_Text . '</span>'
            . $sCR . '   </td>'
            . $sCR . '  </tr>'
             ;
      }
      $sOut = $sOut
         . $sCR . '  <tr>'
         . $sCR . '   <td colspan="3">'
         . $sCR . '                <hr style="border:solid #88ff88 1px;" />'
         . $sCR . '   </td>'
         . $sCR . '  </tr>'
          ;

      // Close page section delimiter
      $sOut = $sOut
         . $sCR . ' </table>'
         . $sCR . ' </div><!-- id="id20111227o2115" -->'
         . $sCR . '</div><!-- id="id20111227o2112" -->'                // [line 20161008°0911`02]
         . $sCR
          ;

      self::$sOtputScriptInfo = $sScriptInfo;
      self::$sOtputMainFrag = $sOut;
   }

   /**
    * This static field collects the built wanted HTML fragment to be output
    *
    * @id 20190129°0731
    * @var String
    */
   public static $sOtputMainFrag = '';

   /**
    * This static field provides the wanted ScriptInfo HTML fragment
    *
    * @todo implement some debug switch to switch the box on and off.
    *      Possibly use an already existing one [todo 20181228°1011]
    * @id 20190129°0733
    * @var String
    */
   public static $sOtputScriptInfo = '';

   /**
    * This static field is a workaround to pass on the value from Globals
    *    to the page. The HTML does not find class Glb. Why not?
    *
    * @id 20190129°0613
    * @var String
    */
   public static $sSesLocalhostInfo = '';

}

LoginPage::runLoginPage();

/* eof */
