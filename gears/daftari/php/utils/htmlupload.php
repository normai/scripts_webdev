<?php

/**
 * This module provides a simple HTML file upload
 *
 * id         : file 20110525°1311
 * license    : GNU AGPL v3
 * copyright  : © 2011 - 2024 Norbert C. Maier
 * authors    : ncm
 * status     : proof-of-concept
 * encoding   : UTF-8-without-BOM
 * reference  : Joerg Krause, 'Kochbuch PHP5' page 138/~225 [ref 20101220°2021, ref 20110525°1141]
 * callers    :
 */

/*
   todo 20120828°1742 eliminate hardcoded target path
*/

use Trekta\Daftari\Globals as Glb;

?>

<p class="PanelSectionTitle" id="id20201128o0232">
   File upload
</p>

<form action="<?php echo($_SERVER[Glb::SVR_KEY_PHP_SELF]) ?>" method="post" id="id20201128o0233">
<p id="id20201128o0234">
   <select name="numfiles" size="1" id="id20201128o0235">
      <option value="1">1 Datei
      <option value="2">2 Dateien
      <option value="3">3 Dateien
      <option value="5">5 Dateien
      <option value="10">10 Dateien
   </select>
   <input type="Submit" value="Anzahl festlegen" name="regnum" id="id20201128o0236">
</p>
</form>

<form enctype="multipart/form-data" method="post" action="<?php echo($_SERVER[Glb::SVR_KEY_PHP_SELF]) ?>" id="id20201128o0237">
<?php

   $sCR = Glb::$sTkNL;

   $sTargetfolder = './../../daftari.DATA/uploads';                    // See 20120828°1742 eliminate hardcoded target path
   $sTargetfolder = realpath($sTargetfolder);

   if (isset($_POST[Glb::POSTKEY_TOL_UPL_regnum]))                     // 'regnum'
   {
      $numfiles = isset($_POST[Glb::POSTKEY_TOL_UPL_numfiles]) ? $_POST[Glb::POSTKEY_TOL_UPL_numfiles] : ''; // 'numfiles'
      echo '<p id="id20201128o0242">';
      echo $sCR . '   <b>' . $numfiles . '</b>';
      echo $numfiles == '1' ? ' Datei kann' : ' Dateien können';
      echo ' hochgeladen werden:';
      echo $sCR . '</p>';
      echo $sCR . '<p id="id20201128o0243">';
      for ($i = 1; $i <= $numfiles; $i++)
      {
         if ($i > 1)
         {
            echo $sCR . '   <br />';
         }
         echo '   <input type="File" name="myfile' . $i . '" size="56" id="id20201128o0244">';
      }
      echo $sCR . '   <br /><input type="Submit" name="sendfiles" value="Dateien senden">';
      echo $sCR . '</p>' . $sCR;
   }
   if (isset($_POST[GLB::POSTKEY_TOL_UPL_sendfiles]))                  // 'sendfiles'
   {
      $numsendfiles = count($_FILES);
      echo $sCR . '<p id="id20201128o0245">';
      echo $sCR . '   <b>' . $numsendfiles . '</b>';
      echo             $numsendfiles == 1 ? ' Datei wurde gesendet.' : ' Dateien wurden gesendet.';
      echo $sCR . '</p>';
      foreach($_FILES as $strFieldName => $arrPostFiles)
      {
         if ($arrPostFiles['size'] > 0)
         {
            $strFileName = $arrPostFiles['name'];                      // e.g. 'csved.bat'
            $intFileSize = $arrPostFiles['size'];                      // e.g. 616
            $strFileMIME = $arrPostFiles['type'];                      // e.g. 'application/octet-stream'
            $strFileTemp = $arrPostFiles['tmp_name'];                  // e.g.'C:\WINDOWS\Temp\php11D.tmp'
            $b = @move_uploaded_file ($strFileTemp, $sTargetfolder . '/' . $strFileName);
            if ($b === true)
            {
               echo $sCR . '<p id="id20201128o0246">';
               echo $sCR . '   Datei <b>' . $strFileName . '</b> wurde erfolgreich hochgeladen:' . $sCR . '</p>';
               echo $sCR . '</p>';
               echo $sCR . '<ul id="id20201128o0247">';
               echo $sCR . ' <li id="id20201128o0252">Größe: ' . $intFileSize . ' Bytes</li>';
               echo $sCR . ' <li id="id20201128o0253">MIME: ' . $strFileMIME . '</li>';
               echo $sCR . '</ul>';
               echo $sCR;
            }
            else
            {
               echo $sCR . '<p id="id20201128o0254">';
               echo $sCR . '   Something went wrong with file <b>' . $strFileName . '</b>';
               //echo $sCR . '<br> (fro ' . realpath($strFileTemp) . ')'; // don't show this the user
               //echo $sCR . '<br> (to ' . $sTargetfolder . ')'; // don't show this the user
               echo $sCR . '</p>' . $sCR;
            }
         }
      }
   }
?>
</form>

</body>
</html>
