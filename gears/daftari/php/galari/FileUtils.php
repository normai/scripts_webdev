<?php

/**
 * This module stores some functions .. which are not used yet ..
 *
 * file      : 20110518°1921
 * license   : GNU AGPL v3
 * copyright : © 2011 - 2024 Norbert C. Maier
 * authors   : ncm
 * status    :
 * encoding  : UTF-8-without-BOM
 * functions : In this file
 *              • preg_split_TEST()
 *              • checkRemoteFile()
 *              • getjpegsize()
 *              • getimagesize_remote()
 * callers   : None so far
 */

namespace Trekta\Daftari;

//use Trekta\Daftari\Globals as Glb;

/**
 * This class stores just some peripheral code ..
 *
 * @id class 20190202°0911
 */
class FileUtils
{

   /**
    * This function consitutes a curl solution to check for an image to
    *   exist before calling getimagesize()
    *
    * id 20110516°2141
    * status Not tested at all, just put here for possible later use
    * ref Function after
    *      http://php.net/manual/de/function.getimagesize.php [ref 20110516°2131]
    *      posted by mycrazydream at 24-Jul-2009 04:00 with the remark:
    *      "I had for quite some time been using getimagesize() to check for the existence
    *      of a remote image. This turned out to take way too long. The following
    *      curl solution only checks the http headers so it is much more efficient."
    * callers None yet
    * @param $url {} ..
    * @return bool Success flag
    */
   public static function checkRemoteFile($url)
   {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$url);
      // Do not download content
      curl_setopt($ch, CURLOPT_NOBODY, 1);
      curl_setopt($ch, CURLOPT_FAILONERROR, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      if (curl_exec($ch) !== FALSE)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   /**
    * This function gets the image dimensions by streaming the file until the information
    *  is to be read, not downloading the complete image as getimagesize() does.
    *
    * @id 20110516°2143
    * @status NOT TESTED AT ALL, JUST PUT HERE FOR EVENTUAL LATER USE
    * @ref Function after
    *     http://php.net/manual/de/function.getimagesize.php [ref 20110516°2131]
    *     posted by djwishbone at hotmail dot com 19-Nov-2003 02:31 with the
    *     remark: Using remote files with getimagesize($URL) never worked for
    *     me. Except when I would grab files from the same server.  However,
    *     I developed some code with the help from the people here that does
    *     work.  If you are having problems give this function a shot.
    * @callers None yet
    * @param $image_url {} ..
    * @return {Array|Boolean} The wanted information or FALSE
    */
   public static function getimagesize_remote($image_url)
   {
      $handle = fopen ($image_url, "rb");
      $contents = "";
      if ($handle) {
        do {
           $data = fread($handle, 8192);
           if (strlen($data) == 0) {
              break;
           }
           $contents .= $data;
        }
        while(Glb::bToggle_TRUE);
      }
      else {
         return FALSE;
      }
      fclose ($handle);

      $im = ImageCreateFromString($contents);
      if (! $im) {
         return FALSE;
      }
      $gis = Array();
      $gis[0] = ImageSX($im);
      $gis[1] = ImageSY($im);
      // Array member 3 is used below to keep with current getimagesize standards
      $gis[3] = "width={$gis[0]} height={$gis[1]}";
      ImageDestroy($im);

      return $gis;
   }

   /**
    * This retrieves JPEG width and height without downloading/reading entire image
    *
    * @id 20110516°2142
    * @status Not tested at all, just put here for possible later use
    * @ref Function after
    *     http://php.net/manual/de/function.getimagesize.php [ref 20110516°2131]
    *     posted by james dot relyea at zifiniti dot com 08-Feb-2009 04:49
    *     with the remark: "As noted below, getimagesize will download the entire
    *     image before it checks for the requested information. This is extremely
    *     slow on large images that are accessed remotely. Since the width/height
    *     is in the first few bytes of the file, there is no need to download the
    *     entire file. I wrote a function to get the size of a JPEG by streaming
    *     bytes until the proper data is found to report the width and height"
    * @callers None yet
    * @param $img_loc {String} The URL of the image to analyse
    * @return {Array|Boolean} The wanted array or false .. (?)
    */
   public static function getjpegsize($img_loc)
   {
      // Retrieve JPEG width and height without downloading/reading entire image.
      $handle = fopen($img_loc, "rb") or die("Invalid file stream.");
      $new_block = NULL;
      if (! feof($handle))
      {
         $new_block = fread($handle, 32);
         $i = 0;
         if ( $new_block[$i] == "\xFF" && $new_block[$i+1] == "\xD8" && $new_block[$i+2] == "\xFF" && $new_block[$i+3] == "\xE0" )
         {
            $i += 4;
            if ( $new_block[$i+2] == "\x4A" && $new_block[$i+3] == "\x46" && $new_block[$i+4] == "\x49" && $new_block[$i+5] == "\x46" && $new_block[$i+6] == "\x00" )
            {
               // Read block size and skip ahead to begin cycling through blocks in search of SOF marker
               $block_size = unpack("H*", $new_block[$i] . $new_block[$i+1]);
               $block_size = hexdec($block_size[1]);
               while(! feof($handle))
               {
                  $i += $block_size;
                  $new_block .= fread($handle, $block_size);
                  if ( $new_block[$i]=="\xFF" )
                  {
                     // New block detected, check for SOF marker
                     $sof_marker = array("\xC0", "\xC1", "\xC2", "\xC3", "\xC5", "\xC6", "\xC7", "\xC8", "\xC9", "\xCA", "\xCB", "\xCD", "\xCE", "\xCF");
                     if ( in_array($new_block[$i+1], $sof_marker) )
                     {
                        // SOF marker detected. Width and height information is contained in bytes 4-7 after this byte.
                         $size_data = $new_block[$i+2] . $new_block[$i+3] . $new_block[$i+4] . $new_block[$i+5] . $new_block[$i+6] . $new_block[$i+7] . $new_block[$i+8];
                         $unpacked = unpack("H*", $size_data);
                         $unpacked = $unpacked[1];
                         $height = hexdec($unpacked[6] . $unpacked[7] . $unpacked[8] . $unpacked[9]);
                         $width = hexdec($unpacked[10] . $unpacked[11] . $unpacked[12] . $unpacked[13]);
                         return array($width, $height);
                     }
                     else
                     {
                         // Skip block marker and read block size
                         $i += 2;
                         $block_size = unpack("H*", $new_block[$i] . $new_block[$i+1]);
                         $block_size = hexdec($block_size[1]);
                     }
                  }
                  else
                  {
                      return FALSE;
                  }
               }
            }
         }
      }
      return FALSE;
   }

   /**
    * This function demonstrates some preg_split variations .. (?)
    *
    * @id 20110518°2021
    * @status Needs be re-tested
    * @note 20110515°2121 ''
    *     With this lines we started writing function 20110502°1324 parse_filename.
    *     Now we ripped those lines out and placed them in a dedicated function
    *     here. For the next use, it probably will have to be re-wired correctly.
    * @callers None yet?
    */
   public static function preg_split_TEST()
   {
      // Just inserted, not used below so far [seq 20110519°1241]
      $aNams = array();
      $aNams[] = "_20060929-1141_SandiThom-IWishIWasAPunkRocker_.mp3";         // oldstyle
      $aNams[] = "#20060929'1141.Sandi Thom - I Wish I Was A Punk Rocker.mp3"; // newstyle
      $aNams[] = "#20060929.1141 Sandi Thom - I Wish I Was A Punk Rocker.mp3"; // newstyle-lazy
      $aNams[] = "#20060929.1141 Sandi Thom - I Wish I Was A Punk Rocker .mp3"; // newstyle-lazy-trailblank
      $aNams[] = "#20060929'1141.Sandi Thom - I Wish ...mp3";             // newstyle (corename with dots)
      $aNams[] = "#20060929.1141 Sandi Thom - I Wish ...mp3";             // newstyle-lazy (corename with dots)
      $aNams[] = "#20060929.1141 Sandi Thom - I Wish .. .mp3";            // newstyle-lazy-trailblank (corename with dots)

      $aNams[] = "_20110428-1221_holla_.txt";
      $aNams[] = "_20110428-1221_holla_.txt.TEST.txt";
      $aNams[] = "_20110428-1221_holla_p99q66_.jpg";
      $aNams[] = "_20110428-1221_holla_.txt";
      $aNams[] = "_20110428-1221_holla_.p99q66.jpg";
      $aNams[] = "#20110428'1221.holla.txt";
      $aNams[] = "#20110428'1221.holla.p99q66_.jpg";
      $aNams[] = "#20110428'1221.holla.txt";
      $aNams[] = "#20110428'1221.holla.p99q66_.jpg";

      // Flavour set [seq 20110516°0141]
      // Todo 20110516°0143 : Adjust and integrate this into a filenameparser test suite.
      $fla = Array();
      $fla[] = array( '_20110428-1221_holla_.txt'              , array() );
      $fla[] = array( '_20110428-1221_holla_.txt.TEST.txt'     , array() );
      $fla[] = array( '_20110428-1221_holla_p99q66_.jpg'       , array() );
      $fla[] = array( '_20110428-1221_holla_.txt'              , array() );
      $fla[] = array( '_20110428-1221_holla_.p99q66.jpg'       , array() );
      $fla[] = array( '#20110428\'1221.holla.txt'              , array() );
      $fla[] = array( '#20110428\'1221.holla.p99q66_.jpg'      , array() );
      $fla[] = array( '#20110428\'1221.holla.txt'              , array() );
      $fla[] = array( '#20110428\'1221.holla.p99q66_.jpg'      , array() );

      // () [seq 20110518°2031]
      for ($i = 0; $i < sizeof($fla); $i++)
      {
         // To the regex split [line 20110518°2033]
         // note : We tried the PREG_SPLIT_NO_EMPTY flag, but that work not as as
         //  expected, if it is set, always just the complete input is returned
         $fla[$i][1] = preg_split('/[#_.]/', $fla[$i][0] );

         // Remove empty element [seq 20110518°2035]
         //  This is what we expected from PREG_SPLIT_NO_EMPTY
         $iSiz = sizeof($fla[$i][1]);
         for ($j = 0; $j < $iSiz ; $j++)
         {
            if ( $fla[$i][1][$j] === '' )
            {
               unset($fla[$i][1][$j]);
            }
         }

         // Re-index the array to heal the index gaps left by unset() [line 20110518°2037]
         $fla[$i][1] = array_values($fla[$i][1]);
      }

      return;
   }

}

/* eof */
