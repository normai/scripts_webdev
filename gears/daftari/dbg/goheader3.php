<?php

// If this file is UTF-8-with-BOM, it will fail,
//  if it is UTF-8-without-BOM, it will succeed.


include("./goheader.php");

$ht = <<<EOT
 <!DOCTYPE html>
 <html lang="en">
  <head>
   <meta charset="utf-8" data-encoding="UTF-8-with-BOM">
   <title>GoHeader_2</title>
   <link rel="stylesheet" href="./../css/daftari.css" />
  </head>

  <body class="BodyFaceHelpfiles">

   <h1 id="">
                GoHeader_2
   </h1>

   <p id="">
          PHP Header test ...
   </p>

   <p class="Footer23Referer">
                <span class="Footer23Url">./<a href="./goheader1.html">goheader1.html</a></span>
                <span class="Footer23Copy">© 2023 - 2024 Norbert C. Maier</span>
                <span class="Footer23License"><a href="https://creativecommons.org/licenses/by-sa/4.0/" class="LnkExtern" data-webshot="20180503°2122">CC BY-SA 4.0</a></span>
                <span class="Footer23PageId">20231009°1131</span>
   </p>
  </body>
 </html>
EOT;
echo($ht);

