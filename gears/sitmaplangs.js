﻿/*!
 * This provides token translations for the crumbs
 *
 * file        : 20210521°1521 (after 20190417°0421) scriptsweb/trunk/p1html/pages/sitmaplangs.js
 * license     : GNU AGPL v3
 * copyright   : © 2020 - 2024 Norbert C. Maier
 * authors     : Norbert C. Maier
 * encoding    : UTF-8-with-BOM
 */

/*global window, Daf*/ // setting for e.g. validatejavascript.com [line 20200103°0241]

/**
 * Announce namespace
 *
 * @id 20170923°0421`10
 * @c_o_n_s_t — Namespace
 */
Daf = window.Daf || {};

/**
 * Announce namespace
 *
 * @id 20150317°0911`02
 * @const — Namespace
 */
Daf.Sitmp = Daf.Sitmp || {};

/**
 * Translations for the crumbs, experimental
 *
 * @id 20190417°0411
 * @type {Object}
 */
Daf.Sitmp.oSitlangs =
{
   "fileinfo" :
   {    "file"     : "scriptsweb/trunk/p1html/pages/sitmaplangs.js"
      , "id"       : "20210521°1521"
      , "encoding" : "UTF-8-with-BOM"
      , "callers"  : "~daftari/jsi/daflingos.js"
   }
   , "Articles#20181217°0233" :
   {
        "eng" : "Articles"
      , "ara" : "مقالات"
      , "chi" : "用品"
      , "cze" : "Články"
      , "fre" : "Des articles"
      , "ger" : "Artikel"
      , "hin" : "सामग्री"
      , "ita" : "Articoli"
      , "jpn" : "記事"
      , "rus" : "статьи"
      , "spa" : "Artículos"
      , "swa" : "Makala"
   }
};
