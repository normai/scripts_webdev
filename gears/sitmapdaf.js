﻿/**
 * file : /sitmapdaf.js
 * id : 20200225°1357 [after 20200221°0331]
 * encoding : UTF-8-with-BOM
 * note :
 */

var DAF_PAGENODES =
[
   [ 'WebDev'              ,  './../index.html' ,
      [ 'HTML'             ,  './../p1html/index.html'   ]
    , [ 'CSS'              ,  './../p2css/index.html'   ]
    , [ 'JS-Bas'           ,  './../p3jsbas/webdevjs.html' ]
    , [ 'JS-Adv'           ,  './../p4jsadv/index.html'
        , [ 'Demos'        ,  './../p4jsadv/pages/demos.html' ]
        , [ 'Listings'     ,  './../p4jsadv/pages/listings.html' ]
      ]
    , [ 'WebAPI'           ,  './../p5webapi/index.html'   ]
    , [ 'Graph'            ,  './../p6graph/index.html'   ]
    , [ 'Networks'         ,  './../p7nwbas/nwbasics.html' ]
    , [ 'Refs'             ,  './../more/references.html' ]
   ]
];
