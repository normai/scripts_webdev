﻿
   project 20200308°2111 'Ackermann Kap 13'

   This folder processes the 23 demos after
   • https://github.com/cleancoderocker/javascripthandbuch/tree/master/Kapitel13


   ————————————————————————————
   Notes :

   ref 20200315°1732 'mdn → iframe'
   url : https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe
   usage : Page 20200315°0811 index.html

   ref 20200315°1726 'stackoverflow → get parameters'
   url : https://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-get-parameters
   usage : seq 20200315°0927 retrieve possible query string parameters

   ref 20200229°0512
   url : https://stackoverflow.com/questions/9216441/intercept-calls-to-console-log-in-chrome
   title : Intercept calls to console.log in Chrome
   usage : file 20200229°0811 consoleprinter.js

   ————————————————————————————
   [file 20200308°2112] ⬞Ω
