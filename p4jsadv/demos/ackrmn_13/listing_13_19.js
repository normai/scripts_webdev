﻿// Der Konstruktor der 'Oberklasse' muß vor Zugriff auf 'this' aufgerufen werden.

'use strict';
class Animal {
   constructor(name, color, age) {
      this.name = name;
      this.color = color;
      this.age = age;
   }
   eat(food) {
      console.log('Mmpf mmpf, ' + food);
   }
   drink(drink) {
      console.log('Mmmmmmh, ' + drink);
   }
   get name() {
      return this._name;
   }
   set name(name) {
      this._name = name;
   }
   get color() {
      return this._color;
   }
   set color(color) {
      this._color = color;
   }
   get age() {
      return this._age;
   }
   set age(age) {
      this._age = age;
   }
}
class Dog extends Animal {
   constructor(name, color, age, type) {
      this.type = type;                  // Fehler, this nicht definiert
      super(name, color, age);           /// Konstruktor der Oberklasse
   }
   get type() {
      return this._type;
   }
   set type(type) {
      this._type = type;
   }
   bark() {
      console.log('Wuff wuff');
   }
}

let a = new Animal();
a.eat('Pizza');
let d = new Dog(); // ↯ Reference error, must call super constructor


/** ---------------------------------------------------
 * file : 20200207°0611
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_19/main.js
 * ----------------------------------------------------
 */
