﻿// Definition einer Funktion, die als Kosntruktorfunktion aufgerufen werden kann

'use strict';
function Animal(name, color, age) {
   this.name = name;
   this.color = color;
   this.age = age;
};

/// now use the function
let a = new Animal('Hamster', 'brown', 1); //// alternative zu Object.create(), aber mit zusätzlichen Parameter
console.log(a.name + ' ist ' + a.color);


/** ---------------------------------------------------
 * file : 20200207°0421
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_08/main.js
 * ----------------------------------------------------
 */
