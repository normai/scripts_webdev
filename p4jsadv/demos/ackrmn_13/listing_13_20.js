﻿// Der Aufruf des Elternkonstruktors darf nicht weggelassen werden.

'use strict';
class Animal {
   constructor(name, color, age) {
      this.name = name;
      this.color = color;
      this.age = age;
   }
   eat(food) {
      console.log('Mmpf mmpf, ' + food);
   }
   drink(drink) {
      console.log('Mmmmmmh, ' + drink);
   }
   get name() {
      return this._name;
   }
   set name(name) {
      this._name = name;
   }
   get color() {
      return this._color;
   }
   set color(color) {
      this._color = color;
   }
   get age() {
      return this._age;
   }
   set age(age) {
      this._age = age;
   }
}
class Dog extends Animal {
   constructor(name, color, age, type) {
      // Fehler, da kein Aufruf des Elternkonstruktors
      /// super(name, color, age);             /// Elternkonstruktors
      this.type = type;                        /// ergänzt
   }
   bark() {
      console.log('Wuff wuff');
   }
}


/** ---------------------------------------------------
 * file : 20200207°0621
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_20/main.js
 * ----------------------------------------------------
 */
