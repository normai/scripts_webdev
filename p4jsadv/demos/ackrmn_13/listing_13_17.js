﻿// Definition von Gettern und Settern

'use strict';
class Animal {
   constructor(name, color, age) {
      this.name = name;               //// What about the underline?!
      this.color = color;
      this.age = age;
   }
   eat(food) {
      console.log('Mmpf mmpf, ' + food);
   }
   drink(drink) {
      console.log('Mmmmmmh, ' + drink);
   }
   get name() {
      return this._name;
   }
   set name(name) {
      this._name = name;
   }
   get color() {
      return this._color;
   }
   set color(color) {
      this._color = color;
   }
   get age() {
      return this._age;
   }
   set age(age) {
      this._age = age;
   }
}

let snake = new Animal('Zischi', 'Grün', 5);
snake.name = "Zischlix";

console.log(snake.name);               // "Zischlix"
console.log(snake.color);              // "Grün"
console.log(snake.age);                // 5


/** ---------------------------------------------------
 * file : 20200207°0541
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_17/main.js
 * ----------------------------------------------------
 */
