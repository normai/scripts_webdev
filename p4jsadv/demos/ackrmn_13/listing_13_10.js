﻿// Definition und Aufruf von Methoden

'use strict';
function Animal(name, color, age) {
   this.name = name;
   this.color = color;
   this.age = age;
   ////this.eat = function(food) {                ////
   ///console.log('Mmpf mmpf, ' + food + '!');    ////
   ///}                                           ////
};

Animal.prototype.eat = function(food) {
   console.log('Mmpf mmpf, ' + food + '!');
};

Animal.prototype.drink = function(drink) {
   console.log('Mmmmmmh, ' + drink + '!');
};
let fish = new Animal('Fischi', 'Grün', 2);
fish.eat('Algen');                     // Ausgabe: 'Mmpf mmpf, Algen!'
fish.drink('Wasser');                  // Ausgabe: 'Mmmmmmh, Wasser!'


/** ---------------------------------------------------
 * file : 20200207°0441
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_10/main.js
 * ----------------------------------------------------
 */
