﻿// Auch Standardobjekte wie 'Error' können abgeleitet werden.

'use strict';
class InvalidValueError extends Error {
   constructor(value) {
      super('Ungültiger Wert: ' + value);
      this.value = value;
   }
   get value() {
      return this._value;
   }
   set value(value) {
      this._value = value;
   }
}
throw new InvalidValueError(5);


/** ---------------------------------------------------
 * file : 20200207°0641
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_22/main.js
 * ----------------------------------------------------
 */
