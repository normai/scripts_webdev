﻿// Beispiel für eine Klasse.
// Erstellen einer Objektinstanz und Aufruf einer Objektmethode unter Verwendung von 'Klassen'.

'use strict';
class Animal {
   constructor(name, color, age) {
      this.name = name;
      this.color = color;
      this.age = age;
   }
   eat(food) {
      console.log('Mmpf mmpf, ' + food);
   }
   drink(drink) {
      console.log('Mmmmmmh, ' + drink);
   }
   toString() {
      return this.name + ', ' + this.color + ', ' + this.age;
   }
}

let fish = new Animal('Fischi', 'Grün', 2);
fish.eat('Algen');
console.log(fish.toString());          // Ausgabe: "Fischi, Grün, 2"


/** ---------------------------------------------------
 * file : 20200207°0531
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/tree/master/Kapitel13/Listing_13_15-16
 * ----------------------------------------------------
 */
