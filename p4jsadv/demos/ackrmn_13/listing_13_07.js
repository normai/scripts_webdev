// Aufruf einer Methode des Prototyps bei Zugriff über die Methode 'getPrototypeOf()'

'use strict';
let animal = {
   name: '',
   color: 'Braun',
   age: 0,
   eat: function(food) {
      console.log('Mmpf mmpf, ' + food + '!');
   },
   drink: function(drink) {
      console.log('Mmmmmmh, ' + drink + '!');
   }
};
let cat = Object.create(animal);
cat.meow = function() {
   console.log('Miauuuuuu!');
};
let dog = Object.create(animal);
dog.bark = function() {
   console.log('Wuff wuff!');
};
let vegetarianDog = Object.create(dog);
vegetarianDog.eat = function(food) {
   if(food.indexOf('Fleisch') >= 0 || food.indexOf('fleisch') >= 0) {
      ///throw new Error('Ich esse doch kein Fleisch!');
      console.log('Ich esse doch kein Fleisch!');
   } else {
      Object.getPrototypeOf(this).eat.call(this, food); //// alternative zu __proto__
   }
};
vegetarianDog.eat('Käse');
vegetarianDog.eat('Fleisch');


﻿/** ---------------------------------------------------
 * file : 20200207°0411
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_07/main.js
 * ----------------------------------------------------
 */
