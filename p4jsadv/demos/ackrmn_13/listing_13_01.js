﻿ // Erstellen von Objekten auf Basis eines Prototyp

'use strict';
let animal = {
   name: '',
   color: 'Braun',
   age: 0,
   eat: function(food) {
      console.log('Mmpf mmpf, ' + food + '!');
   },
   drink: function(drink) {
      console.log('Mmmmmmh, ' + drink + '!');
   }
};
let cat = Object.create(animal);
let dog = Object.create(animal);

/// now use the objects
dog.drink('Water');
dog.eat('Dog biscuits');
cat.drink('Milk');
cat.eat('Cat food');


/** ---------------------------------------------------
 * file : 20200207°0311
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_01/main.js
 * ----------------------------------------------------
 */
