﻿// Erzeugen einer Objektinstanz unter Verwendung einer Konstruktorfunktion

'use strict';
function Animal(name, color, age) {
   this.name = name;
   this.color = color;
   this.age = age;
};
let fish = new Animal('Fischi', 'Grün', 2);
console.log(fish.name);                // Ausgabe: "Fischi"
console.log(fish.color);               // Ausgabe: "Grün"
console.log(fish.age);                 // Ausgabe: 2


/** ---------------------------------------------------
 * file : 20200207°0431
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_09/main.js
 * ----------------------------------------------------
 */
