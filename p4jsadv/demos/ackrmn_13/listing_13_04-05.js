﻿// Überschreiben einer Methode in einem ableitenden Objekt.
// Aufruf von Methoden, die an verschiedenen Stellen in der Prototypenkette definiert sind.

'use strict';
let animal = {
   name: '',
   color: 'Braun',
   age: 0,
   eat: function(food) {
      console.log('Mmpf mmpf, ' + food + '!');
   },
   drink: function(drink) {
      console.log('Mmmmmmh, ' + drink + '!');
   }
};
let cat = Object.create(animal);
cat.meow = function() {
   console.log('Miauuuuuu!');
};
let dog = Object.create(animal);
dog.bark = function() {
   console.log('Wuff wuff!');
};
let vegetarianDog = Object.create(dog);
vegetarianDog.eat = function(food) {
   if (food.indexOf('Fleisch') >= 0 || food.indexOf('fleisch') >= 0) {
      ///throw new Error('Ich esse doch kein Fleisch!');
      console.log('Ich esse doch kein Fleisch!'); ///
   } else {
      console.log('Mmpf mmpf, ' + food + '!');
   }
};
vegetarianDog.eat('Käse');             // vorhanden in vegetarionDog
vegetarianDog.bark();                  // vorhanden in dog
vegetarianDog.drink('Wasser');         // vorhanden in animal
////vegetarianDog.toString();          // vorhanden in Object
console.log('***' + vegetarianDog.toString() + '***');
console.log(vegetarianDog);
//vegetarianDog.eat('Flisch');         ///
vegetarianDog.eat('Fleisch');          ///


/** ---------------------------------------------------
 * file : 20200207°0341
 * credit : Philip Ackermann 2018
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel13/Listing_13_04-05/main.js
 * ----------------------------------------------------
 */
