/**
 * file        : 20200229°0811 consoleprinter.js
 * summary     : Print the console.log() messages additionally to the page
 * versions    : • v0.1.1 20200316°0213 In sync with page 20190204°0211 jqdocumentready.html
 *               • v0.1.0 20200229°0814 Initial release with JsAdv
 * encoding    : UTF-8-with-BOM
 */
(function(){

   // Preserve original function
   var oldLogFunc = console.log;

   // Redefine function
   console.log = function (message) {
      pageOutFunc(message);
      oldLogFunc.apply(console, arguments);
   };

   // Provide the page output function
   let pageOutFunc = function (sMsg) {

      // Retrieve target element
      let eOut = document.querySelector("#OutputArea");                // Like getElementById()
      if (!eOut) {
         let sArea = '<pre id="OutputArea"><span style="color:Orange; font-style:italic;">console.log() :</pre>';
         document.getElementsByTagName("body")[0].insertAdjacentHTML("afterbegin", sArea);  // "beforeend"
      }

      // Style target element
      eOut = document.querySelector("#OutputArea");                    // Again
      eOut.style.border = '2px solid Orange';
      eOut.style.borderRadius = '0.7em';
      eOut.style.fontFamily = 'monospace, monospace';
      eOut.style.padding = '0.1em 0.7em 0.1em 0.7em';
      eOut.style.width = '94%';

      // Output to target element
      let sOut = eOut.innerHTML;
      if (sOut.length > 0) {
         sOut += '\n';
      }
      sOut += '<span style="color:Orange">&gt; </span>';
      eOut.innerHTML = sOut + sMsg;
   };
})();
