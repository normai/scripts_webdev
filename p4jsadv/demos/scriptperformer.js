﻿/**
 * file        : 20200315°0911 scriptperformer.js
 * summary     : Load and run JavaScript code(s) on demand
 * versions    : • 20200315°0912..
 * usage       : (1) Load this script in the head of a HTML file
 *               (2) As well load file 20200229°0811 consoleprinter.js
 *               (3) In this page use links like this "?scriptToLoad=listing_13_03.js"
 *               (4) If such link is clicked, in one box appears the source, in the other box the output
 * note        : The former name was scriptrunner.js, but such projects exist already.
 * encoding    : UTF-8-with-BOM
 */

'use strict';

window.RunJs = {};                                                     // Namespace

/**
 * This performs the script pull-behind
 *
 * @id 20200315°0921
 * @note Compare function 20110821°0121 pullScriptBehind() in
 *     downtown.trilo.de/svn/daftaridev/trunk/daftari/jsi/daftari.js
 * @note Unfortunately, multiple pulls will not work. This is why the
 *     refresh mechanism reloads the page utilizing a get parameter.
 * @param sScLoad {String} The script to load
 * @param sFuncToRun {String} An optional function to start, typically 'init'
 */
RunJs.pullScriptBehind = function (sScLoad, sFuncToRun)
{
   console.log('<span style="font-size:small; font-style:italic; color:Gray;">Run ' + sScLoad + '</span>');  // Notify

   // prepare the involved elements [seq 20110821°0123]
   var head = document.getElementsByTagName('head')[0];
   var script = document.createElement('script');

   // Set script properties [seq 20110821°0124]
   script.src = sScLoad;
   if (sFuncToRun) {
      script.onload = function () { window[sFuncToRun](); } ;          // Tricky syntax
   }

   // Ignit pulling [seq 20110821°0125]
   head.appendChild(script);

   return true;
};

/**
 * Code to run after document is complete
 *
 * @id 20200315°0923
 * @note Spelling 'document.onload' does not work with Chrome 80, why not?
 * @return {undefined}
 */
window.onload = function() {

   // Extract possible query string parameters [seq 20200315°0927]
   let oUrl = new URL(document.location.href);
   let sScriptToLoad = oUrl.searchParams.get('scriptToLoad');
   let sFuncToRun = oUrl.searchParams.get('funcToRun');

   // If script-to-load is given, display it in iframe and run it [condi 20200315°0924]
   if (sScriptToLoad !== null) {

      // Display target script in iframe [seq 20200315°0925]
      let eIfram = document.querySelector('#scriptiframe');
      eIfram.src = sScriptToLoad;                                      // Warning "Resource interpreted as Document but transferred with MIME type application/javascript .." [warning 20200316°0415]

      /*
      todo 20200316°0413 'find more about manipulating iframes'
      do : Find out more about how to adjust an iframe dynamically according the
            specific document it hosts, especially if that is a JavaScript file.
      location : seq 20200316°0411, re-open sequence to continue experiments
      btw : What is the difference between contentWindow and contentDocument?!
      status :
      */

      // Experiments — try find props of faded-in document [seq 20200316°0411]
      // Sorrily, we can neither get e.g. the mime type nor set e.g. font style.
      // See todo 20200316°0413 'find more about manipulating iframes'
      let cw = eIfram.contentWindow;                                   //
      let cd = eIfram.contentDocument;                                 //
      if (false) {
         cd.body.style.color="Magenta";                                // Does not work
         cd.body.style.backgroundColor="Yellow";                       // Causes a yellow flash on load
         let iW = cd.body.scrollWidth;                                 // The same as set before
         eIfram.width = '' + iW + 'px';                                // Nothing new
         console.log('iframe width = "' + eIfram.width + '"');
      }
      cd.body.style.whiteSpace = 'nowrap';                             // Does not work
      cw.document.body.style.whiteSpace = 'nowrap';                    // Does not work

      // Load and run the target script [line 20200315°0926]
      RunJs.pullScriptBehind(sScriptToLoad, sFuncToRun);
   }
};
