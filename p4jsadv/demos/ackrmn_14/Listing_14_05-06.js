﻿/**
 * file : 20200229°0423
 * credit : Code after Philip Ackermann
 *    https://github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel14/Listing_14_05-06/scripts/main.js
 * encoding : UTF-8-with-BOM
 */

function init() {
   (function() {
      let artists = [
         {
            name: 'Kyuss',
            albums: [
               {
                  title: 'Wretch',
                  year: 1991
               },
               {
                  title: 'Blues for the Red Sun',
                  year: 1992
               },
               {
                  title: 'Welcome to Sky Valley',
                  year: 1994
               },
               {
                  title: '...And the Circus Leaves Town',
                  year: 1995
               }
            ]
         },
         {
            name: 'Ben Harper',
            albums: [
               {
                  title: 'The Will to Live',
                  year: 1997
               },
               {
                  title: 'Burn to Shine',
                  year: 1999
               },
               {
                  title: 'Live from Mars',
                  year: 2001
               },
               {
                  title: 'Diamonds on the Inside',
                  year: 2003
               }
            ]
         }
      ];
      let artistNames = [];
      for (let i = 0; i < artists.length; i++) {
         artistNames.push(artists[i].name);
      }
      console.log(artistNames);
   })();

   (function() {
      let artists = [
         {
            name: 'Kyuss',
            albums: [
               {
                  title: 'Wretch',
                  year: 1991
               },
               {
                  title: 'Blues for the Red Sun',
                  year: 1992
               },
               {
                  title: 'Welcome to Sky Valley',
                  year: 1994
               },
               {
                  title: '...And the Circus Leaves Town',
                  year: 1995
               }
            ]
         },
         {
            name: 'Ben Harper',
            albums: [
               {
                  title: 'The Will to Live',
                  year: 1997
               },
               {
                  title: 'Burn to Shine',
                  year: 1999
               },
               {
                  title: 'Live from Mars',
                  year: 2001
               },
               {
                  title: 'Diamonds on the Inside',
                  year: 2003
               }
            ]
         }
      ];

      // () Drei Möglickeiten, die map-Funktion zu parameterisieren
      // (1) use map with lambda expression
      let artistNames1 = artists.map(
         (artist, index, artists) => {
            return artist.name;
         }
      );
      console.log(artistNames1);

      // (2) use map with named function
      mapFunc = function(artist, index, artists) {
         return artist.name;
      }
      let artistNames2 = artists.map(mapFunc);
      console.log(artistNames2);

      // (3) use map with anonymous function
      // see next code example

   })();
}
