﻿/**
 * file : 20200316°0131 demo-intercept-links.js [formerly relocate-links.js]
 * summary : This demonstrates how to programmatically intercept and relocate links
 * author : ncm
 * license : MIT
 * compare : file 20190227°1351 jqdocumentready.js seq 20150321°0441 '$("a").click'
 * encoding : UTF-8-with-BOM
 */

/**
 * This function constitutes the new onclick event handler
 *
 * @id 20200315°0931
 * @param eAnchor {Element} The anchor element being clicked
 * @return {undefined}
 */
RunJs.handleClick = function (eAnchor) {

   let sNewUrl = 'https://www.wikipedia.org/';
   let sMsg = 'The clicked link will be redirected'
             + '\n • from : ' + eAnchor.getAttribute('href')
              + '\n • to : ' + sNewUrl
               + '\n\nReally do redirect?'
                ;
   if(! confirm(sMsg)) return;

   event.preventDefault(); // the crucial line
   document.location.href = sNewUrl; // the wanted action
};

// Register event handlers [seq 20200315°0935]
let els = document.querySelectorAll('[href]');
els.forEach ( el => {
   el.setAttribute('onclick', 'RunJs.handleClick(this);');
});

console.log('All links on page will be redirected.');
