﻿/*!
 * file : 20190902°1911 [after 20190502°0211] /thirdparty.js
 * summary : Provide thirdparty helper functions
 * origin : Cut from file 20190502°0211 http://www.trekta.biz/svn/modpopdev/trunk/modpopdialog/modpopdialog.js
 * license : The MIT license
 * copyright : Norbert C. Maier
 * encoding : UTF-8-without-BOM
 * status : in-statu-nascendi
 */

// Options for JSHint https://jshint.com/ [seq 20190504°0121]
/* jshint laxbreak : true, laxcomma : true */

// Options for JSLint https://jslint.com/ [seq 20190504°0131]
/*jslint
    browser, devel, long, single, this, white
*/
//   browser = Assume a browser (see https://jslint.com/help.html#browser)
//   devel   = Assume in development (see https://jslint.com/help.html)
//   long    = Tolerate long source lines (more than 80 characters)
//   single  = Tolerate single quote strings
//   this    = Tolerate this
//   white   = Tolerate whitespace mess

/**
 * This function shall constitute class ModPop
 *
 * @id 20190902°1921 [after 20190502°0221]
 * @callers •
 */

'use strict';

// Idiom for a namespace
window.ModPop = {};

/**
 * This public static method constitutes a primitive debug output facility
 *
 * @id 20190902°1921 (after 20190503°0231)
 * @note Interestingly, this is a static method defined inside the class body,
 *     not outside like the other static methods so far. Seems to work, does it?
 * @param sMsg {String} The message to output
 */
ModPop.writeDbgMsg = function (sMsg)
{
   // Proglogue [seq 20190503°0233]
   var sOld = '';
   var elArea = document.getElementById('pre_debug_area');

   // Possibly create div [seq 20190503°0235]
   if (elArea === null) {
      ////elBody = document.getElementsByTagName('body')[0];
      elArea = document.createElement('pre');
      elArea.id = 'pre_debug_area';
      elArea.style.fontSize = 'x-small';
      elArea.style.backgroundColor = 'Cornsilk';
      elArea.style.border = '2px solid SlateGray';
      elArea = document.body.insertBefore(elArea, document.body.firstChild);
   }

   // () Write message [seq 20190503°0237]
   sOld = elArea.innerHTML;
   sOld = ((sOld === '') ? sOld : sOld + '\n');
   elArea.innerHTML = sOld + sMsg;
};
