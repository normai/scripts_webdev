﻿
   project 20190909°0907 Ajax
   url         :
   title       : Demonstrate Callbacks and AJAX
   summary     :
   searching   :
   chain       :
   icon        :
   tags        :
   note        :
   ⬞
   —————————————————————— Notes ———————————————————————

   ref 20230930°0931
   Remember some of the cat images
   • https://cdn2.thecatapi.com/images/4k4.gif
   • https://cdn2.thecatapi.com/images/MTg0OTAxMw.jpg
   •

   ———————————————————— Changelog —————————————————————

   version 20230930°1451 v0.1.1
   • In getuinames*.html: Supplement error message for uinames.com being down [lines 20230930°0911]
   • In getcats.html: Scale displayed image down [func 20230930°0921]
   • Restore file 20190909°0921 quickdemos.html demonstrating callback flavours
   • Spawn randomuser*.html from getuinames*.html

   version 20200906°2107 v0.1.0
   • Filled in repo 20180910°1121 scripts_webdev with r20 2020-09-06`21:56

   version 20200906°2107 v0.0.9
   • Filled in repo 20190902°0818 webdev with r23 2019-09-11`08:32

   —————————————————————— Files ———————————————————————

   file 20230930°1431 randomuser1.html  AJAX flavour one (out of order)
   file 20230930°1421 randomuser2.html  AJAX flavour two (out of order)
   file 20230930°1411 randomuser3.html  AJAX flavour three (out of order)

   file 20190910°1021 getcats.html      Fetch images from image server
   file 20190910°1131 getuinames1.html  AJAX flavour one — Out of order
   file 20190910°1331 getuinames2.html  AJAX flavour two — Out of order
   file 20190910°1451 getuinames3.html  AJAX flavour three — Out of order
   file 20190902°1911 helpers.js        Provide some output facility
   file 20190909°0921 quickdemos.html   Callback flavours an other JS particularities

   ———————————————————————
   [file 20190909°0908] ⬞Ω
