'use strict';

﻿var catalog = {
   id22345 : {
      name : 'Gurkenglas (500 ml)',
      price : '2.50'
   },
   id23445 : {
      name : 'Birnen (2 kg)',
      price : '4.50'
   },
   id46647 : {
      name : 'Kirschen',
      price : '2.50'
   },
   id65464 : {
      name : 'Äpfel im Korb (2 kg)',
      price : '4.50'
   },
   id46648 : {
      name : 'Salat',
      price : '2.50'
   },
   id74747 : {
      name : 'Kirschen im Glas (1000 ml)',
      price : '2.50'
   },
   id46646 : {
      name : 'Bananen',
      price : '4.50'
   }
};

function showCatalog() {

   let sTr = '';
   for (let item in catalog) {
      let x = catalog[item];
      let sName = x.name;
      let sPrice = x.price;
      let s = '<tr><td>' + sName + '</td><td>' + sPrice + '</td></tr>';
      sTr += s;
   }
   let el = document.querySelector('tbody');
   el.innerHTML = sTr;
}

/*
 * file : 20180912°0031
 * credit : github.com/cleancoderocker/javascripthandbuch/blob/master/Kapitel12/Listing_12_27/catalog.js
 */
